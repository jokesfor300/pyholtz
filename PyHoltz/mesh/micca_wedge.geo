// Script to generate a 3D-Mesh for a MICCA Chamber (a full slice)
// Philip Buschmann, Georg Mensah, Jonas Moeck, TU Berlin 2015-2016

/////////////////////////////////////////////////////////////
// 		Parameters
// define characteristic length (for mesh size)
lc =  0.05*1;   // general mesh (0.005)
lcf = 0.125*lc*2/2; // flame region (0.25*lc)
lci = 0.125*lc*2/2; // injection region (0.25*lc)

// rotational symmetry
N   = 16;
angle = (2*Pi)/(N); 
phi = angle/2;


// geometric parameters of burner
R  = 0.175;	// distance from the centre of the plenum to rotational axis

dR_p = 0.070/2; 	// radial thickness of plenum
l_p = 0.070;	// height plenum
R_in_p  = R - dR_p;
R_out_p = R + dR_p;

dR_br = 0.033/2; 	// radial thickness of burner
l_br = 0.014;	// height burner
R_in_br = R - dR_br;
R_out_br = R + dR_br;

dR_inj = 0.0189/2; 	// radial thickness of injection
l_inj = 0.006;	// height injection
R_in_inj = R - dR_inj;
R_out_inj = R + dR_inj;

dR_f = 0.036/2; 	// radial thickness of flame
l_f = 0.006;	// height flame
R_in_f = R - dR_f;
R_out_f = R + dR_f;

dR_cc = 0.050/2; 	// radial thickness of duct
l_cc = 0.241;	// height duct
R_in_cc = R - dR_cc;
R_out_cc = R + dR_cc;

// flame properties
lf = 0.1; // flame thickness (around KOS)

/////////////////////////////////////////////////////////////
//		Points		
// Plenum Bottom
Point(1)  = {-R_in_p          ,0               ,-(l_inj + l_br + l_p),lc};
Point(2)  = {-R_out_p         ,0               ,-(l_inj + l_br + l_p),lc};
Point(3)  = {-R_out_p*Cos(phi),R_out_p*Sin(phi),-(l_inj + l_br + l_p),lc};
Point(4)  = {-R_in_p*Cos(phi) ,R_in_p*Sin(phi) ,-(l_inj + l_br + l_p),lc};

// Plenum Top
Point(5)  = {-R_in_p          ,0               ,-(l_inj + l_br),lc};
Point(6)  = {-R_out_p         ,0               ,-(l_inj + l_br),lc};
Point(7)  = {-R_out_p*Cos(phi),R_out_p*Sin(phi),-(l_inj + l_br),lc};
Point(8)  = {-R_in_p*Cos(phi) ,R_in_p*Sin(phi) ,-(l_inj + l_br),lc};

// Burner Bottom
Point(11)  = {-R_in_br ,0    ,-(l_inj + l_br),lci};
Point(12)  = {-R_out_br,0    ,-(l_inj + l_br),lci};
Point(13)  = {-R       ,dR_br,-(l_inj + l_br),lci};

// Burner Top
Point(14)  = {-R_in_br ,0    ,-(l_inj),lc};
Point(15)  = {-R_out_br,0    ,-(l_inj),lc};
Point(16)  = {-R       ,dR_br,-(l_inj),lc};

// Injection Bottom
Point(21)  = {-R_in_inj ,0     ,-(l_inj),lci};
Point(22)  = {-R_out_inj,0     ,-(l_inj),lci};
Point(23)  = {-R        ,dR_inj,-(l_inj),lci};

// Injection Top
Point(24)  = {-R_in_inj ,0     ,0,lci};
Point(25)  = {-R_out_inj,0     ,0,lci};
Point(26)  = {-R        ,dR_inj,0,lci};

// Flame: Towards Bottom
Point(31)  = {-R_in_f ,0   ,0,lcf};
Point(32)  = {-R_out_f,0   ,0,lcf};
Point(33)  = {-R      ,dR_f,0,lcf};

// Flame: Towards Top
Point(34)  = {-R_in_f ,0   ,l_f,lcf};
Point(35)  = {-R_out_f,0   ,l_f,lcf};
Point(36)  = {-R      ,dR_f,l_f,lcf};

// CC Bottom
Point(41)  = {-R_in_cc          ,0                ,0,lc};
Point(42)  = {-R_out_cc         ,0                ,0,lc};
Point(43)  = {-R_out_cc*Cos(phi),R_out_cc*Sin(phi),0,lc};
Point(44)  = {-R_in_cc*Cos(phi) ,R_in_cc*Sin(phi) ,0,lc};

// CC Top
Point(45)  = {-R_in_cc          ,0                ,l_cc,lc};
Point(46)  = {-R_out_cc         ,0                ,l_cc,lc};
Point(47)  = {-R_out_cc*Cos(phi),R_out_cc*Sin(phi),l_cc,lc};
Point(48)  = {-R_in_cc*Cos(phi) ,R_in_cc*Sin(phi) ,l_cc,lc};

// Additional (KOS-origin at every level)
Point(51) = {0,0,-(l_inj + l_br + l_p),lc};
Point(52) = {0,0,-(l_inj + l_br)      ,lc};
Point(53) = {0,0,-(l_inj)             ,lc};
Point(54) = {0,0,0                    ,lc};
Point(55) = {0,0,l_f                  ,lc};
Point(56) = {0,0,l_cc                 ,lc};

Point(61) = {-R,0,-(l_inj + l_br),lc};
Point(62) = {-R,0,-(l_inj)       ,lc};
Point(63) = {-R,0,0              ,lc};
Point(64) = {-R,0,l_f            ,lc};

///// LINES
//		Lines (horizontal)

// Plenum Bottom
Line(101)   = {1,2};
Circle(102) = {2,51,3}; // Circle Arc, starting at 2, ending at 3, center at 51
Line(103)   = {3,4};
Circle(104) = {4,51,1};
// Plenum Top
Line(105)   = {12,6};
Circle(106) = {6,52,7};
Line(107)   = {7,8};
Circle(108) = {8,52,5};
Line(109)   = {5,11};

// Burner Bottom
Line(111)    = {11,12};
Circle(112) = {12,61,13};
Circle(113) = {13,61,11};
// Burner Top
Line(114)   = {22,15};
Circle(115) = {15,62,16};
Circle(116) = {16,62,14};
Line(117)   = {14,21};

// Injection Bottom
Line(121)   = {21,22};
Circle(122) = {22,62,23};
Circle(123) = {23,62,21};
// Injection Top
Line(124)   = {24,25};
Circle(125) = {25,63,26};
Circle(126) = {26,63,24};

// Flame Bottom
Line(131)   = {25,32};
Circle(132) = {32,63,33};
Circle(133) = {33,63,31};
Line(134)   = {31,24};
// Flame Top
Line(135)   = {34,35};
Circle(136) = {35,64,36};
Circle(137) = {36,64,34};

// CC Bottom
Line(141)   = {32,42};
Circle(142) = {42,54,43};
Line(143)   = {43,44};
Circle(144) = {44,54,41};
Line(145)   = {41,31};
// CC Top
Line(146)   = {45,46};
Circle(147) = {46,56,47};
Line(148)   = {47,48};
Circle(149) = {48,56,45};

//		Lines (vertical)
// Plenum Bottom to Plenum Top
Line(201)   = {1,5};
Line(202)   = {2,6};
Line(203)   = {3,7};
Line(204)   = {4,8};
// Burner Bottom to Burner Top
Line(211)   = {11,14};
Line(212)   = {12,15};
Line(213)   = {13,16};
// Injection Bottom to Injection Top
Line(221)   = {21,24};
Line(222)   = {22,25};
Line(223)   = {23,26};
// Flame Bottom to Top
Line(231)   = {31,34};
Line(232)   = {32,35};
Line(233)   = {33,36};
// CC Bottom to CC Top
Line(241)   = {41,45};
Line(242)   = {42,46};
Line(243)   = {43,47};
Line(244)   = {44,48};

// 		Line Loops 
// Plenum
Line Loop(301) = {101,102,103,104};                // Bottom
Line Loop(302) = {105,106,107,108,109,-113,-112};  // Top
Line Loop(303) = {104,201,-108,-204};              // Inner
Line Loop(304) = {102,203,-106,-202};              // Outer
Line Loop(305) = {103,204,-107,-203};              // phi+
Line Loop(306) = {101,202,-105,-111,-109,-201};    // phi-

// Burner
Line Loop(311) = {111,112,113};                 // Bottom
Line Loop(312) = {114,115,116,117,-123,-122};   // Top
Line Loop(313) = {113,211,-116,-213};           // Inner
Line Loop(314) = {112,213,-115,-212};           // Outer
Line Loop(315) = {111,212,-114,-121,-117,-211}; // phi-

// Injection
Line Loop(321) = {121,122,123};        // Bottom
Line Loop(322) = {124,125,126};        // Top
Line Loop(323) = {123,221,-126,-223};  // Inner
Line Loop(324) = {122,223,-125,-222};  // Outer
Line Loop(325) = {121,222,-124,-221};  // phi-

// Flame
Line Loop(331) = {131,132,133,134,-126,-125};  // Bottom
Line Loop(332) = {135,136,137};                // Top
Line Loop(333) = {133,231,-137,-233};          // Inner
Line Loop(334) = {132,233,-136,-232};          // Outer
Line Loop(335) = {134,124,131,232,-135,-231};  // phi-

// CC
Line Loop(341) = {141,142,143,144,145,-133,-132};       // Bottom
Line Loop(342) = {146,147,148,149};                     // Top
Line Loop(343) = {144,241,-149,-244};                   // Inner
Line Loop(344) = {142,243,-147,-242};                   // Outer
Line Loop(345) = {143,244,-148,-243};                   // phi+
Line Loop(346) = {141,242,-146,-241,145,231,135,-232};  // phi-

/////////////////////////////////////////////////////////////
//		SURFACES

// Plenum 
Plane Surface(401) = {301}; // Bottom
Plane Surface(402) = {302}; // Top
Ruled Surface(403) = {303}; // Inner
Ruled Surface(404) = {304}; // Outer
Plane Surface(405) = {305}; // phi+
Plane Surface(406) = {306}; // phi-

// Burner 
Plane Surface(411) = {311}; // Bottom
Plane Surface(412) = {312}; // Top
Ruled Surface(413) = {313}; // Inner
Ruled Surface(414) = {314}; // Outer
Plane Surface(415) = {315}; // phi-

// Injection 
Plane Surface(421) = {321}; // Bottom
Plane Surface(422) = {322}; // Top
Ruled Surface(423) = {323}; // Inner
Ruled Surface(424) = {324}; // Outer
Plane Surface(425) = {325}; // phi-

// Flame 
Plane Surface(431) = {331}; // Bottom
Plane Surface(432) = {332}; // Top
Ruled Surface(433) = {333}; // Inner
Ruled Surface(434) = {334}; // Outer
Plane Surface(435) = {335}; // phi-

// CC 
Plane Surface(441) = {341}; // Bottom
Plane Surface(442) = {342}; // Top
Ruled Surface(443) = {343}; // Inner
Ruled Surface(444) = {344}; // Outer
Plane Surface(445) = {345}; // phi+
Plane Surface(446) = {346}; // phi-

///// LABEL
Physical Surface("Inlet")        = {401}; 
Physical Surface("Outlet")       = {442};
Physical Surface("FlameIn")      = {431}; 
Physical Surface("FlameOut")     = {432};  
Physical Surface("Symmetry")     = {406,415,425,435,446}; 
Physical Surface("Bloch")        = {405,445}; 
Physical Surface("Outer_Plenum")    = {404}; 
Physical Surface("Inner_Plenum")    = {403}; 
Physical Surface("Outer_Burner")    = {404}; 
Physical Surface("Burner_Wall")     = {413,414}; 
Physical Surface("Injection_Wall")  = {423,424}; 
Physical Surface("Outer_CC")        = {444}; 
Physical Surface("Inner_CC")        = {443}; 

/////////////////// VOLUMES ///////////////////
////// SURFACE LOOPS
// Plenum
Surface Loop(501) = {401,403,405,404,406,402,411};
// Burner
Surface Loop(502) = {411,413,414,415,412,421};
// Injection
Surface Loop(503) = {421,423,424,425,422};
// Flame
Surface Loop(504) = {422,431,433,434,435,432};
// CC
Surface Loop(505) = {441,433,434,432,446,443,445,444,442};

////// VOLUMES
Volume(601) = {501};
Volume(602) = {502};
Volume(603) = {503};
Volume(604) = {504};
Volume(605) = {505};

////// LABEL
Physical Volume("Plenum")     = {601};
Physical Volume("Burner")     = {602};
Physical Volume("Injection")  = {603};
Physical Volume("Flame")      = {604};
Physical Volume("CC")         = {605};
Physical Volume("Interior")   = {601,602,603,604,605};


