     // Script to generate a 3D-Mesh for a Rijke tube
// Philip Buschmann, Georg Mensah, Jonas Moeck

/////////////////// PARAMETERS ///////////////////
// Dimensions of the Rijke Tube

lc  = 0.025;   // characteristic length
lcf = .5*lc; // characteristic length for flame
r  = 0.025; // radius of the tube

hl  = 0.6;  // length of the tube (left)
hr  = 0.4;  // length of the tube (right)


/*
lc = 0.1;
r  = 0.1;
h  = 0.2;
*/

// Dimensions of the Flame (is located around KOS)
tf = 0.0001; // thickness of the flame


/////////////////// POINTS ///////////////////
// Inlet
Point(1)  = {0,0,-hl/2,lc};
Point(2)  = {r,0,-hl/2,lc};
Point(3)  = {0,r,-hl/2,lc};
Point(4)  = {-r,0,-hl/2,lc};
Point(5)  = {0,-r,-hl/2,lc};

// Flame In
Point(6)  = {0,0,-(tf/2),lcf};
Point(7)  = {r,0,-(tf/2),lcf};
Point(8)  = {0,r,-(tf/2),lcf};
Point(9)  = {-r,0,-(tf/2),lcf};
Point(10) = {0,-r,-(tf/2),lcf};

// Flame Mid
Point(11)  = {0,0,0,lcf};
Point(12)  = {r,0,0,lcf};
Point(13)  = {0,r,0,lcf};
Point(14)  = {-r,0,0,lcf};
Point(15)  = {0,-r,0,lcf};

// Flame Out
Point(16)  = {0,0,+(tf/2),lcf};
Point(17)  = {r,0,+(tf/2),lcf};
Point(18)  = {0,r,+(tf/2),lcf};
Point(19)  = {-r,0,+(tf/2),lcf};
Point(20)  = {0,-r,+(tf/2),lcf};

// Outlet
Point(21)  = {0,0,+hr/2,lc};
Point(22)  = {r,0,+hr/2,lc};
Point(23)  = {0,r,+hr/2,lc};
Point(24)  = {-r,0,+hr/2,lc};
Point(25)  = {0,-r,+hr/2,lc};


/////////////////// LINES ///////////////////
///// CIRCLES
// Inlet
Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
// Flame in
Circle(5) = {7,6,8};
Circle(6) = {8,6,9};
Circle(7) = {9,6,10};
Circle(8) = {10,6,7};
// Flame Mid
Circle(9)  = {12,11,13};
Circle(10) = {13,11,14};
Circle(11) = {14,11,15};
Circle(12) = {15,11,12};
// Flame out
Circle(13) = {17,16,18};
Circle(14) = {18,16,19};
Circle(15) = {19,16,20};
Circle(16) = {20,16,17};
// Outlet
Circle(17) = {22,21,23};
Circle(18) = {23,21,24};
Circle(19) = {24,21,25};
Circle(20) = {25,21,22};
////// STRAIGHT LINES
// Pre-Flame
Line(21) = {2,7};
Line(22) = {4,9};
Line(23) = {3,8};
Line(24) = {5,10};
// Flame-First Half
Line(25) = {7,12};
Line(26) = {9,14};
Line(27) = {8,13};
Line(28) = {10,15};
// Flame-Second Half
Line(29) = {12,17};
Line(30) = {14,19};
Line(31) = {13,18};
Line(32) = {15,20};
// Post-Flame
Line(33) = {17,22};
Line(34) = {19,24};
Line(35) = {18,23};
Line(36) = {20,25};


/////////////////// SURFACES ///////////////////
///// LINE LOOPS
// Inlet
Line Loop(1) = {1,2,3,4}; 
// Outlet
Line Loop(2) = {17,18,19,20};
// Pre-Flame
Line Loop(3) = {21,5,-23,-1};
Line Loop(4) = {23,6,-22,-2};
Line Loop(5) = {22,7,-24,-3};
Line Loop(6) = {24,8,-21,-4};
// Flame-First Half
Line Loop(7)  = {25,9,-27,-5};
Line Loop(8)  = {27,10,-26,-6};
Line Loop(9)  = {26,11,-28,-7};
Line Loop(10) = {28,12,-25,-8};
// Flame-Second Half
Line Loop(11)  = {29,13,-31,-9};
Line Loop(12)  = {31,14,-30,-10};
Line Loop(13)  = {30,15,-32,-11};
Line Loop(14)  = {32,16,-29,-12};
// Post-Flame
Line Loop(15) = {33,17,-35,-13};
Line Loop(16) = {35,18,-34,-14};
Line Loop(17) = {34,19,-36,-15};
Line Loop(18) = {36,20,-33,-16};
// Flame in
Line Loop(19) = {5,6,7,8};
// Flame mid
Line Loop(20) = {9,10,11,12};
// Flame out
Line Loop(21) = {13,14,15,16};

///// SURFACES
// Pre-Flame
Ruled Surface(17)  = {3};
Ruled Surface(18)  = {4};
Ruled Surface(19)  = {5};
Ruled Surface(20)  = {6};
// Flame-First Half
Ruled Surface(21)  = {7};
Ruled Surface(22)  = {8};
Ruled Surface(23)  = {9};
Ruled Surface(24)  = {10};
// Flame-Second Half
Ruled Surface(25)  = {11};
Ruled Surface(26)  = {12};
Ruled Surface(27)  = {13};
Ruled Surface(28)  = {14};
// Post-Flame
Ruled Surface(29)  = {15};
Ruled Surface(30)  = {16};
Ruled Surface(31)  = {17};
Ruled Surface(32)  = {18};
// Inlet
Plane Surface(33) = {1};
// Outlet
Plane Surface(34) = {2};
// Flame in
Plane Surface(35) = {19};
// Flame mid
Plane Surface(36) = {20};
// Flame out
Plane Surface(37) = {21};

///// LABEL
Physical Surface("Inlet")        = {33}; 
Physical Surface("Outlet")       = {34};
Physical Surface("FlameIn")      = {35}; 
Physical Surface("FlameMid")     = {36}; 
Physical Surface("FlameOut")     = {37};  
Physical Surface("Hull_Pre-Flame")  = {17,18,19,20}; 
Physical Surface("Hull_Flame_1st_Half")  = {21,22,23,24}; 
Physical Surface("Hull_Flame_2nd_Half")  = {25,26,27,28};
Physical Surface("Hull_Post-Flame") = {29,30,31,32}; 


/////////////////// VOLUMES ///////////////////
////// SURFACE LOOPS
// Pre-Flame
Surface Loop(38) = {33,17,18,19,20,35};
// Flame-First Half
Surface Loop(39) = {35,21,22,23,24,36};
// Flame-Second Half
Surface Loop(40) = {36,25,26,27,28,37};
// Post-Flame
Surface Loop(41) = {37,29,30,31,32,34};

////// VOLUMES
Volume(42) = {38};
Volume(43) = {39};
Volume(44) = {40};
Volume(45) = {41};

////// LABEL
Physical Volume("Pre-Flame")  = {42};
Physical Volume("Flame_1stHalf") = {43};
Physical Volume("Flame_2ndHalf") = {44};
Physical Volume("Flame") = {43,44};
Physical Volume("Post-Flame") = {45};
Physical Volume("Interior")   = {42,43,44,45};
