
l1 = 0.45*0.5;
l2 = 0.1*0.5;
l3 = 0.45*0.5;


r = 0.01;

c = 0.1/10;
f = c;

/////////////////// POINTS ///////////////////
// Inlet
Point(1)  = {0,0,-l1,c};
Point(2)  = {r,0,-l1,c};
Point(3)  = {0,r,-l1,c};
Point(4)  = {-r,0,-l1,c};
Point(5)  = {0,-r,-l1,c};

// FlameIn
Point(6)  = {0,0,0,f};
Point(7)  = {r,0,0,f};
Point(8)  = {0,r,0,f};
Point(9)  = {-r,0,0,f};
Point(10)  = {0,-r,0,f};

// FlameOut
Point(11)  = {0,0,l2,f};
Point(12)  = {r,0,l2,f};
Point(13)  = {0,r,l2,f};
Point(14)  = {-r,0,l2,f};
Point(15)  = {0,-r,l2,f};

// Outlet
Point(16)  = {0,0,l2+l3,c};
Point(17)  = {r,0,l2+l3,c};
Point(18)  = {0,r,l2+l3,c};
Point(19)  = {-r,0,l2+l3,c};
Point(20)  = {0,-r,l2+l3,c};


/////////////////// LINES ///////////////////
///// CIRCLES
// Inlet
Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};

// Flame in
Circle(5) = {7,6,8};
Circle(6) = {8,6,9};
Circle(7) = {9,6,10};
Circle(8) = {10,6,7};

// Flame out
Circle(9)  = {12,11,13};
Circle(10) = {13,11,14};
Circle(11) = {14,11,15};
Circle(12) = {15,11,12};

// Outlet
Circle(13) = {17,16,18};
Circle(14) = {18,16,19};
Circle(15) = {19,16,20};
Circle(16) = {20,16,17};


////// STRAIGHT LINES
// Pre-Flame
Line(21) = {2,7};
Line(22) = {4,9};
Line(23) = {3,8};
Line(24) = {5,10};

// Flame
Line(25) = {7,12};
Line(26) = {9,14};
Line(27) = {8,13};
Line(28) = {10,15};

// Post-Flame
Line(29) = {12,17};
Line(30) = {14,19};
Line(31) = {13,18};
Line(32) = {15,20};

/////////////////// Line Loops ///////////////////
///// LINE LOOPS 
// Inlet
Line Loop(1) = {1,2,3,4}; 

// FlameIn
Line Loop(2) = {5,6,7,8};

// FlameOut
Line Loop(3) = {9,10,11,12};

// Outlet
Line Loop(4) = {13,14,15,16};

// Pre-Flame Cylinder Surface
Line Loop(5) = {3,24,-7,-22};
Line Loop(6) = {2,22,-6,-23};
Line Loop(7) = {4,21,-8,-24};
Line Loop(8) = {1,23,-5,-21};

// Flame Cylinder Surface
Line Loop(9) = {7,28,-11,-26};
Line Loop(10) = {8,25,-12,-28};
Line Loop(11) = {5,27,-9,-25};
Line Loop(12) = {6,26,-10,-27};

// Post-Flame Cylinder Surface
Line Loop(13) = {11,32,-15,-30};
Line Loop(14) = {12,29,-16,-32};
Line Loop(15) = {9,31,-13,-29};
Line Loop(16) = {10,30,-14,-31};



////////////////// SURFACES //////////////////
Plane Surface(1) = {1}; // Inlet
Plane Surface(2) = {2}; // Flame Inlet
Plane Surface(3) = {3}; // Flame Outlet
Plane Surface(4) = {4}; // Outlet

// Ruled Surfaces 
// Pre-Flame
Ruled Surface(5) = {5};
Ruled Surface(6) = {6};
Ruled Surface(7) = {7};
Ruled Surface(8) = {8};
// Flame
Ruled Surface(9) = {9};
Ruled Surface(10) = {10};
Ruled Surface(11) = {11};
Ruled Surface(12) = {12};
// Post-Flame
Ruled Surface(13) = {13};
Ruled Surface(14) = {14};
Ruled Surface(15) = {15};
Ruled Surface(16) = {16};

// Label the Surfaces
Physical Surface("Inlet")        = {1}; 
Physical Surface("Outlet")       = {4};
Physical Surface("FlameIn")      = {2}; 
Physical Surface("FlameOut")     = {4};  
Physical Surface("Hull_Pre-Flame")  = {5,6,7,8}; 
Physical Surface("Hull_Flame")  = {9,10,11,12}; 
Physical Surface("Hull_Post-Flame") = {13,14,15,16}; 


/////////////////// VOLUMES ///////////////////
////// SURFACE LOOPS
// Pre-Flame
Surface Loop(1) = {1,2,5,6,7,8};
// Flame
Surface Loop(2) = {2,3,9,10,11,12};
// Post-Flame
Surface Loop(3) = {3,4,13,14,15,16};

////// VOLUMES
Volume(1) = {1};
Volume(2) = {2};
Volume(3) = {3};

////// LABEL
Physical Volume("Pre-Flame")  = {1};
Physical Volume("Flame") = {2};
Physical Volume("Post-Flame") = {3};
Physical Volume("Interior")   = {1,2,3};






