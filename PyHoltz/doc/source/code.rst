Auto Generated Documentation
============================

.. autoclass:: FEM.mesh_class.mesh
   :members: 

.. autoclass:: FEM.discretization.discretization
   :members:

.. autoclass:: NLEVP.parameter.para_model
   :members:

.. autofunction:: NLEVP.Nicoud.iteration

.. autofunction:: NLEVP.Newton.iteration

.. autofunction:: NLEVP.inverse_iter.inverse_iter

.. autofunction:: NLEVP.Householder.minres

.. autofunction:: NLEVP.Halley.iteration


