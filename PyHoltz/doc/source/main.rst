.. PyHoltz documentation master file, created by
   sphinx-quickstart on Wed Jun  7 10:26:10 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyHoltz's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2
	
   intro
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

