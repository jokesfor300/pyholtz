Introduction
============

PyHoltz is solver for the 3D thermoacoustic Helmholtz equation by FEM to
perform linear stability analyses of thermoacoustic instabilities.
PyHoltz provides routines for mesh handling and capabilites for 
the study of annular combustion chambers from a unit cell. These capabilities
include Bloch-wave theory for computations on a reference cell and mesh handling
to compute a full annular setup for comparison.
The linear stability analysis is supplement with powerful routines to
perform sensitivity analysis to input parameters by adjoint methods,
based on high-order perturbation of eigenvalues- and eigenfunctions.
PyHoltz provides two frameworks to solve the nonlinear eigenvalue problem 
resulting from the discretisation of the underlying PDE:
fixed-point methods and contour-integration.

Copyright (C) 2017  Georg A. Mensah, Philip E. Buschmann, Jonas P. Moeck

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
