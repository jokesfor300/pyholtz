#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Thu Oct 27 13:00:43 2016

@author: mrmorningstar
"""


from scipy import eye, rand, zeros, matrix, diag, logical_not, imag, real, shape
from scipy.linalg import  svd, eig #,solve
from scipy.sparse.linalg import spsolve
from scipy import sparse
import cmath as cmath
from math import ceil, pi
import numpy as np

#%% Multi-EW variant of the circular integration
def Circ(L, z0, R, K = 10, l = 25, N = 25, tol_rank = 1E-10):
    ''' Resolvent integral method for the case where there are more eigenvalues than the system dimension
    '''
    ### 0. Supplementary routines
    def get_tuple(p):
        locations = []
        for run in np.arange(p + 1):
            locations.append(np.array([run,(p + 1) - run - 1]))
        return np.array(locations)
    
    #### 1. Initialisation
    m     = L(0.0).shape[0]
    l     = l # m
    V_hat = eye(m,l) # rand(m,l)#
    B0    = np.zeros((K*m,K*l))*0.0*1j
    B1    = np.zeros((K*m,K*l))*0.0*1j
    
    #### 2. Assembly of the B-matrices via the matrices A0 & A1
    down_B0 = 0
    down_B1 = 0
    for p in np.arange(0, (2*K - 1) + 1):
        print('Assembling: p = ' + str(p))
        
        # 2.1. Assemble A0 and A1 as circular contour integral
        A = np.zeros((m,l))*0.0*1j
        for j in range(N):
            Ls = sparse.csr_matrix(L(z0+R*cmath.exp(2*j*cmath.pi*1j/N)))
            LinvV = spsolve(Ls,V_hat)
            A     = A + LinvV*((z0 + R*cmath.exp(2*j*cmath.pi*1j/N))**p) * cmath.exp(2*j*cmath.pi*1j/N)
        A = A*R/N

        # 2.2. Assembly of B0:
        if p < K:
            locations = get_tuple(p)
            for loc in locations:
                B0[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A 
        else:
            down_B0 += 1
            locations = get_tuple(K - p + 2) + (down_B0)
            for loc in locations:
                B0[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A
                
                
        # 2.3. Assembly of B1:
        if p < K + 1:
            locations = get_tuple(p - 1)
            for loc in locations:
                #print(loc)
                B1[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A 
        else:
            down_B1 += 1
            locations = get_tuple(K  + 1 - p + 2 ) + (down_B1)
            for loc in locations:
                #print(loc)
                B1[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A
    
    #### 3. Compute SVD
    V, Sigma, Wh = svd(B0,full_matrices=False)
    
    #print(np.shape(V))
    #print(np.shape(Wh))
    
    #### 4. Compute rank test
    #print(tol_rank)
    print(Sigma)
#    print(Sigma>tol_rank)
    Sigma = Sigma[Sigma>tol_rank];
    k     = len(Sigma)
    if k>=l*K:
        print('Rank-Test FAILED')
        rankTestFlag = 1
    else:
        rankTestFlag = 0
    #print(k)
    V  = V[0:K*m,0:k];
    W  = Wh[0:k,0:K*m]
    W  = matrix(W)
    W  = W.H
    Wh = Wh[0:k,0:K*l];
    V  = matrix(V)
    Wh = matrix(Wh)
    
    
    #print('Km ' + str(K*m))
    #print('Kl ' + str(K*l))
    
    #print(np.shape(V.H))
    #print(np.shape(B1))
    #print(np.shape(W.H))
    
    #### 5. Assmeble matrix D and compute its eigenvalues
    lam,v  = eig(V.H*B1*Wh.H*diag(1/Sigma), left=False, right=True)

    v=V*v
    
    mask = abs(lam-z0) < R
    lamOut = lam[logical_not(mask)]
    vOut   = v[:,logical_not(mask)]
    
    #print(lamOut)#/cmath.pi/2.0)    
    
    lam = lam[mask]
    v   = v[:,mask]
    #print(lamOut/pi/2)
    # w=W*w
    #ToDo: adjoint
    return lam,v, rankTestFlag

#%% this is the Beyn method for an arbitary contour \Gamma and for the possibility of more EW than the system dimension
def Beyn(T, phi, K = 4, l = 25, tol_rank = 1E-10, doprint = False):
    ''' Resolvent integral method for the case where there are more eigenvalues than the system dimension
    '''
    ### 0. Supplementary routines
    def get_tuple(p):
        locations = []
        for run in np.arange(p + 1):
            locations.append(np.array([run,(p + 1) - run - 1]))
        return np.array(locations)
    
    #### 1. Initialisation
    m     = T(0.0).shape[0]
    l     = l#m
    N     = len(phi)
    V_hat = eye(m,l) # rand(m,l)#
    B0    = np.zeros((K*m,K*l))*0.0*1j
    B1    = np.zeros((K*m,K*l))*0.0*1j
    
    #### 2. Assembly of the B-matrices via the matrices A0 & A1
    down_B0 = 0
    down_B1 = 0
    for p in np.arange(0, (2*K - 1) + 1):
        if doprint:
            print('Assembling: p = ' + str(p))
        # 2.0. Prepare the values
        vals = []
        for k in np.arange(0,N):
            vals.append(spsolve(T(phi[k]),V_hat))    
        
        
        # 2.1. Assemble matrix A
        A = zeros((m,l))   
        for k in np.arange(0,N):
            if k == (N-1):
                A = A + ((phi[0]**p)*vals[0] + (phi[k]**p)*vals[k])*(phi[0] - phi[k])
            else:
                A = A + ((phi[k+1]**p)*vals[k+1] + (phi[k]**p)*vals[k])*(phi[k+1] - phi[k])
        A = 0.5*A/(cmath.pi*2.0*1j)

        # 2.2. Assembly of B0:
        if p < K:
            locations = get_tuple(p)
            for loc in locations:
                B0[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A 
        else:
            down_B0 += 1
            locations = get_tuple(K - p + 2) + (down_B0)
            for loc in locations:
                B0[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A
                
                
        # 2.3. Assembly of B1:
        if p < K + 1:
            locations = get_tuple(p - 1)
            for loc in locations:
                #print(loc)
                B1[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A 
        else:
            down_B1 += 1
            locations = get_tuple(K  + 1 - p + 2 ) + (down_B1)
            for loc in locations:
                #print(loc)
                B1[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A
    
    #### 3. Compute SVD
    V, Sigma, Wh = svd(B0,full_matrices=False)
    
    #### 4. Compute rank test
    Sigma = Sigma[Sigma>tol_rank];
    k     = len(Sigma)
    if k>=l*K:
        print('Rank-Test FAILED')
        rankTestFlag = 1
    else:
        rankTestFlag = 0
    # old
    V  = V[0:K*m,0:k];
    W  = Wh[0:k,0:K*m]
    W  = matrix(W)
    W  = W.H
    Wh = Wh[0:k,0:K*l];
    V  = matrix(V)
    Wh = matrix(Wh)
    
    #### 5. Assmeble matrix D and compute its eigenvalues
    lam,v  = eig(V.H*B1*Wh.H*diag(1/Sigma), left=False, right=True)

    #### 6. Extract eigenvalues inside rectangular contour and get the actual eigenvectors ####
    v=V*v
    
    z0   = np.min(np.real(phi)) + np.min(np.imag(phi))*1j
    z1   = np.max(np.real(phi)) + np.max(np.imag(phi))*1j
    mask = ((imag(lam-z0) > 0.0 ) * (real(lam-z0) > 0.0)) * ((imag(lam-z1) < 0.0) * (real(lam-z1) < 0.0))
    
    #lamOut = lam[logical_not(mask)]
    #vOut   = v[:,logical_not(mask)]
       
    lam = lam[mask]
    v   = v[:,mask]
    
    # Extract the real eigenvectors
    v  = v[0:m,:]
    
    return lam,v, rankTestFlag
    
#%% Multi-EW Version of the standard rectangular-beyn
#def Rect(T, phi, K = 4, l = 25, tol_rank = 1E-10):
#    ''' Resolvent integral method for the case where there are more eigenvalues than the system dimension
#    '''
#    ### 0. Supplementary routines
#    def get_tuple(p):
#        locations = []
#        for run in np.arange(p + 1):
#            locations.append(np.array([run,(p + 1) - run - 1]))
#        return np.array(locations)
#    
#    #### 1. Initialisation
#    m     = T(0.0).shape[0]
#    l     = m
#    N     = len(phi)
#    V_hat = rand(m,l)#eye(m,l) 
#    B0    = np.zeros((K*m,K*l))*0.0*1j
#    B1    = np.zeros((K*m,K*l))*0.0*1j
#    
#    #### 2. Assembly of the B-matrices via the matrices A0 & A1
#    down_B0 = 0
#    down_B1 = 0
#    for p in np.arange(0, (2*K - 1) + 1):
#        print('Assembling: p = ' + str(p))
#        # 2.0. Prepare the values
#        vals = []
#        for k in np.arange(0,N):
#            vals.append(spsolve(T(phi[k]),V_hat))    
#        
#        
#        # 2.1. Assemble matrix A
#        A = zeros((m,l))   
#        for k in np.arange(0,N):
#            if k == (N-1):
#                A = A + ((phi[0]**p)*vals[0] + (phi[k]**p)*vals[k])*(phi[0] - phi[k])
#            else:
#                A = A + ((phi[k+1]**p)*vals[k+1] + (phi[k]**p)*vals[k])*(phi[k+1] - phi[k])
#        
#        # 2.2. Assembly of B0:
#        if p < K:
#            locations = get_tuple(p)
#            for loc in locations:
#                B0[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A 
#        else:
#            down_B0 += 1
#            locations = get_tuple(K - p + 2) + (down_B0)
#            for loc in locations:
#                B0[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A
#                
#                
#        # 2.3. Assembly of B1:
#        if p < K + 1:
#            locations = get_tuple(p - 1)
#            for loc in locations:
#                #print(loc)
#                B1[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A 
#        else:
#            down_B1 += 1
#            locations = get_tuple(K  + 1 - p + 2 ) + (down_B1)
#            for loc in locations:
#                #print(loc)
#                B1[(loc[1])*m:(loc[1]+1)*(m),(loc[0])*l:(loc[0]+1)*(l)] = A
#    
#    #### 3. Compute SVD
#    V, Sigma, Wh = svd(B0,full_matrices=False)
#    
#    #print(np.shape(V))
#    #print(np.shape(Wh))
#    
#    #### 4. Compute rank test
#    #print(tol_rank)
#    #print(Sigma)
##    print(Sigma>tol_rank)
#    Sigma = Sigma[Sigma>tol_rank];
#    k     = len(Sigma)
#    if k>=l*K:
#        print('Rank-Test FAILED')
#        rankTestFlag = 1
#    else:
#        rankTestFlag = 0
#    #print(k)
#    V  = V[0:K*m,0:k];
#    W  = Wh[0:k,0:K*m]
#    W  = matrix(W)
#    W  = W.H
#    Wh = Wh[0:k,0:K*l];
#    V  = matrix(V)
#    Wh = matrix(Wh)
#    
#    
#    #print('Km ' + str(K*m))
#    #print('Kl ' + str(K*l))
#    
#    #print(np.shape(V.H))
#    #print(np.shape(B1))
#    #print(np.shape(W.H))
#    
#    #### 5. Assmeble matrix D and compute its eigenvalues
#    lam,v  = eig(V.H*B1*Wh.H*diag(1/Sigma), left=False, right=True)
#
#    v=V*v
#    
#    z0 = np.min(np.real(phi)) + np.min(np.imag(phi))*1j
#    z1 = np.max(np.real(phi)) + np.max(np.imag(phi))*1j
#    mask = ((imag(lam-z0) > 0.0 ) * (real(lam-z0) > 0.0)) * ((imag(lam-z1) < 0.0) * (real(lam-z1) < 0.0))
#    
#    lamOut = lam[logical_not(mask)]
#    vOut   = v[:,logical_not(mask)]
#    
#    #print(lamOut)#/cmath.pi/2.0)    
#    
#    lam = lam[mask]
#    v   = v[:,mask]
#    #print(lamOut/pi/2)
#    # w=W*w
#    #ToDo: adjoint
#    return lam,v, rankTestFlag


