# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
@authors: Georg Mensah, Alessandro Orchini TU Berlin 2017
Collection of routines to evaluate functions and their derivatives for a fast calculation 
of high order power expansions
"""

import numpy as np
import cmath as cmath
from scipy.optimize import fsolve, minimize
from scipy.special import binom, factorial


class polynomial:
    def __init__(self, coeffs):
        self.Coeffs = np.array(coeffs)
        self.N = len(coeffs)

    def get_Coeffs(self):
        return self._Coeffs

    def set_Coeffs(self, coeffs):
        Coeffs = np.array(coeffs)
        while len(Coeffs) > 1 and Coeffs[-1] == 0:
            Coeffs = Coeffs[:-1]
        self._Coeffs = Coeffs
        self._N = len(coeffs)

    def get_N(self):
        return self._N

    def set_N(self, N):
        pass

    N = property(get_N, set_N)
    Coeffs = property(get_Coeffs, set_Coeffs)

    def eval(self, z):
        return sum(self.Coeffs * z ** np.arange(self.N))

    def __call__(self, z):
        return self.eval(z)

    def __add__(self, other):
        if self.N < other.N:
            Sum = np.hstack((self.Coeffs, [0] * (other.N - self.N))) + other.Coeffs

        elif self.N > other.N:
            Sum = self.Coeffs - np.hstack((other.Coeffs, [0] * (self.N - other.N)))
        else:
            Sum = self.Coeffs + other.Coeffs

        return polynomial(Sum)

    def __sub__(self, other):
        if self.N < other.N:
            Diff = np.hstack((self.Coeffs, [0] * (other.N - self.N))) - other.Coeffs

        elif self.N > other.N:
            Diff = self.Coeffs - np.hstack((other.Coeffs, [0] * (self.N - other.N)))
        else:
            Diff = self.Coeffs - other.Coeffs

        return polynomial(Diff)

    def __mul__(self, other):
        Prod = np.zeros((self.N - 1) + (other.N - 1) + 1)
        A = self.Coeffs
        B = other.Coeffs
        for i in range(self.N):
            for j in range(other.N):
                Prod[i + j] = Prod[i + j] + A[i] * B[j]

        return polynomial(Prod)

    def derive(self):
        if self.N > 1:
            Derivative = np.zeros(self.N - 1)
            Coeffs = self.Coeffs
            for i in range(self.N - 1):
                Derivative[i] = Coeffs[i + 1] * (i + 1)
        else:
            Derivative = [0]

        return polynomial(Derivative)

    def __invert__(self):
        # use tilde for derivative
        return self.derive()

    def __str__(self):
        return str(self.Coeffs)

    def __repr__(self):
        if self.N == 1:
            symbol = str(self._Coeffs[0])
        else:
            symbol = ''
            if self._Coeffs[0] != 0: symbol = symbol + '+' + str(self._Coeffs[0])
            for i in range(1, self.N):
                if self._Coeffs[i] != 0:
                    symbol = symbol + '+'
                    if self._Coeffs[i] != 1: symbol = symbol + str(self._Coeffs[i])
                    symbol = symbol + 'z**' + str(i)
            symbol = symbol[1:]
        return symbol


class rational:
    def __init__(self, num, den):  # TODO: kürzen
        self.Num = num
        self.Den = den

    def get_Num(self):
        return self._Num

    def set_Num(self, num):
        if not isinstance(num, polynomial): num = polynomial(num)  # TODO: fix polynomial usage
        self._Num = num

    def get_Den(self):
        return self._Num

    def set_Den(self, den):
        if not isinstance(den, polynomial): den = polynomial(den)
        self._Den = den

    Num = property(get_Num, set_Num)
    Den = property(get_Den, set_Den)

    def eval(self, z):
        return self._Num(z) / self._Den(z)

    def __call__(self, z):
        return self.eval(z)

    def derive(self):
        num = self._Num.derive() * self._Den - self._Num * self._Den.derive()
        den = self._Den * self._Den
        return rational(num._Coeffs, den._Coeffs)

    def __invert__(self):
        # use tilde for derivative
        return self.derive()

    def __str__(self):
        return str(self._Num) + '/' + str(self._Den)

    def __repr__(self):
        return repr(self._Num) + '/' + repr(self._Den)


class ntau2:
    def __init__(self, n, tau):
        self.n = n
        self.tau = tau

    def __call__(self, epsilon, eps_deriv, omega, omega_deriv):
        #        n=self.n
        #        tau=self.tau
        epsilon = np.array([epsilon])
        if np.prod(epsilon.shape) == 1:  # len(epsilon)==1:
            n = epsilon[0]
            tau = self.tau

        elif np.prod(epsilon.shape) == 2:  # len(epsilon)==2:
            n = epsilon[0][0]
            tau = epsilon[0][1]

        if eps_deriv == [0, 0] or eps_deriv == 0:
            if omega_deriv == 0:
                return n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * tau * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * tau ** 2 * n * cmath.exp(-1j * omega * tau)

        elif eps_deriv == [1, 0] or eps_deriv == 1:
            if omega_deriv == 0:
                return cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * tau * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * tau ** 2 * cmath.exp(-1j * omega * tau)

        elif eps_deriv == [0, 1]:
            if omega_deriv == 0:
                return -1j * omega * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * (-1j * omega * tau + 1) * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * (-1j * omega * tau ** 2 + 2 * tau) * n * cmath.exp(-1j * omega * tau)
        elif eps_deriv == [2, 0] or eps_deriv == 2:
            return 0

        elif eps_deriv == [1, 1]:
            if omega_deriv == 0:
                return -1j * omega * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * (-1j * omega * tau + 1) * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * (-1j * omega * tau ** 2 + 2 * tau) * cmath.exp(-1j * omega * tau)

        elif eps_deriv == [0, 2]:
            if omega_deriv == 0:
                return -1 * omega ** 2 * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * (-1j * omega) * n * cmath.exp(-1j * omega * tau) - 1 * (
                        -1j * omega ** 2 * tau + omega) * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * (-1j * omega * tau * 2 + 2) * n * cmath.exp(-1j * omega * tau) + 1j * (
                        -1j * omega ** 2 * tau ** 2 + 2 * tau * omega) * n * cmath.exp(-1j * omega * tau)
        else:
            raise ValueError('argh this derivative is not implemented')


class ntau2tau:
    def __init__(self, n, tau):
        self.n = n
        self.tau = tau

    def __call__(self, epsilon, eps_deriv, omega, omega_deriv):
        #        n=self.n
        #        tau=self.tau
        epsilon = np.array([epsilon])
        if np.prod(epsilon.shape) == 1:  # len(epsilon)==1:
            tau = epsilon[0]
            n = self.n

        elif np.prod(epsilon.shape) == 2:  # len(epsilon)==2:
            n = epsilon[0][1]
            tau = epsilon[0][0]

        if eps_deriv == [0, 0] or eps_deriv == 0:
            if omega_deriv == 0:
                return n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * tau * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * tau ** 2 * n * cmath.exp(-1j * omega * tau)

        elif eps_deriv == [0, 1]:
            if omega_deriv == 0:
                return cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * tau * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * tau ** 2 * cmath.exp(-1j * omega * tau)

        elif eps_deriv == [1, 0] or eps_deriv == 1:
            if omega_deriv == 0:
                return -1j * omega * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * (-1j * omega * tau + 1) * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * (-1j * omega * tau ** 2 + 2 * tau) * n * cmath.exp(-1j * omega * tau)
        elif eps_deriv == [0, 2]:
            return 0

        elif eps_deriv == [1, 1]:
            if omega_deriv == 0:
                return -1j * omega * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * (-1j * omega * tau + 1) * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * (-1j * omega * tau ** 2 + 2 * tau) * cmath.exp(-1j * omega * tau)

        elif eps_deriv == [2, 0] or eps_deriv == 2:
            if omega_deriv == 0:
                return -1 * omega ** 2 * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 1:
                return -1j * (-1j * omega) * n * cmath.exp(-1j * omega * tau) - 1 * (
                        -1j * omega ** 2 * tau + omega) * n * cmath.exp(-1j * omega * tau)
            elif omega_deriv == 2:
                return -1 * (-1j * omega * tau * 2 + 2) * n * cmath.exp(-1j * omega * tau) + 1j * (
                        -1j * omega ** 2 * tau ** 2 + 2 * tau * omega) * n * cmath.exp(-1j * omega * tau)
        else:
            raise ValueError('argh this derivative is not implemented')


def pow0(x, n):
    if n > 0:
        return 0
    else:
        return 1


def pow1(x, n):
    if n > 1:
        return 0
    elif n == 1:
        return 1
    else:
        return x


def pow2(x, n):
    if n > 2:
        return 0
    elif n == 2:
        return 2
    elif n == 1:
        return 2 * x
    else:
        return x ** 2


def pow0_2(x, n):
    if isinstance(n, int):
        return pow0(x, n)

    else:
        if sum(n) != 0:
            return 0
        else:
            return 1


def powk(x, n, k):
    if isinstance(k, int) and k > 0 and n > k:
        f = 0
    else:
        f = 1
        i = k
        for j in range(n):
            f *= i
            i -= 1
        f *= x ** (k - n)
    return f


def pow_a(a):
    def func(x, n):
        return powk(x, n, a)

    return func


def exp_a(a):
    def exp(x, n):
        return a ** n * np.exp(a * x)

    return exp


def monoratio(a):
    def mratio(x, n):
        return (-1) ** n * factorial(n) * (a + x) ** (-n - 1)

    return mratio


def monoratiok(a, k):
    def mratio(x, n):
        return (-1) ** n * np.prod(np.r_[k:(k + n)]) * (a + x) ** (-n - k)

    return mratio


def exp_delay(omega, n, tau):
    return (-1j * tau) ** n * cmath.exp(-1j * omega * tau)


def tau_delay(omega, tau, m, n):
    a = -1j
    u = lambda x, l: powk(x, l, m)
    f = 0
    for i in range(n + 1):
        f += binom(n, i) * u(tau, i) * (a * omega) ** (n - i)
    f *= a ** m * np.exp(a * omega * tau)
    return f


def inv_sigma(a, x0):
    f = lambda x: 1 / (1 + np.exp((x - x0) * a))

    def function(x, n):
        if n == 0:
            return 1 / (np.exp(a * (x - x0)) + 1)

        elif n == 1:
            return -a * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2
        elif n == 2:
            return a ** 2 * np.exp(a * (x - x0)) * np.tanh(a * (x - x0) / 2) / (np.exp(a * (x - x0)) + 1) ** 2
        elif n == 3:
            return a ** 3 * (-(np.exp(a * (x - x0)) + 1) ** 2 + 6 * (np.exp(a * (x - x0)) + 1) * np.exp(
                a * (x - x0)) - 6 * np.exp(2 * a * (x - x0))) * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 4
        elif n == 4:
            return a ** 4 * (-(np.exp(a * (x - x0)) + 1) ** 3 + 14 * (np.exp(a * (x - x0)) + 1) ** 2 * np.exp(
                a * (x - x0)) - 36 * (np.exp(a * (x - x0)) + 1) * np.exp(2 * a * (x - x0)) + 24 * np.exp(
                3 * a * (x - x0))) * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 5
        elif n == 5:
            return a ** 5 * (-(np.exp(a * (x - x0)) + 1) ** 4 + 30 * (np.exp(a * (x - x0)) + 1) ** 3 * np.exp(
                a * (x - x0)) - 150 * (np.exp(a * (x - x0)) + 1) ** 2 * np.exp(2 * a * (x - x0)) + 240 * (
                                     np.exp(a * (x - x0)) + 1) * np.exp(3 * a * (x - x0)) - 120 * np.exp(
                4 * a * (x - x0))) * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 6
        elif n == 6:
            return a ** 6 * (
                    -1 + 62 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 540 * np.exp(2 * a * (x - x0)) / (
                    np.exp(a * (x - x0)) + 1) ** 2 + 1560 * np.exp(3 * a * (x - x0)) / (
                            np.exp(a * (x - x0)) + 1) ** 3 - 1800 * np.exp(4 * a * (x - x0)) / (
                            np.exp(a * (x - x0)) + 1) ** 4 + 720 * np.exp(5 * a * (x - x0)) / (
                            np.exp(a * (x - x0)) + 1) ** 5) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 7:
            return a ** 7 * (-1 + 126 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 1806 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 8400 * np.exp(3 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 3 - 16800 * np.exp(4 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 4 + 15120 * np.exp(5 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 5 - 5040 * np.exp(6 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 6) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 8:
            return a ** 8 * (-1 + 254 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 5796 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 40824 * np.exp(3 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 3 - 126000 * np.exp(4 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 4 + 191520 * np.exp(5 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 5 - 141120 * np.exp(6 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 6 + 40320 * np.exp(7 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 7) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 9:
            return a ** 9 * (-1 + 510 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 18150 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 186480 * np.exp(3 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 3 - 834120 * np.exp(4 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 4 + 1905120 * np.exp(5 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 5 - 2328480 * np.exp(6 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 6 + 1451520 * np.exp(7 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 7 - 362880 * np.exp(8 * a * (x - x0)) / (
                                     np.exp(a * (x - x0)) + 1) ** 8) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 10:
            return a ** 10 * (-1 + 1022 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 55980 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 818520 * np.exp(3 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 3 - 5103000 * np.exp(4 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 4 + 16435440 * np.exp(5 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 5 - 29635200 * np.exp(6 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 6 + 30240000 * np.exp(7 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 7 - 16329600 * np.exp(8 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 8 + 3628800 * np.exp(9 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 9) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 11:
            return a ** 11 * (-1 + 2046 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 171006 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 3498000 * np.exp(3 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 3 - 29607600 * np.exp(4 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 4 + 129230640 * np.exp(5 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 5 - 322494480 * np.exp(6 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 6 + 479001600 * np.exp(7 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 7 - 419126400 * np.exp(8 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 8 + 199584000 * np.exp(9 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 9 - 39916800 * np.exp(10 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 10) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 12:
            return a ** 12 * (-1 + 4094 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 519156 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 14676024 * np.exp(3 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 3 - 165528000 * np.exp(4 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 4 + 953029440 * np.exp(5 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 5 - 3162075840 * np.exp(6 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 6 + 6411968640 * np.exp(7 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 7 - 8083152000 * np.exp(8 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 8 + 6187104000 * np.exp(9 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 9 - 2634508800 * np.exp(10 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 10 + 479001600 * np.exp(11 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 11) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 13:
            return a ** 13 * (-1 + 8190 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 1569750 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 60780720 * np.exp(3 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 3 - 901020120 * np.exp(4 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 4 + 6711344640 * np.exp(5 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 5 - 28805736960 * np.exp(6 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 6 + 76592355840 * np.exp(7 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 7 - 130456085760 * np.exp(8 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 8 + 142702560000 * np.exp(9 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 9 - 97037740800 * np.exp(10 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 10 + 37362124800 * np.exp(11 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 11 - 6227020800 * np.exp(12 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 12) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 14:
            return a ** 14 * (-1 + 16382 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 4733820 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 249401880 * np.exp(3 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 3 - 4809004200 * np.exp(4 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 4 + 45674188560 * np.exp(5 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 5 - 248619571200 * np.exp(6 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 6 + 843184742400 * np.exp(7 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 7 - 1863435974400 * np.exp(8 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 8 + 2731586457600 * np.exp(9 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 9 - 2637143308800 * np.exp(10 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 10 + 1612798387200 * np.exp(
                11 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 11 - 566658892800 * np.exp(12 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 12 + 87178291200 * np.exp(13 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 13) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2
        elif n == 15:
            return a ** 15 * (-1 + 32766 * np.exp(a * (x - x0)) / (np.exp(a * (x - x0)) + 1) - 14250606 * np.exp(
                2 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 2 + 1016542800 * np.exp(3 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 3 - 25292030400 * np.exp(4 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 4 + 302899156560 * np.exp(5 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 5 - 2060056318320 * np.exp(6 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 6 + 8734434508800 * np.exp(7 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 7 - 24359586451200 * np.exp(8 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 8 + 45950224320000 * np.exp(9 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 9 - 59056027430400 * np.exp(
                10 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 10 + 50999300352000 * np.exp(11 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 11 - 28332944640000 * np.exp(
                12 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 12 + 9153720576000 * np.exp(13 * a * (x - x0)) / (
                                      np.exp(a * (x - x0)) + 1) ** 13 - 1307674368000 * np.exp(
                14 * a * (x - x0)) / (np.exp(a * (x - x0)) + 1) ** 14) * np.exp(a * (x - x0)) / (
                           np.exp(a * (x - x0)) + 1) ** 2

    return function


# [3.108148442701161e+29, 2.3385097556549368e+27, 7.30640269946154e+24, 1.7874854516267292e+22, 2.550750286656444e+19, 3.180129880158837e+16, 25750617043413.23, 17675079309.69877, 9350373.607644955, 2877.319649867614, 1.0]
den = [3.1049375383908199e+029, 2.3380717339601936e+027, 7.3038915400774745e+024, 1.7874037959448787e+022,
       25503576613735743000, 31802192925377248, 25748871765554.695, 17676256817.367859, 9350191.7487609554,
       2877.5856338114909, 1]
# [3.006417459737476e+29, 1.4855636746977687e+27, -1.9424164088238214e+24, 7.115615489045119e+21, -1.3634864112477917e+19, 1.05209150629321e+16, -12589465715223.61, 5111177799.787564, -3918313.743109674, 680.0162435257434, -0.3796194247876642]
num = [3.0031514962493997e+029, 1.4863269277848269e+027, -1.944348933839247e+024, 7.1192614914499742e+021,
       -13638843058793906000, 10524655149683706, -12592153721955.291, 5112575280.146472, -3918994.6409264896,
       680.17759357378884, -0.3796753279031021]


def rat_FTF(num=num, den=den):
    # takes numerator and denominator for system TF
    r = rational(num, den)

    def function(x, n):
        a = r
        for i in range(0, n):
            a = ~a
        return (1j) ** n * a(1j * x)

    return function


def polyfunc(coeffs):
    p = polynomial(coeffs)

    def function(x, n):
        a = p
        for i in range(0, n):
            a = ~a
        return a(x)

    return function


def rat_FTF_delayed(delay):
    Rat = rat_FTF()
    # R=lambda x,n:Rat(np.real(x),n)
    R = lambda x, n: Rat(x, n)

    def function(x, n):
        if n == 0:
            return R(x, 0) * exp_delay(x, 0, delay)
        elif n == 1:
            return R(x, 1) * exp_delay(x, 0, delay) + R(x, 0) * exp_delay(x, 1, delay)
        elif n == 2:
            return R(x, 2) * exp_delay(x, 0, delay) + 2 * R(x, 1) * exp_delay(x, 1, delay) + R(x, 0) * exp_delay(x, 2,
                                                                                                                 delay)
        elif n == 3:
            return R(x, 3) * exp_delay(x, 0, delay) + 3 * R(x, 2) * exp_delay(x, 1, delay) + 3 * R(x, 1) * exp_delay(x,
                                                                                                                     2,
                                                                                                                     delay) + R(
                x, 0) * exp_delay(x, 3, delay)

    return function


def gain_phase_error(epsilon, deriv):
    eps = np.array(epsilon)
    g = eps[1]
    phi = eps[0]
    # print('Juhu',epsilon,deriv)
    if deriv == [0, 0] or deriv == 0:
        return (1 + g) * np.exp(-1j * phi)
    elif deriv == [1, 0]:
        return -1j * (1 + g) * np.exp(-1j * phi)
    elif deriv == [2, 0]:
        return -(1 + g) * np.exp(-1j * phi)
    elif deriv == [0, 1]:
        return np.exp(-1j * phi)
    elif deriv == [0, 2]:
        return 0
    elif deriv == [1, 1]:
        return -1j * np.exp(-1j * phi)


# optimization
def opt(f, x0, what='root', cplx=True, bnds=None):
    if cplx:
        # create wrapper
        def fun(x):
            x_real = x[0]
            x_imag = x[1]
            y = f(x_real + 1j * x_imag)
            y = np.array([np.real(y), np.imag(y)])
            return y

        x0 = np.array([np.real(x0), np.imag(x0)])
        if cplx: bnbnds = [(0, 1), (0, 1)]

    else:
        fun = f
        bnds = [(0, 1)]

    if what == 'root':
        x0 = fsolve(fun, x0)
        print(x0)


    elif what == 'min':
        x0 = minimize(fun, x0, bounds=bnds).x

    if cplx:
        x0 = x0[0] + 1j * x0[1]

    return x0


# plane wave tube outlet
def plane_wave_open_admittance(a):
    def Y(w, n):
        if n == 0:
            return -1 / (a * (0.25 * a * w + 0.61 * 1j))
        elif n == 1:
            return +0.25 / (0.25 * a * w + 0.61 * 1j) ** 2
        elif n == 2:
            return -(0.125 * a * w - 5.55111512312578e-17 * 1j) / (w * (
                    0.015625 * a ** 3 * w ** 3 + 0.114375 * 1j * a ** 2 * w ** 2 - 0.279075 * a * w - 0.226981 * 1j))
        elif n == 3:
            return +(0.09375 * a ** 2 * w ** 2 + 2.22044604925031e-16) / (w ** 2 * (
                    0.00390625 * a ** 4 * w ** 4 + 0.038125 * 1j * a ** 3 * w ** 3 - 0.1395375 * a ** 2 * w ** 2 - 0.226981 * 1j * a * w + 0.13845841))
        elif n == 4:
            return -(
                    0.09375 * a ** 3 * w ** 3 + 8.88178419700125e-16 * 1j * a ** 2 * w ** 2 - 3.5527136788005e-15 * 1j) / (
                           w ** 3 * (
                           0.0009765625 * a ** 5 * w ** 5 + 0.0119140625 * 1j * a ** 4 * w ** 4 - 0.058140625 * a ** 3 * w ** 3 - 0.141863125 * 1j * a ** 2 * w ** 2 + 0.1730730125 * a * w + 0.0844596301 * 1j))
        elif n == 5:
            return -(
                    -0.1171875 * a ** 4 * w ** 4 + 3.5527136788005e-15 * 1j * a ** 3 * w ** 3 - 1.4210854715202e-14 * a ** 2 * w ** 2 - 7.105427357601e-15 * 1j * a * w + 7.105427357601e-15) / (
                           w ** 4 * (
                           0.000244140625 * a ** 6 * w ** 6 + 0.00357421875 * 1j * a ** 5 * w ** 5 - 0.021802734375 * a ** 4 * w ** 4 - 0.0709315625 * 1j * a ** 3 * w ** 3 + 0.129804759375 * a ** 2 * w ** 2 + 0.12668944515 * 1j * a * w - 0.051520374361))
        else:
            raise ValueError('Derivative order of n = ' + str(n) + ' is not supported.\n Use a value from 0 to 5!')

    return Y


# Start AO modified
def w_sin_wa(a):
    def func(w, k=0):
        if k % 4 == 0:
            return - k * a ** (k - 1) * np.cos(a * w) + a ** k * w * np.sin(a * w)
        elif k % 4 == 1:
            return + k * a ** (k - 1) * np.sin(a * w) + a ** k * w * np.cos(a * w)
        elif k % 4 == 2:
            return + k * a ** (k - 1) * np.cos(a * w) - a ** k * w * np.sin(a * w)
        elif k % 4 == 3:
            return - k * a ** (k - 1) * np.sin(a * w) - a ** k * w * np.cos(a * w)

    return func


def w_cos_wa(a):
    def func(w, k=0):
        if k % 4 == 0:
            return + k * a ** (k - 1) * np.sin(a * w) + a ** k * w * np.cos(a * w)
        elif k % 4 == 1:
            return + k * a ** (k - 1) * np.cos(a * w) - a ** k * w * np.sin(a * w)
        elif k % 4 == 2:
            return - k * a ** (k - 1) * np.sin(a * w) - a ** k * w * np.cos(a * w)
        elif k % 4 == 3:
            return - k * a ** (k - 1) * np.cos(a * w) + a ** k * w * np.sin(a * w)

    return func


def sin_wa(a):
    def func(w, k=0):
        if k % 4 == 0:
            return a ** k * np.sin(a * w)
        elif k % 4 == 1:
            return a ** k * np.cos(a * w)
        elif k % 4 == 2:
            return -a ** k * np.sin(a * w)
        elif k % 4 == 3:
            return -a ** k * np.cos(a * w)

    return func


def cos_wa(a):
    def func(w, k=0):
        if k % 4 == 0:
            return a ** k * np.cos(a * w)
        elif k % 4 == 1:
            return -a ** k ** np.sin(a * w)
        elif k % 4 == 2:
            return -a ** k * np.cos(a * w)
        elif k % 4 == 3:
            return a ** k * np.sin(a * w)

    return func


def exp_wa(a):
    def func(w, k=0):
        return a ** k * np.exp(a * w)

    return func


def log_waplusb(a, b):
    def func(w, k=0):
        if k == 0:
            return np.log(a * w + b)
        else:
            return (-1) ** (k - 1) * factorial(k - 1) * a ** k / (a * w + b) ** k

    return func


def arctan_w():
    def func(w, k=0):
        if k == 0:
            return np.arctan(w)
        else:
            return (-1) ** (k - 1) * factorial(k - 1) * np.sin(k * np.arcsin(1 / np.sqrt(1 + w ** 2))) / (
                    1 + w ** 2) ** (k / 2.)

    return func


def nw_exp_minjtw_sin_aw(a):
    # n*w*np.exp(-1j*t*w)*np.sin(a*w)
    def func(w, n, t, k=0, l=0, m=0):
        der = 0
        ws = w_sin_wa(a)
        if m > 0:
            raise ValueError('Derivative order of m = ' + str(m) + ' is not supported.\n Need to be implemented.')
        if l >= 2:
            return 0
        elif l == 0:
            for ind in range(0, k + 1):
                der += n * binom(k, ind) * ws(w, ind) * (-1j * t) ** (k - ind) * np.exp(-1j * t * w)
            return der
        elif l == 1:
            for ind in range(0, k + 1):
                der += binom(k, ind) * ws(w, ind) * (-1j * t) ** (k - ind) * np.exp(-1j * t * w)
            return der

    return func


def w_exp_minjtw_sin_aw(a):
    # w*np.exp(-1j*t*w)*np.sin(a*w)
    def func(w, t, k=0, m=0):
        der = 0
        ws = w_sin_wa(a)
        if m > 0:
            raise ValueError('Derivative order of m = ' + str(m) + ' is not supported.\n Need to be implemented.')
        for ind in range(0, k + 1):
            der += binom(k, ind) * ws(w, ind) * (-1j * t) ** (k - ind) * np.exp(-1j * t * w)
        return der

    return func
# End AO modified
