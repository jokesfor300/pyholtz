#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
import numpy as np


def gauss_legendre_method(f,points,N=10):
    '''Function uses the Gauss-Legendre method to integrate f(x) along
        a curve defined by points. 
        
    Parameters
    ----------
    f : function
        function which will be integrated.
    
    points : list
        list of complex points along which the integration takes place. 
        
    N : int
        number of Gauss points.
        
    Returns
    -------
    Int: float
        integral of f along points.
    
    '''
    
    from numpy.polynomial.legendre import leggauss
    from parallel import parallizer
    Xi,W = leggauss(N)
    
    # Koordinatentransformation
#    for idx,t in enumerate(x):
#        x[idx] = 1/(b-a) * (2*t-a-b)
    
    def integ(x):
        return f(x[0])*x[1]
    
    L = len(points)
    Int = 0
    tasks=[]
    for i in range(0,L): # TODO: könnten wir auch parallelisieren mit action = 'append'
        a = points[i]
        if i < len(points)-1:
            b = points[i+1]
        else:
            b = points[0]
        
            
        xi = (b-a)/2 * Xi + (a+b)/2
        w  = W* (b-a)/2
        
        tasks+=[ [x,y]  for (x,y) in zip(xi,w)]

    Int = parallizer(tasks,integ)            
    return Int

def polygon_aufbereiter(l,pol):
    ''' Functions compares edge length of polygon to maximal length.
        If maximum length is exceeded additional points are added.
    
    Parameters
    ----------
    l : float
        maximum length of edges
        
    pol : list, complex
        list of complex numbers defining the polygon
    
    Returns
    -------
    new_polygon : list, complex
        polygon with maximum edge length l
    
    '''
    # Polygon ist eine Liste von komplexen Zahlen
    L = len(pol)
    new_polygon = []
    for i in range(0,L):
        if i<L-1:
            diff = pol[i+1]-pol[i]
        else:
            diff = pol[0]-pol[i]
        dist=np.abs(diff)
        print(dist,i)
        N = int(np.ceil(dist/l)) # soviele stücke kommen dazu, also teil-1 punkte
        if N==0:
            N=1
        diff/=N
        new_polygon+=[k*diff+pol[i] for k in range(N)]
    return new_polygon

 
#%%
#
#pol=[0,1+1j,1+5j,2+2j]
#pol=[0,1,1+1j,1j]
#x=[[np.real(z).item(0),np.imag(z).item(0)] for z in pol ]
##x+=[x[0]]
#pfad=Path(x)
#print(pfad.contains_points([[.5,-.5],[.5,.5]]))
#%%
#pol = [0,1+1j,1+5j,2+2j]
#pol_new = polygon_aufbereiter(1.,pol)
#print(pol_new)


############################################%% kleiner Test
#
#def function(x):
#    return x**2 +5
#a = 0
#b = 4
#
#Integral = gauss_legendre_method(function,[0,4,10j,],20)
#
#poly=[240-10j,250-10j,250+10j,240+10j] 
#poly=[p*np.pi*2 for p in poly]
#poly=polygon_aufbereiter(float('inf'),poly)
#Integral = gauss_legendre_method(function,poly,50)

#print(Integral)
###################################################    
##%% komplexer test
#def function(x):
#    return x**2
#
#Integral = gauss_legendre_method(function,0,1)
#Integral += gauss_legendre_method(function,1,1+1j)
#Integral += gauss_legendre_method(function,1+1j,0)
#
#
##%%
#import numpy as np
#from matplotlib.path import Path
#points = [[1,1],[1,2],[10,10],[3,0],[2,4],[1,1]]
#a=Path(points,closed=True) # letzter Punkt muss wieder der erste sein, damit es geschlossen ist.
#point=[2,3]
#a.contains_point(point)
#
##%% 
#import sympy as sym
#x=sym.symbols('x')
#f=sym.exp(2*x)+5
#1509.9789935208642
#
##%%
#xi,w = leggauss(10)

