#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Tue Feb 23 12:33:46 2016

@author: georg
"""


from scipy import eye, rand, zeros, empty, matrix, diag, logical_not, imag, real, shape
from scipy.linalg import  svd, eig #,solve
from scipy.sparse.linalg import spsolve
import cmath as cmath
from math import ceil, pi
import numpy as np
from PyHoltz.nlevp.gausslegendre import polygon_aufbereiter, gauss_legendre_method
from matplotlib.path import Path 


def Circ(L,z0,R,l=25,N=25,tol_rank=1E-10,info='False'):
    ''' Beyn's contour integral method. Calculates eigenvalues inside a circle.
    
    Parameters
    ----------
    
    L : function, callable object
        operator family
        
    z0 : float or complex
        center of circle
          
    R : float
        radius of contour circle
    
    l : int, optional
        maximum number of eigenvalues (default 25)
    
    N : int, optional
        number of evaluation points (default 25)
    
    tol_rank : float
        tolerance

               
    Returns
    -------
    
    lam : matrix
        eigenvalues
            
    v : matrix
        eigenvectors
         
    Notes
    -----


    '''
    m=L(0).shape[0]
    V_hat= rand(m,l) #eye(m,l) # rand(m,l)
    
    
    #Assemble A0 and A1 as circular contour integral
    A0=zeros((m,l))
    A1=A0
    for j in range(N):
        if info:
            print(str(j) + '/' + str(N),flush=True)
        LinvV=spsolve(L(z0+R*cmath.exp(2*j*cmath.pi*1j/N)),V_hat)
        A0=A0+LinvV*cmath.exp(2*j*cmath.pi*1j/N)
        A1=A1+LinvV*cmath.exp(4*j*cmath.pi*1j/N)
    A0=A0*R/N
    A1=z0*A0+R**2/N*A1
    
    #svd 
    V,Sigma,Wh=svd(A0,full_matrices=False)
    #print('shape',Wh.shape, V.shape)    
    
    #rank test
    if info:
        print(Sigma)
        print(Sigma>tol_rank)
    Sigma=Sigma[Sigma>tol_rank];
    k=len(Sigma)
    if k>=l:
        if info:
            print('Rank-Test FAILED')
        rankTestFlag = 1
    else:
        rankTestFlag = 0
    V=V[0:m,0:k];
    W=Wh[0:k,0:m]
    W=matrix(W)
    W=W.H
    Wh=Wh[0:k,0:l];
    V=matrix(V)
    Wh=matrix(Wh)

    #print(np.shape(V.H))
    #print(np.shape(A1))
    #print(np.shape(W.H))    
    
    
    lam,v =eig(V.H*A1*Wh.H*diag(1/Sigma),left=False, right=True)
   
    v=V*v
    

    mask = abs(lam-z0) < R
    if info:
        print(lam/2/np.pi)
        print(mask)
    lamOut = lam[logical_not(mask)]
    vOut   = v[:,logical_not(mask)]
    
    lam = lam[mask]
    v   = v[:,mask]
    #print(lamOut/pi/2)
    # w=W*w
    #ToDo: adjoint
    return lam,v, rankTestFlag

def Rect(L,z0,z1,l=25,N=25,tol_rank=1E-10):
    m=L(0).shape[0]
    V_hat= rand(m,l) #eye(m,l) # rand(m,l)
    
    
    
    #Assemble A0 and A1 as rectangular contour integral
    span=z1-z0        
    A0=zeros((m,l))    
    A1=A0   
    
    #lower edge
    M=ceil(span.real/(span.real+span.imag)*N/2) #number of sample points on this edge
    M = max(M,2)
    LinvV=spsolve(L(z0),V_hat)    
    A0=A0+.5/(M-1)*LinvV*span.real
    A1=A1+.5/(M-1)*z0*LinvV*span.real     
    for j in range(1,M-1):
        #print('lower edge:', j, ',/', M, flush=True)
        z=z0+j*span.real/(M-1)
        LinvV=spsolve(L(z),V_hat)
        A0=A0+1/(M-1)*LinvV*span.real
        A1=A1+1/(M-1)*z*LinvV*span.real            
    LinvV=spsolve(L(z0+span.real),V_hat)
    A0=A0+.5/(M-1)*LinvV*span.real 
    A1=A1+.5/(M-1)*(z0+span.real)*LinvV*span.real
    
    #right edge
    M=ceil(span.imag/(span.real+span.imag)*N/2) #number of sample points on this edge
    M = max(M,2)
    LinvV=spsolve(L(z0+span.real),V_hat)    
    A0=A0+.5/(M-1)*LinvV*span.imag*1j
    A1=A1+.5/(M-1)*(z0+span.real)*LinvV*span.imag*1j     
    for j in range(1,M-1):
        #print('right edge:', j, ',/', M, flush=True)
        z=z0+span.real+j*span.imag*1j/(M-1)
        LinvV=spsolve(L(z),V_hat)
        A0=A0+1/(M-1)*LinvV*span.imag*1j
        A1=A1+1/(M-1)*z*LinvV*span.imag*1j           
    LinvV=spsolve(L(z1),V_hat)
    A0=A0+.5/(M-1)*LinvV*span.imag*1j 
    A1=A1+.5/(M-1)*(z1)*LinvV*span.imag*1j

    #upper edge
    M=ceil(span.real/(span.real+span.imag)*N/2) #number of sample points on this edge
    M = max(M,2)
    LinvV=spsolve(L(z1),V_hat)
    A0=A0-.5/(M-1)*LinvV*span.real
    A1=A1-.5/(M-1)*z1*LinvV*span.real     
    for j in range(1,M-1):
        #print('upper edge:', j, ',/', M, flush=True)
        z=z1-j*span.real/(M-1)
        LinvV=spsolve(L(z),V_hat)
        A0=A0-1/(M-1)*LinvV*span.real
        A1=A1-1/(M-1)*z*LinvV*span.real            
    LinvV=spsolve(L(z1-span.real),V_hat)
    A0=A0-.5/(M-1)*LinvV*span.real 
    A1=A1-.5/(M-1)*(z1-span.real)*LinvV*span.real

    #left edge
    M=ceil(span.imag/(span.real+span.imag)*N/2) #number of sample points on this edge
    M = max(M,2)
    LinvV=spsolve(L(z1-span.real),V_hat)    
    A0=A0-.5/(M-1)*LinvV*span.imag*1j
    A1=A1-.5/(M-1)*(z1-span.real)*LinvV*span.imag*1j     
    for j in range(1,M-1):
        #print('left edge:', j, ',/', M, flush=True)
        z=z1-span.real-j*span.imag*1j/(M-1)
        LinvV=spsolve(L(z),V_hat)
        A0=A0-1/(M-1)*LinvV*span.imag*1j
        A1=A1-1/(M-1)*z*LinvV*span.imag*1j            
    LinvV=spsolve(L(z0),V_hat)
    A0=A0-.5/(M-1)*LinvV*span.imag*1j 
    A1=A1-.5/(M-1)*(z0)*LinvV*span.imag*1j    
    
    A0=1/2/cmath.pi/1j*A0
    A1=1/2/cmath.pi/1j*A1
    
    #A1=A1-.5*(z0+z1)*A0;
    #svd 
    V,Sigma,Wh=svd(A0,full_matrices=False)

    #rank test
    print('Singular Values:',Sigma)
    #print(Sigma>tol_rank)
    Sigma=Sigma[Sigma>tol_rank];
    k=len(Sigma)
    if k>=l:
        print('Rank-Test FAILED')
        rankTestFlag = 1
    else:
        rankTestFlag = 0
    V=V[0:m,0:k];
    Wh=Wh[0:k,0:l];
    V=matrix(V)
    Wh=matrix(Wh)
    #print(Wh.shape)
    #print(A1.shape)
    #print(V.shape)
    lam,v =eig(V.H*A1*Wh.H*diag(1/Sigma),left=False, right=True)
    #lam=lam+.5*(z0+z1)
    v=V*v

    
    mask = ((imag(lam-z0) > 0.0 ) * (real(lam-z0) > 0.0)) * ((imag(lam-z1) < 0.0) * (real(lam-z1) < 0.0))
    lamOut = lam[logical_not(mask)]
    vOut   = v[:,logical_not(mask)]
    
    lam = lam[mask]
    v   = v[:,mask]
    #w=Wh.H*w
    #print('These are kept:')
    #print(lam/cmath.pi/2.0)
    #print('Kicked because they are outside:')
    #print(lamOut/cmath.pi/2.0)
    return lam,v, rankTestFlag#,w
        

def Polygon(L,pol,l=25,N=25,h=float('inf'),tol_rank=1E-10,info=False):
    ''' Beyn's contour integral method. Calculates eigenvalues inside a polygon.
    
    Parameters
    ----------
    
    L : function, callable object
        operator family
        
    pol : list
        list of numbers forming a polygon in the complex domain
    
    l : int, optional
        maximum number of eigenvalues (default 25)
    
    N : int, optional
        number of evaluation points (default 25)
    
    h : float, optional
        maximal length of polygon edges (default 'inf'). Method adds nodes if 
        edge is too long.
    
    tol_rank : float
        tolerance

               
    Returns
    -------
    
    lam : matrix
        eigenvalues
            
    v : matrix
        eigenvectors
         
    Notes
    -----


    '''
    m=L(0).shape[0]
    V_hat= rand(m,l) #eye(m,l) # rand(m,l)
    new_pol=polygon_aufbereiter(h,pol)
    
    def integ(z):
        A=empty((2,m,l),dtype=complex)
        A[0]=spsolve(L(z),V_hat)
        A[1]=z*A[0]
        return A
    
    A=gauss_legendre_method(integ,new_pol,N)
    #svd 
    V,Sigma,Wh=svd(A[0],full_matrices=False)
    
    #rank test
    if info:
        print(Sigma)
        print(Sigma>tol_rank)
    Sigma=Sigma[Sigma>tol_rank];
    k=len(Sigma)
    if k>=l:
        if info:
            print('Rank-Test FAILED')
        rankTestFlag = 1
    else:
        rankTestFlag = 0
        V=V[0:m,0:k];
    W=Wh[0:k,0:m]
    W=matrix(W)
    W=W.H
    Wh=Wh[0:k,0:l];
    V=matrix(V)
    Wh=matrix(Wh)
    
    lam,v =eig(V.H*A[1]*Wh.H*diag(1/Sigma),left=False, right=True)
   
    v=V*v
    
    print(pol)
    pol=[[np.real(z),np.imag(z)] for z in pol ]
    path=Path(pol)
    mask=path.contains_points([[np.real(z),np.imag(z)] for z in lam ])
    if info:
        print(lam)
        print(mask)
    #lamOut = lam[logical_not(mask)]
    #vOut   = v[:,logical_not(mask)]
    
    print('#########')
    print(len(lam),len(Sigma))
    lam = lam[mask]
    v   = v[:,mask]
    Sigma =Sigma[mask]
    #print(lamOut/pi/2)
    # w=W*w
    #ToDo: adjoint
    rankTestFlag=[Sigma,rankTestFlag]
    return lam,v, rankTestFlag

    
