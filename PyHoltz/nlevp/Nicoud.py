#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Fri Dec  4 16:21:54 2015

@author: Georg A. Mensah
"""
import math as math
import numpy as np
import scipy.sparse.linalg as linalg
import gc #this program has serious issues with garbage collection

def iteration(lhs,rhs,k,maxiter=10,tol=0,relax=1,n_eig_val=3,v0=[],output=True):
    ''' Utilizes Nicoud's fix point iteration for the solution of 
    non-linear eigenvalue problems according to [1]_.
    
    The eigenvalue problem considered reads as follows:
    ``lhs(k)*v = k**2*rhs(k)*v``
    
    Parameters
    ----------
    
    lhs : function, callable object
        left hand side of the eigenvalue problem
            
    rhs : function, callable object
        right hand side of the eigenvalue problem (without ``k**2``)
        
    k : float or complex
        initial guess for the eigenvalue
          
    maxiter : int, optional
        maximum number of iterations (default is ``10``) 
        
    tol : float, optional
        tolerance. Convergnece is assumend  when ``abs(k_n-k_{n-1})<tol``

    relax : float, optional   
        relaxation parameter between 0 and 1. (This feature is 
        experimental, the default is 1, which implies no relaxation )
            
    v0 : array_like, optional 
        initial guess for the eigenvector (if not specified the vector is 
        initialized usings ones only)
           
    Returns
    -------
    
    k : matrix
        eigenvalue
            
    v : matrix
        eigenvector
        
    v_adj : matrix
        adjoint eigenvector
            
    Notes
    -----
    In case of mode degenracy the vectors returned feature multiple 
    columns. The algorithm is a slight modification of the algorithm 
    developed in [1]_. 
        
 
    
    ..  [1] F. Nicoud, L. Benoit, C. Sensiau and T. Poinsot. 2007. Acoustic 
        Modes in Combustors with Complex Impedances and Multidimensional 
        Active Flames. AIAA Journal 45(2):426-432
    '''
    if output: print("Launching Picard ...")
    auto_relax=relax=='auto'
    cerfacs_relax=relax=='cerfacs'
    k0=float('inf')
    k_n=[k]
    iteration=0
    if v0==[]:
        v0=np.ones((lhs(k).shape[0],1))
    
    try:
        while abs(k0-k)>tol and iteration<maxiter:
            if output: print("Iteration: ",iteration, ", Res:",abs(k0-k),',Freq',k/2/math.pi)
            k0=k
            A=lhs(k0)
            M=rhs(k0)
            k,v = linalg.eigs(A=A,M=M, k=n_eig_val, sigma = k0**2,v0=v0)
            k=np.sqrt(k)
            delta_k=abs(k-k0)         
            index=np.argsort(delta_k)
            k=k[index[0]]
            #f0=k.copy()
            v0=np.matrix(v[:,index[0],np.newaxis]) #TODO: consider relxation on vector
            #print(k[index]/2/np.pi)
            if auto_relax:
                #compute derivative
                #1.compute adjoint solution        
                k_adj,v_adj = linalg.eigs(A=(lhs(k0)).H,M=(rhs(k0)).H, k=n_eig_val, sigma = (k0**2).conjugate())
                k_adj=np.sqrt(k_adj)
                delta_k_adj=abs(k_adj-k0.conjugate())  
                index=np.argsort(delta_k_adj) #Todo degenerate theory
                v_adj=np.matrix(v_adj[:,index[0],np.newaxis])
                #derivative is taken at k0!
                deriv=(v_adj.H*lhs(k0,m=1)*v0)/(v_adj.H*rhs(k0)*v0)/2/k0
                relax=1/(1-deriv)
                #print('relax: ',relax,' abs: ',abs(relax),'deriv: ',deriv)
            #if cerfacs_relax:
            #    deriv=(k-k0)
                
            k=relax*k+(1-relax)*k0
            k=k.item(0) 
            if auto_relax and abs(k-k0)>100:
                k=k0+3*np.exp(1j*np.angle(k-k0))
            iteration+=1
            #print(k/2/math.pi)
            k_n.append(k)
            gc.collect()
            #print(np.linalg.norm(lhs(k0)*v0-rhs(k0)*v0*k0**2))

    except Exception as excp:
        if output:
            print('Error occured:')
            print(excp)
            print('...aborted Picard!',flush=True)
        flag=-2        
        v_adj=v0
        
    
    
    else:
        if output: print("Iterations: ",iteration, ", Res:",abs(k0-k),',Freq',k/2/math.pi)
        
        
        if n_eig_val>1 and delta_k[index[1]]<=(tol*10):
            #print('juhu',v0.shape)
            v0=v[:,index[0:2]]
            #print('juhu',v0.shape)
            
        #Compute adjoint solution        
        k_adj,v_adj = linalg.eigs(A=(lhs(k0)).H,M=(rhs(k0)).H, k=n_eig_val, sigma = (k0**2).conjugate())
        k_adj=np.sqrt(k_adj)
        delta_k_adj=abs(k_adj-k0.conjugate())  
        #print(delta_k_adj)
        index=np.argsort(delta_k_adj)
        if n_eig_val>1 and delta_k_adj[1]<=tol*10:
            v_adj=v_adj[:,index[0:2]]
        else:
            v_adj=v_adj[:,index[0],np.newaxis]
        
        
        if iteration>=maxiter:
            flag=-1
            if output: print('Warning: Maximum number of iterations has been reached!')        
        elif np.abs(k-k0)<=tol:
            flag=1
            if output: print('Solution has converged.')            

        else:
            if output: print('Warning: This should not be possible....\n If you can read this contact GAM!')
            flag=-3
        
        if output: print('...finished Picard!',flush=True)
        #print(delta_k,tol,delta_k[index[1]])

            
        #print('RES:', abs(k0-k))
    return np.array(k),np.matrix(v0),np.matrix(v_adj),iteration, flag 
        
        