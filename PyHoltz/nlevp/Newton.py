#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
# Georg Mensah, Philip Buschmann, Jonas Moeck, TU Berlin 2015
"""
Created on Fri Dec  4 16:21:54 2015

@author: georg
"""
import math as math
import numpy as np
import scipy.sparse.linalg as linalg
def iteration(L,k,maxiter=10,tol=0,relax=1,n_eig_val=3,v0=[],output=True):
    ''' Utilizes Newton's method for the solution of 
    non-linear eigenvalue problems.
    
    The eigenvalue problem considered reads as follows:
    ``L(k)*v =0``
    
    Parameters
    ----------
    
    L : function, callable object
        operator family
        
    k : float or complex
        initial guess for the eigenvalue
          
    maxiter : int, optional
        maximum number of iterations (default is ``10``) 
        
    tol : float
        tolerance. Convergence is assumend  when ``abs(k_n-k_{n-1})<tol``

    relax : float, optional   
        relaxation parameter between ``0`` and ``1``. (This feature is 
        experimental, the default is ``1``, which implies no relaxation )
            
    v0: array_like, optional 
        initial guess for the eigenvector (if not specified the vector is 
        initialized usings ones only)
           
    Returns
    -------
    
    k : matrix
        eigenvalue
            
    v : matrix
        eigenvector
        
    v_adj : matrix
        adjoint eigenvector
            
    Notes
    -----
        In case of mode degenracy the vectors returned feature multiple 
        columns.

    '''
    if output: print("Launching Newton...")
    k0=float('inf')
    k_n=[k]
    iteration=0
    
    if v0==[]:
        v0=np.ones((L(k).shape[0],1))
    while abs(k0-k)>tol and iteration<maxiter:
        if output: print("Iteration: ",iteration, ", Res:",abs(k0-k),',Freq',k/2/math.pi)
        k0=k
        A=L(k0)
        #print('shape :',v0.shape)
        k,v = linalg.eigs(A=A, k=n_eig_val, sigma = 0,v0=v0)
        delta_k=abs(k)         
        index=np.argsort(delta_k)
        k2=k[index]
        v2=np.matrix(v[:,index])
        k=k[index[0]]
        v0=np.matrix(v[:,index[0],np.newaxis]) #TODO: consider relxation on vector
        #print('shape :',v0.shape)
        #compute derivative
        #1.compute adjoint solution        
        k_adj,v_adj = linalg.eigs(A=(A).H, k=n_eig_val, sigma = 0)
        delta_k_adj=abs(k_adj)  
        index_adj=np.argsort(delta_k_adj) #Todo degenerate theory
        v_adj2=np.matrix(v_adj[:,index_adj])
        v_adj=np.matrix(v_adj[:,index_adj[0],np.newaxis])
        #derivative is taken at k0!
        deriv=(v_adj.H*L(k0,m=1)*v0)/(v_adj.H*v0)
        #deriv2=(v0.H*(L(k0,m=0).T*L(k0,m=1)+L(k0,m=1).T*L(k0,m=0))*v0)/(v0.H*v0)/(2*k)
        #print('test: ', deriv,'number2 :',  deriv2 , 'ratio: ', abs(deriv/deriv2), 'endtest')
        more=[]
        k_adj2=k_adj[index_adj]
        for i in range(n_eig_val):
            deriv2=(v_adj2[:,i].H*L(k0,m=1)*v2[:,i])/(v_adj2[:,i].H*v2[:,i])
            more.append((relax*(k0-k2[i]/deriv2)+(1-relax)*k0).item(0))
            #print(i,'. ', k2[i], ' vs. ',k_adj2[i])
        more=np.array(more)
        index=np.argsort(abs(more-k0)) #choose closest to k0
                    
        #deriv2=(v_adj2.H*L(k0,m=1)*v2)/(v_adj2.H*v2)
        k=relax*(k0-k/deriv)+(1-relax)*k0    
        
        #k=k.item(0) 
        k=more[index[0]].item(0)        
        iteration+=1
        #print('shape:', more.shape, 'idx', index)
        #print('juhu: ',k/2/math.pi,more[index[0]]/2/math.pi)
        #print('less :',k/2/np.pi,'more :',more/2/np.pi)
        k_n.append(k)
        #print(np.linalg.norm(lhs(k0)*v0-rhs(k0)*v0*k0**2))

    #print(delta_k,tol,delta_k[index[1]])
    if n_eig_val>1 and delta_k[index[1]]<=(tol*10):
        #print('juhu',v0.shape)
        v0=v[:,index[0:2]]
        #print('juhu',v0.shape)
        
    #Compute adjoint solution        
    k_adj,v_adj = linalg.eigs(A=(L(k0)).H, k=3, sigma = 0)
    delta_k_adj=abs(k_adj)  
    #print(delta_k_adj)
    index=np.argsort(delta_k_adj)
    if n_eig_val>1 and delta_k_adj[1]<=tol*10:
        v_adj=v_adj[:,index[0:2]]
    else:
        v_adj=v_adj[:,index[0],np.newaxis]
        
            
    if output: 
        print("Iterations: ",iteration, ", Res:",abs(k0-k),',Freq',k/2/math.pi)
        print("... Newton finished!")
    #print('RES:', abs(k0-k))
    return np.array(k), np.matrix(v0), np.matrix(v_adj), iteration
        
        
