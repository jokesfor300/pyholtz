# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
import pickle

from PyHoltz.nlevp.perturb_utils import accel_asc, part2mult, multinomcoeff, weight
import numpy as np
from PyHoltz.parallel import parallizer2
import scipy.sparse.linalg as sspla
import os
import csv
from ipywidgets import FloatProgress
from IPython.display import display

# %% load data
# dirname = os.path.dirname(__file__)
#
# try:
#    MN = []
#    ORD=20
#    for k in range(ORD+1):
#	    with open(dirname+"/perturbation_data/L_order_"+str(k)+".pickle", "rb") as fp: 
#                MN.append(pickle.load(fp))# Unpickling
#    alltasks=[]
#
#
#    for k in range(ORD+1):
#        tasks=[]    
#        for key, val in MN[k].items():
#            #print(key, val)
#            tasks+=[[key,val]]
#        alltasks+=[tasks]
# except:
#	print("Perturbation data does not exist!")
#	pass
# %% there are no longer any pickle files, now it's all in .txt files
# 
# 

# tell the program where to look for the files
dirname = os.path.dirname(__file__)
# print(dirname)

MN = []
try:
    # MN bekommt für jede Ordnung einen Listeneintrag, der ein dictionary ist
    # mit: keys: (0,1) tuple
    # und values: [array,array] liste von arrays
    ORD = 30
    for k in range(ORD + 1):
        Ltxt = os.listdir(dirname + "/perturbation_data/L_order_" + str(k))
        dicforoneorder = {}
        for txt in Ltxt:
            with open(dirname + "/perturbation_data/L_order_" + str(k) + '/' + txt) as onetxt:
                reader = csv.reader(onetxt, delimiter=',')
                rawline1 = next(reader)
                rawline1 = [int(i.strip('()')) for i in rawline1]
                line1 = tuple(rawline1)

                rawline2 = next(reader)
                line2 = []
                lineEntry = []
                for char in rawline2:
                    char = char.strip(' [')
                    newchar = char.strip(' ]')
                    if len(newchar) > 0:
                        lineEntry.append(int(newchar))
                    if ']' in char:
                        line2.append(np.array(lineEntry))
                        lineEntry = []

                entry = {line1: line2}
                dicforoneorder.update(entry)
        MN.append(dicforoneorder)

    alltasks = []  # alltasks is created as before
    for k in range(ORD + 1):
        tasks = []
        for key, val in MN[k].items():
            # print(key, val)
            tasks += [[key, val]]
        alltasks += [tasks]
except FileNotFoundError:
    # print("Perturbation data for order {} does not exist!".format(k))
    pass


def perturb(L, v0, v0_adj, N, printOrd=True):
    # normalize
    v0 /= np.sqrt(v0.H * v0)
    v0_adj /= v0_adj.H * L(1, 0) * v0
    lamb = [0] * (N + 1)
    v = [v0]

    for k in range(1, N + 1):
        if printOrd:
            print('Solving order', k)
        r = np.zeros(v0.shape, dtype=complex)
        for n in range(1, k + 1):
            r += L(0, n) * v[k - n]

        for m in range(1, k + 1):
            for mu in accel_asc(m):
                if mu == [k]: continue
                mu = part2mult(mu)
                for n in range(k - m + 1):
                    coeff = 1
                    for g, mu_g in enumerate(mu):
                        coeff *= lamb[g + 1] ** mu_g
                    r += L(int(np.sum(mu)), n) * v[k - n - m] * multinomcoeff(mu) * coeff

        lamb[k] = (-v0_adj.H * r / (v0_adj.H * L(1, 0) * v0))
        lamb[k] = lamb[k].item(0)

        v.append(np.matrix(sspla.spsolve(L(0, 0), -(r + lamb[k] * L(1, 0) * v0))).T)

        # print('Order ', k)
        # print(np.linalg.norm(L(0,0)*v[k] + (r +lamb[k]*L(1,0)*v0)  ))
        # print(sspla.norm(L(0,0)),flush=True)

    return lamb, v


def perturb_fast(L, v0, v0_adj, N, printOrd=True):
    # normalize
    v0 /= np.sqrt(v0.H * v0)
    v0_adj /= v0_adj.H * L(1, 0) * v0
    lmbd = [0] * (N + 1)
    v = [v0]

    for k in range(1, N + 1):
        if printOrd:
            print('Solving order', k)
        r = np.zeros(v0.shape, dtype=complex)
        for (m, n) in MN[k]:
            w = np.zeros(v0.shape, dtype=complex)
            for mu in MN[k][m, n]:
                coeff = 1
                for g, mu_g in enumerate(mu):
                    coeff *= lmbd[g + 1] ** mu_g
                w += v[k - n - weight(mu)] * multinomcoeff(mu) * coeff
            r += L(m, n) * w

        lmbd[k] = (-v0_adj.H * r / (v0_adj.H * L(1, 0) * v0))  # nromalization is giving numerical stability!!!!
        lmbd[k] = lmbd[k].item(0)

        v.append(np.matrix(sspla.spsolve(L(0, 0), -(r + lmbd[k] * L(1, 0) * v0))).T)
        v[k] -= (v0.H * v[k]).item(0) * v0  # if v[k] is orthogonal to v0 its norm is minimal => less round-off errors
    #        print('Order', k)
    #        print(np.linalg.norm(L(0,0)*v[k] + (r +lmbd[k]*L(1,0)*v0)  ))
    #        print(np.linalg.norm(r +lmbd[k]*L(1,0)*v0))
    #        print(sspla.norm(L(0,0)))

    #        if k>5: v[k]*=0
    #        s=0.+0.j
    #        for l in range(1,k):
    #            s+=v[l].H*v[k-l]
    #        print('s: ',s,'inner ',v[0].H*v[k])
    #        v[k]-=((s)/2+v[0].H*v[k]).item(0)*v[0]

    return lmbd, v


def perturb_fast_parallel(L, v0, v0_adj, N, normalize, info=False, printOrd=True):
    # normalize
    v0 /= np.sqrt(v0.conj().dot(v0))
    L10v0 = L(1, 0).dot(v0)
    if v0_adj is None:
        v0_adj = v0
    v0_adj /= v0_adj.conj().dot(L10v0)
    lamb = [0] * (N + 1)
    v = [v0]

    # create a process bar
    if info:
        max_count = N
        f = FloatProgress(min=0, max=max_count, bar_style='success')  # instantiate the bar
        display(f)  # display the bar

    for k in range(1, N + 1):
        if printOrd:
            print('Solving order', k)

        def build_term(idx):
            # print('#######################')
            # print(data,flush=True)
            with open(dirname + "/perturbation_data/L_order_" + str(k) + "/" + str(idx) + ".txt", "r") as fp:
                txt = fp.readline()
                mn = txt.strip('()\n').split(',')
                m, n = int(mn[0]), int(mn[1])
                multiindices = eval(fp.readline())
            # m,n=data[0]
            w = np.zeros(v0.shape, dtype=complex)
            for mu in multiindices:  # data[1]:
                mu = np.array(mu)
                coeff = 1
                for g, mu_g in enumerate(mu):
                    coeff *= lamb[g + 1] ** mu_g
                w += v[k - n - weight(mu)] * multinomcoeff(mu) * coeff
            return L(m, n) * w

        # r=parallizer2(alltasks[k].copy(),build_term,Queue=False)
        nfiles = len(os.listdir(dirname + "/perturbation_data/L_order_" + str(k) + "/"))
        tasks = [i + 1 for i in range(nfiles)]
        r = parallizer2(tasks, build_term, queue=True)
        #        r=np.zeros(v0.shape,dtype=complex)
        #        for (m,n) in MN[k]:
        #            w=np.zeros(v0.shape,dtype=complex)
        #            for mu in MN[k][(m,n)]:
        #                coeff=1
        #                for g,mu_g in enumerate(mu):
        #                    coeff*=lamb[g+1]**mu_g
        #                w+=v[k-n-weigh(mu)]*multinomcoeff(mu)*coeff
        #            r+=L(m,n)*w

        lamb[k] = -v0_adj.conj().dot(r) / v0_adj.conj().dot(L10v0)  # nromalization is giving numerical stability!!!!
        lamb[k] = lamb[k].item(0)

        v.append(sspla.spsolve(L(0, 0), -(r + lamb[k] * L10v0)))
        v[k] -= v0.conj().dot(v[k]) * v0
        if normalize:
            if info:
                f.value += 1
            s = 0j
            for l in range(1, k):
                s += v[l].conj().dot(v[k - l])
                v[k] -= v[0] * (s / 2. + v[0].conj().dot(v[k]))

    return lamb, v
