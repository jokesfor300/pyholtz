# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Sat Mar 10 12:20:53 2018

@author: georg
"""
from scipy.special import factorial
from scipy.sparse.linalg import spsolve, lsqr, norm
import numpy as np
import pickle
from PyHoltz.parallel import parallizer2
from copy import deepcopy


# %%
def accel_asc(n):
    # Kellehrer's Algorithm
    a = [0 for i in range(n + 1)]
    k = 1
    y = n - 1
    while k != 0:
        x = a[k - 1] + 1
        k -= 1
        while 2 * x <= y:
            a[k] = x
            y -= x
            k += 1
        l = k + 1
        while x <= y:
            a[k] = x
            a[l] = y
            yield a[:k + 2]
            x += 1
            y -= 1
        a[k] = x + y
        y = x + y - 1
        yield a[:k + 1]


def part2mult(multi):
    # reorganize partition into multiindex
    z = np.sum(multi)
    n = np.zeros(z, dtype='int64')
    if multi != [0]:
        for i in multi:
            n[i - 1] = n[i - 1] + 1
        return n
    else:
        return np.zeros(0, dtype='int64')


def multinomcoeff(mu):
    coeff = factorial(np.sum(mu)) / np.prod(factorial(mu))
    coeff = coeff.item(0)
    return coeff


def perturb(L, v0, v0_adj, N):
    # normalize
    v0 /= np.sqrt(v0.H * v0)
    v0_adj /= v0_adj.H * L(1, 0) * v0
    lamb = [0] * (N + 1)
    v = [v0]

    for k in range(1, N + 1):
        r = np.zeros(v0.shape, dtype=complex)
        for n in range(1, k + 1):
            r += L(0, n) * v[k - n]

        for m in range(1, k + 1):
            for mu in accel_asc(m):
                if mu == [k]: continue
                mu = part2mult(mu)
                for n in range(k - m + 1):
                    coeff = 1
                    for g, mu_g in enumerate(mu):
                        coeff *= lamb[g + 1] ** mu_g
                    r += L(int(np.sum(mu)), n) * v[k - n - m] * multinomcoeff(mu) * coeff

        lamb[k] = (-v0_adj.H * r / (v0_adj.H * L(1, 0) * v0))
        lamb[k] = lamb[k].item(0)

        v.append(np.matrix(spsolve(L(0, 0), -(r + lamb[k] * L(1, 0) * v0))).T)

        print('Order ', k)
        print(np.linalg.norm(L(0, 0) * v[k] + (r + lamb[k] * L(1, 0) * v0)))
        print(norm(L(0, 0)), flush=True)

    return lamb, v


# %%
with open("L30.pickle", "rb") as fp:  # Unpickling
    MN = pickle.load(fp)

# %%
alltasks = []
for k in range(31):
    tasks = []
    for key, val in MN[k].items():
        # print(key, val)
        tasks += [[key, val]]
    alltasks += [tasks]


# %%


def weigh(mu):
    weight = 0
    for g, mu_g in enumerate(mu):
        weight += (g + 1) * mu_g
    return weight


# %%
def perturb_fast(L, v0, v0_adj, N):
    # normalize
    v0 /= np.sqrt(v0.H * v0)
    v0_adj /= v0_adj.H * L(1, 0) * v0
    lamb = [0] * (N + 1)
    v = [v0]

    for k in range(1, N + 1):
        r = np.zeros(v0.shape, dtype=complex)
        for (m, n) in MN[k]:
            w = np.zeros(v0.shape, dtype=complex)
            for mu in MN[k][(m, n)]:
                coeff = 1
                for g, mu_g in enumerate(mu):
                    coeff *= lamb[g + 1] ** mu_g
                w += v[k - n - weigh(mu)] * multinomcoeff(mu) * coeff
            r += L(m, n) * w

        lamb[k] = (-v0_adj.H * r / (v0_adj.H * L(1, 0) * v0))  # nromalization is giving numerical stability!!!!
        lamb[k] = lamb[k].item(0)

        v.append(np.matrix(spsolve(L(0, 0), -(r + lamb[k] * L(1, 0) * v0))).T)
        v[k] -= (v0.H * v[k]).item(0) * v0  # if v[k] is orthogonal to v0 its norm is minimal => less round-off errors
    #        print('Order', k)
    #        print(np.linalg.norm(L(0,0)*v[k] + (r +lamb[k]*L(1,0)*v0)  ))
    #        print(np.linalg.norm(r +lamb[k]*L(1,0)*v0))
    #        print(norm(L(0,0)))

    #        if k>5: v[k]*=0
    #        s=0.+0.j
    #        for l in range(1,k):
    #            s+=v[l].H*v[k-l]
    #        print('s: ',s,'inner ',v[0].H*v[k])
    #        v[k]-=((s)/2+v[0].H*v[k]).item(0)*v[0]

    return lamb, v


def perturb_fast_parallel(L, v0, v0_adj, N):
    # normalize
    print('###############Juhu############')
    v0 /= np.sqrt(v0.H * v0)
    print('###############Juhu2###########')
    v0_adj /= v0_adj.H * L(1, 0) * v0
    print('###############Juhu3###########')
    lamb = [0] * (N + 1)
    v = [v0]

    for k in range(1, N + 1):
        def build_term(data):
            # print('#######################')
            # print(data,flush=True)
            m, n = data[0]
            w = np.zeros(v0.shape, dtype=complex)
            for mu in data[1]:
                coeff = 1
                for g, mu_g in enumerate(mu):
                    coeff *= lamb[g + 1] ** mu_g
                w += v[k - n - weigh(mu)] * multinomcoeff(mu) * coeff
            # print('#######################')
            # print(L(m,n)*w,flush=True)
            # print('Hier',flush=True)
            return L(m, n) * w

        r = parallizer2(alltasks[k].copy(), build_term, queue=False)
        #        r=np.zeros(v0.shape,dtype=complex)
        #        for (m,n) in MN[k]:
        #            w=np.zeros(v0.shape,dtype=complex)
        #            for mu in MN[k][(m,n)]:
        #                coeff=1
        #                for g,mu_g in enumerate(mu):
        #                    coeff*=lamb[g+1]**mu_g
        #                w+=v[k-n-weigh(mu)]*multinomcoeff(mu)*coeff
        #            r+=L(m,n)*w

        lamb[k] = (-v0_adj.H * r / (v0_adj.H * L(1, 0) * v0))  # nromalization is giving numerical stability!!!!
        lamb[k] = lamb[k].item(0)

        v.append(np.matrix(spsolve(L(0, 0), -(r + lamb[k] * L(1, 0) * v0))).T)
        v[k] -= (v0.H * v[k]).item(0) * v0

    return lamb, v


# %%
def prepare_perturbation(N):
    L = [{}]
    for k in range(1, N + 1):
        L.append({})
        for n in range(1, k + 1):
            L[k][(0, n)] = [[]]

        for m in range(1, k + 1):
            for mu in accel_asc(m):
                if mu == [k]: continue
                mu = part2mult(mu)
                for n in range(k - m + 1):
                    idx = (int(np.sum(mu)), n)
                    if idx in L[k]:
                        L[k][idx].append(mu)
                    else:
                        L[k][idx] = [mu]

    return L

# %%
# import pickle
# N=30
# L=prepare_perturbation(N)
# with open("L"+str(N)+".pickle", "wb") as fp:
#    pickle.dump(L,fp)
##%%    
# with open("test.txt", "rb") as fp:   # Unpickling
#    l = pickle.load(fp)
