# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Mon Apr  9 09:48:23 2018

@author: georg
"""

# d/(dx^1)(f(x)^(-a)) = -a f(x)^(-a - 1) f'(x)
# (d^2 )/(dx^2)(f(x)^(-a)) = a f(x)^(-a - 2) ((a + 1) f'(x)^2 - f(x) f''(x))
# (d^3 )/(dx^3)(f(x)^(-a)) = -a f(x)^(-a - 3) ((a^2 + 3 a + 2) f'(x)^3 - 3 (a + 1) f(x) f'(x) f''(x) + f(x)^2 f^(3)(x))
# (d^4 )/(dx^4)(f(x)^(-a)) = a f(x)^(-a - 4) (-6 (a^2 + 3 a + 2) f(x) f'(x)^2 f''(x) + (a^3 + 6 a^2 + 11 a + 6) f'(x)^4 + f(x)^2 (3 (a + 1) f''(x)^2 - f(x) f^(4)(x)) + 4 (a + 1) f(x)^2 f^(3)(x) f'(x))
# (d^5 )/(dx^5)(f(x)^(-a)) =-a f(x)^(-a - 5) ((a + 1) (a + 2) (a + 3) (a + 4) f'(x)^5 + 10 (a + 1) (a + 2) f(x)^2 f^(3)(x) f'(x)^2 - 10 (a + 1) (a + 2) (a + 3) f(x) f'(x)^3 f''(x) + f(x)^3 (f(x) f^(5)(x) - 10 (a + 1) f^(3)(x) f''(x)) + 5 (a + 1) f(x)^2 f'(x) (3 (a + 2) f''(x)^2 - f(x) f^(4)(x)))

import numpy as np
from matplotlib import pyplot as plt
import scipy.sparse as ssp
import scipy.sparse.linalg as sspla
from scipy.special import factorial


def householder_update(f, a=1):
    order = len(f) - 1
    a = 1. / a

    if order == 1:  # aka Newton's method
        dz = -f[0] / (a * f[1])
    elif order == 2:  # aka Halley's method
        dz = -2 * f[0] * f[1] / ((a + 1) * f[1] ** 2 - f[0] * f[2])
    elif order == 3:
        dz = -3 * f[0] * ((a + 1) * f[1] ** 2 - f[0] * f[2]) / (
                (a ** 2 + 3 * a + 2) * f[1] ** 3 - 3 * (a + 1) * f[0] * f[1] * f[2] + f[0] ** 2 * f[3])
    elif order == 4:
        dz = -4 * f[0] * ((a ** 2 + 3 * a + 2) * f[1] ** 3 - 3 * (a + 1) * f[0] * f[1] * f[2] + f[0] ** 2 * f[3]) / (
                -6 * (a ** 2 + 3 * a + 2) * f[0] * f[1] ** 2 * f[2] + (a ** 3 + 6 * a ** 2 + 11 * a + 6) * f[
            1] ** 4 + f[0] ** 2 * (3 * (a + 1) * f[2] ** 2 - f[0] * f[4]) + 4 * (a + 1) * f[0] ** 2 * f[3] * f[1])
    else:
        dz = -5 * f[0] * (-6 * (a ** 2 + 3 * a + 2) * f[0] * f[1] ** 2 * f[2] + (a ** 3 + 6 * a ** 2 + 11 * a + 6) * f[
            1] ** 4 + f[0] ** 2 * (3 * (a + 1) * f[2] ** 2 - f[0] * f[4]) + 4 * (a + 1) * f[0] ** 2 * f[3] * f[1]) / (
                     (a + 1) * (a + 2) * (a + 3) * (a + 4) * f[1] ** 5 + 10 * (a + 1) * (a + 2) * f[0] ** 2 * f[3] *
                     f[1] ** 2 - 10 * (a + 1) * (a + 2) * (a + 3) * f[0] * f[1] ** 3 * f[2] + f[0] ** 3 * (
                             f[0] * f[5] - 10 * (a + 1) * f[3] * f[2]) + 5 * (a + 1) * f[0] ** 2 * f[1] * (
                             3 * (a + 2) * f[2] ** 2 - f[0] * f[4]))

    return dz


def householder(func, z0=0, maxiter=100, tol=0, relax=1, order=5):
    z = float('inf')
    z_out = [z0]
    n = 0
    while np.abs(z - z0) > tol and n < maxiter:
        z = z0
        f = func(z)[:order + 1]
        dz = householder_update(f)
        z0 = z0 + relax * dz
        z_out += [z0]
        n += 1
    return z_out


def minres(L, z0, maxiter=10, tol=0., relax=1., lam_tol=1e-8, order=1, n_eig_val=1, v0=[], output=True, printRes=True):
    """ Utilizes Householder's method for the solution of
    non-linear eigenvalue problems. Method is based on Newton's algorithm
    for rootfinding generalized by considering higher order derivatives.

    The eigenvalue problem considered reads as follows:
    `L(k)*v =0`

    Parameters
    ----------

    L : function, callable object
        operator family

    z0 : float or complex
        initial guess for the eigenvalue

    maxiter : int, optional
        maximum number of iterations (default is 10)

    lam_tol : float, optional
        tolerance. Convergnece is assumend  when `abs(z_n-z_{n-1})<tol`

    order : int, optional
        order of derivatives (default is 5)

    n_eig_val : int, optional
        tolerance. Convergnece is assumend  when `abs(z_n-z_{n-1})<tol`

    v0: array_like, optional
        initial guess for the eigenvector

    Returns
    -------

    z0 : matrix
        eigenvalue

    v0 : matrix
        eigenvector

    v0_adj : matrix
        adjoint eigenvector

    n : int
        number of iterations

    Notes
    -----
        If order is set to 1 / 2 then the method is identical to Newton's / Halley's method.

    """
    if output:
        print("Launching Householder...")
        print("Iter    Res:     dz:     z:")
        print("----------------------------------")

    z = float('inf')
    lam = float('inf')
    # print('Activity test 3:',L.active,flush=True)
    n = 0
    if v0 == []:
        v0 = np.ones((L(z0).shape[0], 1))
        v0_adj = np.ones((L(z0).shape[0], 1))
    # TODO: Inititialize adjoint

    try:
        while (np.abs(z - z0) > tol or np.abs(lam) > lam_tol) and n < maxiter:
            if output:
                print(n, np.abs(lam), np.abs(z - z0), z)
            z = z0
            L.set_value(L.eigval_param, z)

            A = L(z)
            M = -L.terms[-1]['matrix']  # AO: I think this should be the identity to be safer
            # print(L.active,' ',A.shape, ' ',v0.shape)
            # print(n_eig_val)
            lam, v = sspla.eigs(A=A, M=M, k=n_eig_val, sigma=0, v0=v0)
            lam_adj, v_adj = sspla.eigs(A=(A).H, M=M.H, k=n_eig_val, sigma=0, v0=v0_adj)
            # TODO: consider conjugation
            # TODO: multiple eigenvalues
            indexing = np.argsort(np.abs(lam))
            lam = lam[indexing]
            v = v[:, indexing]
            indexing = np.argsort(np.abs(lam_adj))
            lam_adj = lam_adj[indexing]
            v_adj = v_adj[:, indexing]

            # TODO: sort v_adj
            delta_z = []
            for i in range(0, n_eig_val):
                L.p = np.matrix(v[:, i]).T
                L.p_adj = np.matrix(v_adj[:, i]).T
                L.omega = lam[i]
                L.new_perturb_fast(L.eigval_param, order, printOrd=False)  # L.new_perturb_fast('omega',order)
                f = [factorial(idx) * coeff for idx, coeff in enumerate(L.omega_pert)]
                dz = householder_update(f)
                # print(z+dz)
                delta_z.append(dz)

            indexing = np.argsort(np.abs(dz))
            lam = lam[indexing[0]].item(0)
            z0 = z0 + relax * delta_z[indexing[0]]
            # print('here ',( v0.T  + (v[:,indexing[0]])   ).shape)
            v0 = (1 - relax) * v0 + relax * np.expand_dims(v[:, indexing[0]], axis=1)
            v0_adj = (1 - relax) * v0_adj + relax * np.expand_dims(v_adj[:, indexing[0]], axis=1)
            n += 1
            # TODO: turn this into foot controlled loop

    except Exception as e:
        print('Error occured:')
        print(e)
        print('...aborted Householder!', flush=True)
        flag = -2
    else:
        # A=L(z0)
        # I=-L.terms[-1]['matrix']
        # normA=np.sqrt(linalg.eigs(A.H*A,return_eigenvectors=False,k=1)).item(0)
        # print('normA',normA)
        if output:
            print(n, np.abs(lam), np.abs(z - z0), z)

        if n >= maxiter:
            flag = -1
            if output:
                print('Warning: Maximum number of iterations has been reached!')

        elif np.abs(lam) <= lam_tol:
            flag = 1
            if output:
                print('Solution has converged!')
        elif np.abs(z - z0) <= tol:
            flag = 0
            if output:
                print('Warning: Slow convergence!')

        else:
            if output:
                print('Warning: This should not be possible....\n If you can read this contact GAM!')
            flag = -3

        if output:
            print('...finished Householder!', flush=True)

    if printRes:
        print('#####################')
        print(' Householder results ')
        print('#####################')
        print('Number of steps:', n)
        print('Last step parameter variation:', abs(z0 - z))
        print('Auxiliary eigenvalue lambda residual (rhs):', np.abs(lam))
        print('Eigenvalue/(2*pi):', z / 2 / np.pi)
        print(' ')
    return np.array(z0), np.matrix(v0), np.matrix(v0_adj), n, flag


def testfunc(z, n=6):
    f = []
    for k in range(6):
        if k == 0:
            f += [z ** n - 1]
        elif k <= n:
            coeff = 1
            for i in range(k):
                coeff *= (n - i)
            f += [coeff * z ** (n - k)]
        else:
            f += [0]
    return f

# %%

# plt.close('all')
# plt.figure('convergence')
# for order in range(5):
#    func=lambda z: testfunc(z,6)
#    conv=householder(func,z0=101,order=order+1) 
#    print(conv[-1])
#    plt.semilogy(np.abs(np.array(conv)-1),label='order: '+str(order+1))
# plt.legend()
