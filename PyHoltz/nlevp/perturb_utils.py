# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________

import numpy as np
from scipy.special import factorial


def accel_asc(n):
    # Kellehrer's Algorithm
    a = [0 for i in range(n + 1)]
    k = 1
    y = n - 1
    while k != 0:
        x = a[k - 1] + 1
        k -= 1
        while 2 * x <= y:
            a[k] = x
            y -= x
            k += 1
        l = k + 1
        while x <= y:
            a[k] = x
            a[l] = y
            yield a[:k + 2]
            x += 1
            y -= 1
        a[k] = x + y
        y = x + y - 1
        yield a[:k + 1]


def part2mult(multi):
    # reorganize partition into multiindex
    z = np.sum(multi)
    n = np.zeros(z, dtype='int64')
    if multi != [0]:
        for i in multi:
            n[i - 1] = n[i - 1] + 1
        return n
    else:
        return np.zeros(0, dtype='int64')


def multinomcoeff(mu):
    coeff = factorial(np.sum(mu)) / np.prod(factorial(mu))
    coeff = coeff.item(0)
    return coeff


def weight(mu):
    res = 0
    for g, mu_g in enumerate(mu):
        res += (g + 1) * mu_g
    return res


def prepare_perturbation(k):
    L = {}
    print('creating data set for order ', k, '...', flush=True)
    for n in range(1, k + 1):
        # print(n,flush=True)
        L[(0, n)] = [[]]

    for m in range(1, k + 1):
        for mu in accel_asc(m):
            if mu == [k]: continue
            mu = part2mult(mu)
            for n in range(k - m + 1):
                idx = (int(np.sum(mu)), n)
                if idx in L:
                    L[idx].append(mu)
                else:
                    L[idx] = [mu]
    return L