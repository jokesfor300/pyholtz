#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
import numpy as np

def w_exp_minjtw_sin_aw(a):
	def func(w,t,k=0,l=0):
		if l != 0:
			print("Argh no derivative of tau allowed!")
			return None
		if k==0:
			return w*np.exp(-1j*t*w)*np.sin(a*w)
		if k==1:
			return a*w*np.exp(-1j*t*w)*np.cos(a*w) - 1j*t*w*np.exp(-1j*t*w)*np.sin(a*w) + np.exp(-1j*t*w)*np.sin(a*w)
		if k==2:
			return -a**2*w*np.exp(-1j*t*w)*np.sin(a*w) - 2*1j*a*t*w*np.exp(-1j*t*w)*np.cos(a*w) + 2*a*np.exp(-1j*t*w)*np.cos(a*w) - t**2*w*np.exp(-1j*t*w)*np.sin(a*w) - 2*1j*t*np.exp(-1j*t*w)*np.sin(a*w)
		if k==3:
			return -a**3*w*np.exp(-1j*t*w)*np.cos(a*w) + 3*1j*a**2*t*w*np.exp(-1j*t*w)*np.sin(a*w) - 3*a**2*np.exp(-1j*t*w)*np.sin(a*w) - 3*a*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) - 6*1j*a*t*np.exp(-1j*t*w)*np.cos(a*w) + 1j*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) - 3*t**2*np.exp(-1j*t*w)*np.sin(a*w)
		if k==4:
			return a**4*w*np.exp(-1j*t*w)*np.sin(a*w) + 4*1j*a**3*t*w*np.exp(-1j*t*w)*np.cos(a*w) - 4*a**3*np.exp(-1j*t*w)*np.cos(a*w) + 6*a**2*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) + 12*1j*a**2*t*np.exp(-1j*t*w)*np.sin(a*w) + 4*1j*a*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) - 12*a*t**2*np.exp(-1j*t*w)*np.cos(a*w) + t**4*w*np.exp(-1j*t*w)*np.sin(a*w) + 4*1j*t**3*np.exp(-1j*t*w)*np.sin(a*w)
		if k==5:
			return a**5*w*np.exp(-1j*t*w)*np.cos(a*w) - 5*1j*a**4*t*w*np.exp(-1j*t*w)*np.sin(a*w) + 5*a**4*np.exp(-1j*t*w)*np.sin(a*w) + 10*a**3*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) + 20*1j*a**3*t*np.exp(-1j*t*w)*np.cos(a*w) - 10*1j*a**2*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) + 30*a**2*t**2*np.exp(-1j*t*w)*np.sin(a*w) + 5*a*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) + 20*1j*a*t**3*np.exp(-1j*t*w)*np.cos(a*w) - 1j*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) + 5*t**4*np.exp(-1j*t*w)*np.sin(a*w)
		if k==6:
			return -a**6*w*np.exp(-1j*t*w)*np.sin(a*w) - 6*1j*a**5*t*w*np.exp(-1j*t*w)*np.cos(a*w) + 6*a**5*np.exp(-1j*t*w)*np.cos(a*w) - 15*a**4*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) - 30*1j*a**4*t*np.exp(-1j*t*w)*np.sin(a*w) - 20*1j*a**3*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) + 60*a**3*t**2*np.exp(-1j*t*w)*np.cos(a*w) - 15*a**2*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) - 60*1j*a**2*t**3*np.exp(-1j*t*w)*np.sin(a*w) - 6*1j*a*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) + 30*a*t**4*np.exp(-1j*t*w)*np.cos(a*w) - t**6*w*np.exp(-1j*t*w)*np.sin(a*w) - 6*1j*t**5*np.exp(-1j*t*w)*np.sin(a*w)
		if k==7:
			return -a**7*w*np.exp(-1j*t*w)*np.cos(a*w) + 7*1j*a**6*t*w*np.exp(-1j*t*w)*np.sin(a*w) - 7*a**6*np.exp(-1j*t*w)*np.sin(a*w) - 21*a**5*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) - 42*1j*a**5*t*np.exp(-1j*t*w)*np.cos(a*w) + 35*1j*a**4*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) - 105*a**4*t**2*np.exp(-1j*t*w)*np.sin(a*w) - 35*a**3*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) - 140*1j*a**3*t**3*np.exp(-1j*t*w)*np.cos(a*w) + 21*1j*a**2*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) - 105*a**2*t**4*np.exp(-1j*t*w)*np.sin(a*w) - 7*a*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) - 42*1j*a*t**5*np.exp(-1j*t*w)*np.cos(a*w) + 1j*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) - 7*t**6*np.exp(-1j*t*w)*np.sin(a*w)
		if k==8:
			return a**8*w*np.exp(-1j*t*w)*np.sin(a*w) + 8*1j*a**7*t*w*np.exp(-1j*t*w)*np.cos(a*w) - 8*a**7*np.exp(-1j*t*w)*np.cos(a*w) + 28*a**6*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) + 56*1j*a**6*t*np.exp(-1j*t*w)*np.sin(a*w) + 56*1j*a**5*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) - 168*a**5*t**2*np.exp(-1j*t*w)*np.cos(a*w) + 70*a**4*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) + 280*1j*a**4*t**3*np.exp(-1j*t*w)*np.sin(a*w) + 56*1j*a**3*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) - 280*a**3*t**4*np.exp(-1j*t*w)*np.cos(a*w) + 28*a**2*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) + 168*1j*a**2*t**5*np.exp(-1j*t*w)*np.sin(a*w) + 8*1j*a*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) - 56*a*t**6*np.exp(-1j*t*w)*np.cos(a*w) + t**8*w*np.exp(-1j*t*w)*np.sin(a*w) + 8*1j*t**7*np.exp(-1j*t*w)*np.sin(a*w)
		if k==9:
			return a**9*w*np.exp(-1j*t*w)*np.cos(a*w) - 9*1j*a**8*t*w*np.exp(-1j*t*w)*np.sin(a*w) + 9*a**8*np.exp(-1j*t*w)*np.sin(a*w) + 36*a**7*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) + 72*1j*a**7*t*np.exp(-1j*t*w)*np.cos(a*w) - 84*1j*a**6*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) + 252*a**6*t**2*np.exp(-1j*t*w)*np.sin(a*w) + 126*a**5*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) + 504*1j*a**5*t**3*np.exp(-1j*t*w)*np.cos(a*w) - 126*1j*a**4*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) + 630*a**4*t**4*np.exp(-1j*t*w)*np.sin(a*w) + 84*a**3*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) + 504*1j*a**3*t**5*np.exp(-1j*t*w)*np.cos(a*w) - 36*1j*a**2*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) + 252*a**2*t**6*np.exp(-1j*t*w)*np.sin(a*w) + 9*a*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) + 72*1j*a*t**7*np.exp(-1j*t*w)*np.cos(a*w) - 1j*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) + 9*t**8*np.exp(-1j*t*w)*np.sin(a*w)
		if k==10:
			return -a**10*w*np.exp(-1j*t*w)*np.sin(a*w) - 10*1j*a**9*t*w*np.exp(-1j*t*w)*np.cos(a*w) + 10*a**9*np.exp(-1j*t*w)*np.cos(a*w) - 45*a**8*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) - 90*1j*a**8*t*np.exp(-1j*t*w)*np.sin(a*w) - 120*1j*a**7*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) + 360*a**7*t**2*np.exp(-1j*t*w)*np.cos(a*w) - 210*a**6*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) - 840*1j*a**6*t**3*np.exp(-1j*t*w)*np.sin(a*w) - 252*1j*a**5*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) + 1260*a**5*t**4*np.exp(-1j*t*w)*np.cos(a*w) - 210*a**4*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) - 1260*1j*a**4*t**5*np.exp(-1j*t*w)*np.sin(a*w) - 120*1j*a**3*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) + 840*a**3*t**6*np.exp(-1j*t*w)*np.cos(a*w) - 45*a**2*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) - 360*1j*a**2*t**7*np.exp(-1j*t*w)*np.sin(a*w) - 10*1j*a*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) + 90*a*t**8*np.exp(-1j*t*w)*np.cos(a*w) - t**10*w*np.exp(-1j*t*w)*np.sin(a*w) - 10*1j*t**9*np.exp(-1j*t*w)*np.sin(a*w)
		if k==11:
			return -a**11*w*np.exp(-1j*t*w)*np.cos(a*w) + 11*1j*a**10*t*w*np.exp(-1j*t*w)*np.sin(a*w) - 11*a**10*np.exp(-1j*t*w)*np.sin(a*w) - 55*a**9*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) - 110*1j*a**9*t*np.exp(-1j*t*w)*np.cos(a*w) + 165*1j*a**8*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) - 495*a**8*t**2*np.exp(-1j*t*w)*np.sin(a*w) - 330*a**7*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) - 1320*1j*a**7*t**3*np.exp(-1j*t*w)*np.cos(a*w) + 462*1j*a**6*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) - 2310*a**6*t**4*np.exp(-1j*t*w)*np.sin(a*w) - 462*a**5*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) - 2772*1j*a**5*t**5*np.exp(-1j*t*w)*np.cos(a*w) + 330*1j*a**4*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) - 2310*a**4*t**6*np.exp(-1j*t*w)*np.sin(a*w) - 165*a**3*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) - 1320*1j*a**3*t**7*np.exp(-1j*t*w)*np.cos(a*w) + 55*1j*a**2*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) - 495*a**2*t**8*np.exp(-1j*t*w)*np.sin(a*w) - 11*a*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) - 110*1j*a*t**9*np.exp(-1j*t*w)*np.cos(a*w) + 1j*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) - 11*t**10*np.exp(-1j*t*w)*np.sin(a*w)
		if k==12:
			return a**12*w*np.exp(-1j*t*w)*np.sin(a*w) + 12*1j*a**11*t*w*np.exp(-1j*t*w)*np.cos(a*w) - 12*a**11*np.exp(-1j*t*w)*np.cos(a*w) + 66*a**10*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) + 132*1j*a**10*t*np.exp(-1j*t*w)*np.sin(a*w) + 220*1j*a**9*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) - 660*a**9*t**2*np.exp(-1j*t*w)*np.cos(a*w) + 495*a**8*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) + 1980*1j*a**8*t**3*np.exp(-1j*t*w)*np.sin(a*w) + 792*1j*a**7*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) - 3960*a**7*t**4*np.exp(-1j*t*w)*np.cos(a*w) + 924*a**6*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) + 5544*1j*a**6*t**5*np.exp(-1j*t*w)*np.sin(a*w) + 792*1j*a**5*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) - 5544*a**5*t**6*np.exp(-1j*t*w)*np.cos(a*w) + 495*a**4*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) + 3960*1j*a**4*t**7*np.exp(-1j*t*w)*np.sin(a*w) + 220*1j*a**3*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) - 1980*a**3*t**8*np.exp(-1j*t*w)*np.cos(a*w) + 66*a**2*t**10*w*np.exp(-1j*t*w)*np.sin(a*w) + 660*1j*a**2*t**9*np.exp(-1j*t*w)*np.sin(a*w) + 12*1j*a*t**11*w*np.exp(-1j*t*w)*np.cos(a*w) - 132*a*t**10*np.exp(-1j*t*w)*np.cos(a*w) + t**12*w*np.exp(-1j*t*w)*np.sin(a*w) + 12*1j*t**11*np.exp(-1j*t*w)*np.sin(a*w)
		if k==13:
			return a**13*w*np.exp(-1j*t*w)*np.cos(a*w) - 13*1j*a**12*t*w*np.exp(-1j*t*w)*np.sin(a*w) + 13*a**12*np.exp(-1j*t*w)*np.sin(a*w) + 78*a**11*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) + 156*1j*a**11*t*np.exp(-1j*t*w)*np.cos(a*w) - 286*1j*a**10*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) + 858*a**10*t**2*np.exp(-1j*t*w)*np.sin(a*w) + 715*a**9*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) + 2860*1j*a**9*t**3*np.exp(-1j*t*w)*np.cos(a*w) - 1287*1j*a**8*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) + 6435*a**8*t**4*np.exp(-1j*t*w)*np.sin(a*w) + 1716*a**7*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) + 10296*1j*a**7*t**5*np.exp(-1j*t*w)*np.cos(a*w) - 1716*1j*a**6*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) + 12012*a**6*t**6*np.exp(-1j*t*w)*np.sin(a*w) + 1287*a**5*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) + 10296*1j*a**5*t**7*np.exp(-1j*t*w)*np.cos(a*w) - 715*1j*a**4*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) + 6435*a**4*t**8*np.exp(-1j*t*w)*np.sin(a*w) + 286*a**3*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) + 2860*1j*a**3*t**9*np.exp(-1j*t*w)*np.cos(a*w) - 78*1j*a**2*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) + 858*a**2*t**10*np.exp(-1j*t*w)*np.sin(a*w) + 13*a*t**12*w*np.exp(-1j*t*w)*np.cos(a*w) + 156*1j*a*t**11*np.exp(-1j*t*w)*np.cos(a*w) - 1j*t**13*w*np.exp(-1j*t*w)*np.sin(a*w) + 13*t**12*np.exp(-1j*t*w)*np.sin(a*w)
		if k==14:
			return -a**14*w*np.exp(-1j*t*w)*np.sin(a*w) - 14*1j*a**13*t*w*np.exp(-1j*t*w)*np.cos(a*w) + 14*a**13*np.exp(-1j*t*w)*np.cos(a*w) - 91*a**12*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) - 182*1j*a**12*t*np.exp(-1j*t*w)*np.sin(a*w) - 364*1j*a**11*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) + 1092*a**11*t**2*np.exp(-1j*t*w)*np.cos(a*w) - 1001*a**10*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) - 4004*1j*a**10*t**3*np.exp(-1j*t*w)*np.sin(a*w) - 2002*1j*a**9*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) + 10010*a**9*t**4*np.exp(-1j*t*w)*np.cos(a*w) - 3003*a**8*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) - 18018*1j*a**8*t**5*np.exp(-1j*t*w)*np.sin(a*w) - 3432*1j*a**7*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) + 24024*a**7*t**6*np.exp(-1j*t*w)*np.cos(a*w) - 3003*a**6*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) - 24024*1j*a**6*t**7*np.exp(-1j*t*w)*np.sin(a*w) - 2002*1j*a**5*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) + 18018*a**5*t**8*np.exp(-1j*t*w)*np.cos(a*w) - 1001*a**4*t**10*w*np.exp(-1j*t*w)*np.sin(a*w) - 10010*1j*a**4*t**9*np.exp(-1j*t*w)*np.sin(a*w) - 364*1j*a**3*t**11*w*np.exp(-1j*t*w)*np.cos(a*w) + 4004*a**3*t**10*np.exp(-1j*t*w)*np.cos(a*w) - 91*a**2*t**12*w*np.exp(-1j*t*w)*np.sin(a*w) - 1092*1j*a**2*t**11*np.exp(-1j*t*w)*np.sin(a*w) - 14*1j*a*t**13*w*np.exp(-1j*t*w)*np.cos(a*w) + 182*a*t**12*np.exp(-1j*t*w)*np.cos(a*w) - t**14*w*np.exp(-1j*t*w)*np.sin(a*w) - 14*1j*t**13*np.exp(-1j*t*w)*np.sin(a*w)
		if k==15:
			return -a**15*w*np.exp(-1j*t*w)*np.cos(a*w) + 15*1j*a**14*t*w*np.exp(-1j*t*w)*np.sin(a*w) - 15*a**14*np.exp(-1j*t*w)*np.sin(a*w) - 105*a**13*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) - 210*1j*a**13*t*np.exp(-1j*t*w)*np.cos(a*w) + 455*1j*a**12*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) - 1365*a**12*t**2*np.exp(-1j*t*w)*np.sin(a*w) - 1365*a**11*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) - 5460*1j*a**11*t**3*np.exp(-1j*t*w)*np.cos(a*w) + 3003*1j*a**10*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) - 15015*a**10*t**4*np.exp(-1j*t*w)*np.sin(a*w) - 5005*a**9*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) - 30030*1j*a**9*t**5*np.exp(-1j*t*w)*np.cos(a*w) + 6435*1j*a**8*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) - 45045*a**8*t**6*np.exp(-1j*t*w)*np.sin(a*w) - 6435*a**7*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) - 51480*1j*a**7*t**7*np.exp(-1j*t*w)*np.cos(a*w) + 5005*1j*a**6*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) - 45045*a**6*t**8*np.exp(-1j*t*w)*np.sin(a*w) - 3003*a**5*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) - 30030*1j*a**5*t**9*np.exp(-1j*t*w)*np.cos(a*w) + 1365*1j*a**4*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) - 15015*a**4*t**10*np.exp(-1j*t*w)*np.sin(a*w) - 455*a**3*t**12*w*np.exp(-1j*t*w)*np.cos(a*w) - 5460*1j*a**3*t**11*np.exp(-1j*t*w)*np.cos(a*w) + 105*1j*a**2*t**13*w*np.exp(-1j*t*w)*np.sin(a*w) - 1365*a**2*t**12*np.exp(-1j*t*w)*np.sin(a*w) - 15*a*t**14*w*np.exp(-1j*t*w)*np.cos(a*w) - 210*1j*a*t**13*np.exp(-1j*t*w)*np.cos(a*w) + 1j*t**15*w*np.exp(-1j*t*w)*np.sin(a*w) - 15*t**14*np.exp(-1j*t*w)*np.sin(a*w)
		if k==16:
			return a**16*w*np.exp(-1j*t*w)*np.sin(a*w) + 16*1j*a**15*t*w*np.exp(-1j*t*w)*np.cos(a*w) - 16*a**15*np.exp(-1j*t*w)*np.cos(a*w) + 120*a**14*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) + 240*1j*a**14*t*np.exp(-1j*t*w)*np.sin(a*w) + 560*1j*a**13*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) - 1680*a**13*t**2*np.exp(-1j*t*w)*np.cos(a*w) + 1820*a**12*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) + 7280*1j*a**12*t**3*np.exp(-1j*t*w)*np.sin(a*w) + 4368*1j*a**11*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) - 21840*a**11*t**4*np.exp(-1j*t*w)*np.cos(a*w) + 8008*a**10*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) + 48048*1j*a**10*t**5*np.exp(-1j*t*w)*np.sin(a*w) + 11440*1j*a**9*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) - 80080*a**9*t**6*np.exp(-1j*t*w)*np.cos(a*w) + 12870*a**8*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) + 102960*1j*a**8*t**7*np.exp(-1j*t*w)*np.sin(a*w) + 11440*1j*a**7*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) - 102960*a**7*t**8*np.exp(-1j*t*w)*np.cos(a*w) + 8008*a**6*t**10*w*np.exp(-1j*t*w)*np.sin(a*w) + 80080*1j*a**6*t**9*np.exp(-1j*t*w)*np.sin(a*w) + 4368*1j*a**5*t**11*w*np.exp(-1j*t*w)*np.cos(a*w) - 48048*a**5*t**10*np.exp(-1j*t*w)*np.cos(a*w) + 1820*a**4*t**12*w*np.exp(-1j*t*w)*np.sin(a*w) + 21840*1j*a**4*t**11*np.exp(-1j*t*w)*np.sin(a*w) + 560*1j*a**3*t**13*w*np.exp(-1j*t*w)*np.cos(a*w) - 7280*a**3*t**12*np.exp(-1j*t*w)*np.cos(a*w) + 120*a**2*t**14*w*np.exp(-1j*t*w)*np.sin(a*w) + 1680*1j*a**2*t**13*np.exp(-1j*t*w)*np.sin(a*w) + 16*1j*a*t**15*w*np.exp(-1j*t*w)*np.cos(a*w) - 240*a*t**14*np.exp(-1j*t*w)*np.cos(a*w) + t**16*w*np.exp(-1j*t*w)*np.sin(a*w) + 16*1j*t**15*np.exp(-1j*t*w)*np.sin(a*w)
		if k==17:
			return a**17*w*np.exp(-1j*t*w)*np.cos(a*w) - 17*1j*a**16*t*w*np.exp(-1j*t*w)*np.sin(a*w) + 17*a**16*np.exp(-1j*t*w)*np.sin(a*w) + 136*a**15*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) + 272*1j*a**15*t*np.exp(-1j*t*w)*np.cos(a*w) - 680*1j*a**14*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) + 2040*a**14*t**2*np.exp(-1j*t*w)*np.sin(a*w) + 2380*a**13*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) + 9520*1j*a**13*t**3*np.exp(-1j*t*w)*np.cos(a*w) - 6188*1j*a**12*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) + 30940*a**12*t**4*np.exp(-1j*t*w)*np.sin(a*w) + 12376*a**11*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) + 74256*1j*a**11*t**5*np.exp(-1j*t*w)*np.cos(a*w) - 19448*1j*a**10*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) + 136136*a**10*t**6*np.exp(-1j*t*w)*np.sin(a*w) + 24310*a**9*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) + 194480*1j*a**9*t**7*np.exp(-1j*t*w)*np.cos(a*w) - 24310*1j*a**8*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) + 218790*a**8*t**8*np.exp(-1j*t*w)*np.sin(a*w) + 19448*a**7*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) + 194480*1j*a**7*t**9*np.exp(-1j*t*w)*np.cos(a*w) - 12376*1j*a**6*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) + 136136*a**6*t**10*np.exp(-1j*t*w)*np.sin(a*w) + 6188*a**5*t**12*w*np.exp(-1j*t*w)*np.cos(a*w) + 74256*1j*a**5*t**11*np.exp(-1j*t*w)*np.cos(a*w) - 2380*1j*a**4*t**13*w*np.exp(-1j*t*w)*np.sin(a*w) + 30940*a**4*t**12*np.exp(-1j*t*w)*np.sin(a*w) + 680*a**3*t**14*w*np.exp(-1j*t*w)*np.cos(a*w) + 9520*1j*a**3*t**13*np.exp(-1j*t*w)*np.cos(a*w) - 136*1j*a**2*t**15*w*np.exp(-1j*t*w)*np.sin(a*w) + 2040*a**2*t**14*np.exp(-1j*t*w)*np.sin(a*w) + 17*a*t**16*w*np.exp(-1j*t*w)*np.cos(a*w) + 272*1j*a*t**15*np.exp(-1j*t*w)*np.cos(a*w) - 1j*t**17*w*np.exp(-1j*t*w)*np.sin(a*w) + 17*t**16*np.exp(-1j*t*w)*np.sin(a*w)
		if k==18:
			return -a**18*w*np.exp(-1j*t*w)*np.sin(a*w) - 18*1j*a**17*t*w*np.exp(-1j*t*w)*np.cos(a*w) + 18*a**17*np.exp(-1j*t*w)*np.cos(a*w) - 153*a**16*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) - 306*1j*a**16*t*np.exp(-1j*t*w)*np.sin(a*w) - 816*1j*a**15*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) + 2448*a**15*t**2*np.exp(-1j*t*w)*np.cos(a*w) - 3060*a**14*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) - 12240*1j*a**14*t**3*np.exp(-1j*t*w)*np.sin(a*w) - 8568*1j*a**13*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) + 42840*a**13*t**4*np.exp(-1j*t*w)*np.cos(a*w) - 18564*a**12*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) - 111384*1j*a**12*t**5*np.exp(-1j*t*w)*np.sin(a*w) - 31824*1j*a**11*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) + 222768*a**11*t**6*np.exp(-1j*t*w)*np.cos(a*w) - 43758*a**10*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) - 350064*1j*a**10*t**7*np.exp(-1j*t*w)*np.sin(a*w) - 48620*1j*a**9*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) + 437580*a**9*t**8*np.exp(-1j*t*w)*np.cos(a*w) - 43758*a**8*t**10*w*np.exp(-1j*t*w)*np.sin(a*w) - 437580*1j*a**8*t**9*np.exp(-1j*t*w)*np.sin(a*w) - 31824*1j*a**7*t**11*w*np.exp(-1j*t*w)*np.cos(a*w) + 350064*a**7*t**10*np.exp(-1j*t*w)*np.cos(a*w) - 18564*a**6*t**12*w*np.exp(-1j*t*w)*np.sin(a*w) - 222768*1j*a**6*t**11*np.exp(-1j*t*w)*np.sin(a*w) - 8568*1j*a**5*t**13*w*np.exp(-1j*t*w)*np.cos(a*w) + 111384*a**5*t**12*np.exp(-1j*t*w)*np.cos(a*w) - 3060*a**4*t**14*w*np.exp(-1j*t*w)*np.sin(a*w) - 42840*1j*a**4*t**13*np.exp(-1j*t*w)*np.sin(a*w) - 816*1j*a**3*t**15*w*np.exp(-1j*t*w)*np.cos(a*w) + 12240*a**3*t**14*np.exp(-1j*t*w)*np.cos(a*w) - 153*a**2*t**16*w*np.exp(-1j*t*w)*np.sin(a*w) - 2448*1j*a**2*t**15*np.exp(-1j*t*w)*np.sin(a*w) - 18*1j*a*t**17*w*np.exp(-1j*t*w)*np.cos(a*w) + 306*a*t**16*np.exp(-1j*t*w)*np.cos(a*w) - t**18*w*np.exp(-1j*t*w)*np.sin(a*w) - 18*1j*t**17*np.exp(-1j*t*w)*np.sin(a*w)
		if k==19:
			return -a**19*w*np.exp(-1j*t*w)*np.cos(a*w) + 19*1j*a**18*t*w*np.exp(-1j*t*w)*np.sin(a*w) - 19*a**18*np.exp(-1j*t*w)*np.sin(a*w) - 171*a**17*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) - 342*1j*a**17*t*np.exp(-1j*t*w)*np.cos(a*w) + 969*1j*a**16*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) - 2907*a**16*t**2*np.exp(-1j*t*w)*np.sin(a*w) - 3876*a**15*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) - 15504*1j*a**15*t**3*np.exp(-1j*t*w)*np.cos(a*w) + 11628*1j*a**14*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) - 58140*a**14*t**4*np.exp(-1j*t*w)*np.sin(a*w) - 27132*a**13*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) - 162792*1j*a**13*t**5*np.exp(-1j*t*w)*np.cos(a*w) + 50388*1j*a**12*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) - 352716*a**12*t**6*np.exp(-1j*t*w)*np.sin(a*w) - 75582*a**11*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) - 604656*1j*a**11*t**7*np.exp(-1j*t*w)*np.cos(a*w) + 92378*1j*a**10*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) - 831402*a**10*t**8*np.exp(-1j*t*w)*np.sin(a*w) - 92378*a**9*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) - 923780*1j*a**9*t**9*np.exp(-1j*t*w)*np.cos(a*w) + 75582*1j*a**8*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) - 831402*a**8*t**10*np.exp(-1j*t*w)*np.sin(a*w) - 50388*a**7*t**12*w*np.exp(-1j*t*w)*np.cos(a*w) - 604656*1j*a**7*t**11*np.exp(-1j*t*w)*np.cos(a*w) + 27132*1j*a**6*t**13*w*np.exp(-1j*t*w)*np.sin(a*w) - 352716*a**6*t**12*np.exp(-1j*t*w)*np.sin(a*w) - 11628*a**5*t**14*w*np.exp(-1j*t*w)*np.cos(a*w) - 162792*1j*a**5*t**13*np.exp(-1j*t*w)*np.cos(a*w) + 3876*1j*a**4*t**15*w*np.exp(-1j*t*w)*np.sin(a*w) - 58140*a**4*t**14*np.exp(-1j*t*w)*np.sin(a*w) - 969*a**3*t**16*w*np.exp(-1j*t*w)*np.cos(a*w) - 15504*1j*a**3*t**15*np.exp(-1j*t*w)*np.cos(a*w) + 171*1j*a**2*t**17*w*np.exp(-1j*t*w)*np.sin(a*w) - 2907*a**2*t**16*np.exp(-1j*t*w)*np.sin(a*w) - 19*a*t**18*w*np.exp(-1j*t*w)*np.cos(a*w) - 342*1j*a*t**17*np.exp(-1j*t*w)*np.cos(a*w) + 1j*t**19*w*np.exp(-1j*t*w)*np.sin(a*w) - 19*t**18*np.exp(-1j*t*w)*np.sin(a*w)
		if k==20:
			return a**20*w*np.exp(-1j*t*w)*np.sin(a*w) + 20*1j*a**19*t*w*np.exp(-1j*t*w)*np.cos(a*w) - 20*a**19*np.exp(-1j*t*w)*np.cos(a*w) + 190*a**18*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) + 380*1j*a**18*t*np.exp(-1j*t*w)*np.sin(a*w) + 1140*1j*a**17*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) - 3420*a**17*t**2*np.exp(-1j*t*w)*np.cos(a*w) + 4845*a**16*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) + 19380*1j*a**16*t**3*np.exp(-1j*t*w)*np.sin(a*w) + 15504*1j*a**15*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) - 77520*a**15*t**4*np.exp(-1j*t*w)*np.cos(a*w) + 38760*a**14*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) + 232560*1j*a**14*t**5*np.exp(-1j*t*w)*np.sin(a*w) + 77520*1j*a**13*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) - 542640*a**13*t**6*np.exp(-1j*t*w)*np.cos(a*w) + 125970*a**12*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) + 1007760*1j*a**12*t**7*np.exp(-1j*t*w)*np.sin(a*w) + 167960*1j*a**11*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) - 1511640*a**11*t**8*np.exp(-1j*t*w)*np.cos(a*w) + 184756*a**10*t**10*w*np.exp(-1j*t*w)*np.sin(a*w) + 1847560*1j*a**10*t**9*np.exp(-1j*t*w)*np.sin(a*w) + 167960*1j*a**9*t**11*w*np.exp(-1j*t*w)*np.cos(a*w) - 1847560*a**9*t**10*np.exp(-1j*t*w)*np.cos(a*w) + 125970*a**8*t**12*w*np.exp(-1j*t*w)*np.sin(a*w) + 1511640*1j*a**8*t**11*np.exp(-1j*t*w)*np.sin(a*w) + 77520*1j*a**7*t**13*w*np.exp(-1j*t*w)*np.cos(a*w) - 1007760*a**7*t**12*np.exp(-1j*t*w)*np.cos(a*w) + 38760*a**6*t**14*w*np.exp(-1j*t*w)*np.sin(a*w) + 542640*1j*a**6*t**13*np.exp(-1j*t*w)*np.sin(a*w) + 15504*1j*a**5*t**15*w*np.exp(-1j*t*w)*np.cos(a*w) - 232560*a**5*t**14*np.exp(-1j*t*w)*np.cos(a*w) + 4845*a**4*t**16*w*np.exp(-1j*t*w)*np.sin(a*w) + 77520*1j*a**4*t**15*np.exp(-1j*t*w)*np.sin(a*w) + 1140*1j*a**3*t**17*w*np.exp(-1j*t*w)*np.cos(a*w) - 19380*a**3*t**16*np.exp(-1j*t*w)*np.cos(a*w) + 190*a**2*t**18*w*np.exp(-1j*t*w)*np.sin(a*w) + 3420*1j*a**2*t**17*np.exp(-1j*t*w)*np.sin(a*w) + 20*1j*a*t**19*w*np.exp(-1j*t*w)*np.cos(a*w) - 380*a*t**18*np.exp(-1j*t*w)*np.cos(a*w) + t**20*w*np.exp(-1j*t*w)*np.sin(a*w) + 20*1j*t**19*np.exp(-1j*t*w)*np.sin(a*w)
		if k==21:
			return a**21*w*np.exp(-1j*t*w)*np.cos(a*w) - 21*1j*a**20*t*w*np.exp(-1j*t*w)*np.sin(a*w) + 21*a**20*np.exp(-1j*t*w)*np.sin(a*w) + 210*a**19*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) + 420*1j*a**19*t*np.exp(-1j*t*w)*np.cos(a*w) - 1330*1j*a**18*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) + 3990*a**18*t**2*np.exp(-1j*t*w)*np.sin(a*w) + 5985*a**17*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) + 23940*1j*a**17*t**3*np.exp(-1j*t*w)*np.cos(a*w) - 20349*1j*a**16*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) + 101745*a**16*t**4*np.exp(-1j*t*w)*np.sin(a*w) + 54264*a**15*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) + 325584*1j*a**15*t**5*np.exp(-1j*t*w)*np.cos(a*w) - 116280*1j*a**14*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) + 813960*a**14*t**6*np.exp(-1j*t*w)*np.sin(a*w) + 203490*a**13*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) + 1627920*1j*a**13*t**7*np.exp(-1j*t*w)*np.cos(a*w) - 293930*1j*a**12*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) + 2645370*a**12*t**8*np.exp(-1j*t*w)*np.sin(a*w) + 352716*a**11*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) + 3527160*1j*a**11*t**9*np.exp(-1j*t*w)*np.cos(a*w) - 352716*1j*a**10*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) + 3879876*a**10*t**10*np.exp(-1j*t*w)*np.sin(a*w) + 293930*a**9*t**12*w*np.exp(-1j*t*w)*np.cos(a*w) + 3527160*1j*a**9*t**11*np.exp(-1j*t*w)*np.cos(a*w) - 203490*1j*a**8*t**13*w*np.exp(-1j*t*w)*np.sin(a*w) + 2645370*a**8*t**12*np.exp(-1j*t*w)*np.sin(a*w) + 116280*a**7*t**14*w*np.exp(-1j*t*w)*np.cos(a*w) + 1627920*1j*a**7*t**13*np.exp(-1j*t*w)*np.cos(a*w) - 54264*1j*a**6*t**15*w*np.exp(-1j*t*w)*np.sin(a*w) + 813960*a**6*t**14*np.exp(-1j*t*w)*np.sin(a*w) + 20349*a**5*t**16*w*np.exp(-1j*t*w)*np.cos(a*w) + 325584*1j*a**5*t**15*np.exp(-1j*t*w)*np.cos(a*w) - 5985*1j*a**4*t**17*w*np.exp(-1j*t*w)*np.sin(a*w) + 101745*a**4*t**16*np.exp(-1j*t*w)*np.sin(a*w) + 1330*a**3*t**18*w*np.exp(-1j*t*w)*np.cos(a*w) + 23940*1j*a**3*t**17*np.exp(-1j*t*w)*np.cos(a*w) - 210*1j*a**2*t**19*w*np.exp(-1j*t*w)*np.sin(a*w) + 3990*a**2*t**18*np.exp(-1j*t*w)*np.sin(a*w) + 21*a*t**20*w*np.exp(-1j*t*w)*np.cos(a*w) + 420*1j*a*t**19*np.exp(-1j*t*w)*np.cos(a*w) - 1j*t**21*w*np.exp(-1j*t*w)*np.sin(a*w) + 21*t**20*np.exp(-1j*t*w)*np.sin(a*w)
		if k==22:
			return -a**22*w*np.exp(-1j*t*w)*np.sin(a*w) - 22*1j*a**21*t*w*np.exp(-1j*t*w)*np.cos(a*w) + 22*a**21*np.exp(-1j*t*w)*np.cos(a*w) - 231*a**20*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) - 462*1j*a**20*t*np.exp(-1j*t*w)*np.sin(a*w) - 1540*1j*a**19*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) + 4620*a**19*t**2*np.exp(-1j*t*w)*np.cos(a*w) - 7315*a**18*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) - 29260*1j*a**18*t**3*np.exp(-1j*t*w)*np.sin(a*w) - 26334*1j*a**17*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) + 131670*a**17*t**4*np.exp(-1j*t*w)*np.cos(a*w) - 74613*a**16*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) - 447678*1j*a**16*t**5*np.exp(-1j*t*w)*np.sin(a*w) - 170544*1j*a**15*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) + 1193808*a**15*t**6*np.exp(-1j*t*w)*np.cos(a*w) - 319770*a**14*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) - 2558160*1j*a**14*t**7*np.exp(-1j*t*w)*np.sin(a*w) - 497420*1j*a**13*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) + 4476780*a**13*t**8*np.exp(-1j*t*w)*np.cos(a*w) - 646646*a**12*t**10*w*np.exp(-1j*t*w)*np.sin(a*w) - 6466460*1j*a**12*t**9*np.exp(-1j*t*w)*np.sin(a*w) - 705432*1j*a**11*t**11*w*np.exp(-1j*t*w)*np.cos(a*w) + 7759752*a**11*t**10*np.exp(-1j*t*w)*np.cos(a*w) - 646646*a**10*t**12*w*np.exp(-1j*t*w)*np.sin(a*w) - 7759752*1j*a**10*t**11*np.exp(-1j*t*w)*np.sin(a*w) - 497420*1j*a**9*t**13*w*np.exp(-1j*t*w)*np.cos(a*w) + 6466460*a**9*t**12*np.exp(-1j*t*w)*np.cos(a*w) - 319770*a**8*t**14*w*np.exp(-1j*t*w)*np.sin(a*w) - 4476780*1j*a**8*t**13*np.exp(-1j*t*w)*np.sin(a*w) - 170544*1j*a**7*t**15*w*np.exp(-1j*t*w)*np.cos(a*w) + 2558160*a**7*t**14*np.exp(-1j*t*w)*np.cos(a*w) - 74613*a**6*t**16*w*np.exp(-1j*t*w)*np.sin(a*w) - 1193808*1j*a**6*t**15*np.exp(-1j*t*w)*np.sin(a*w) - 26334*1j*a**5*t**17*w*np.exp(-1j*t*w)*np.cos(a*w) + 447678*a**5*t**16*np.exp(-1j*t*w)*np.cos(a*w) - 7315*a**4*t**18*w*np.exp(-1j*t*w)*np.sin(a*w) - 131670*1j*a**4*t**17*np.exp(-1j*t*w)*np.sin(a*w) - 1540*1j*a**3*t**19*w*np.exp(-1j*t*w)*np.cos(a*w) + 29260*a**3*t**18*np.exp(-1j*t*w)*np.cos(a*w) - 231*a**2*t**20*w*np.exp(-1j*t*w)*np.sin(a*w) - 4620*1j*a**2*t**19*np.exp(-1j*t*w)*np.sin(a*w) - 22*1j*a*t**21*w*np.exp(-1j*t*w)*np.cos(a*w) + 462*a*t**20*np.exp(-1j*t*w)*np.cos(a*w) - t**22*w*np.exp(-1j*t*w)*np.sin(a*w) - 22*1j*t**21*np.exp(-1j*t*w)*np.sin(a*w)
		if k==23:
			return -a**23*w*np.exp(-1j*t*w)*np.cos(a*w) + 23*1j*a**22*t*w*np.exp(-1j*t*w)*np.sin(a*w) - 23*a**22*np.exp(-1j*t*w)*np.sin(a*w) - 253*a**21*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) - 506*1j*a**21*t*np.exp(-1j*t*w)*np.cos(a*w) + 1771*1j*a**20*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) - 5313*a**20*t**2*np.exp(-1j*t*w)*np.sin(a*w) - 8855*a**19*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) - 35420*1j*a**19*t**3*np.exp(-1j*t*w)*np.cos(a*w) + 33649*1j*a**18*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) - 168245*a**18*t**4*np.exp(-1j*t*w)*np.sin(a*w) - 100947*a**17*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) - 605682*1j*a**17*t**5*np.exp(-1j*t*w)*np.cos(a*w) + 245157*1j*a**16*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) - 1716099*a**16*t**6*np.exp(-1j*t*w)*np.sin(a*w) - 490314*a**15*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) - 3922512*1j*a**15*t**7*np.exp(-1j*t*w)*np.cos(a*w) + 817190*1j*a**14*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) - 7354710*a**14*t**8*np.exp(-1j*t*w)*np.sin(a*w) - 1144066*a**13*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) - 11440660*1j*a**13*t**9*np.exp(-1j*t*w)*np.cos(a*w) + 1352078*1j*a**12*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) - 14872858*a**12*t**10*np.exp(-1j*t*w)*np.sin(a*w) - 1352078*a**11*t**12*w*np.exp(-1j*t*w)*np.cos(a*w) - 16224936*1j*a**11*t**11*np.exp(-1j*t*w)*np.cos(a*w) + 1144066*1j*a**10*t**13*w*np.exp(-1j*t*w)*np.sin(a*w) - 14872858*a**10*t**12*np.exp(-1j*t*w)*np.sin(a*w) - 817190*a**9*t**14*w*np.exp(-1j*t*w)*np.cos(a*w) - 11440660*1j*a**9*t**13*np.exp(-1j*t*w)*np.cos(a*w) + 490314*1j*a**8*t**15*w*np.exp(-1j*t*w)*np.sin(a*w) - 7354710*a**8*t**14*np.exp(-1j*t*w)*np.sin(a*w) - 245157*a**7*t**16*w*np.exp(-1j*t*w)*np.cos(a*w) - 3922512*1j*a**7*t**15*np.exp(-1j*t*w)*np.cos(a*w) + 100947*1j*a**6*t**17*w*np.exp(-1j*t*w)*np.sin(a*w) - 1716099*a**6*t**16*np.exp(-1j*t*w)*np.sin(a*w) - 33649*a**5*t**18*w*np.exp(-1j*t*w)*np.cos(a*w) - 605682*1j*a**5*t**17*np.exp(-1j*t*w)*np.cos(a*w) + 8855*1j*a**4*t**19*w*np.exp(-1j*t*w)*np.sin(a*w) - 168245*a**4*t**18*np.exp(-1j*t*w)*np.sin(a*w) - 1771*a**3*t**20*w*np.exp(-1j*t*w)*np.cos(a*w) - 35420*1j*a**3*t**19*np.exp(-1j*t*w)*np.cos(a*w) + 253*1j*a**2*t**21*w*np.exp(-1j*t*w)*np.sin(a*w) - 5313*a**2*t**20*np.exp(-1j*t*w)*np.sin(a*w) - 23*a*t**22*w*np.exp(-1j*t*w)*np.cos(a*w) - 506*1j*a*t**21*np.exp(-1j*t*w)*np.cos(a*w) + 1j*t**23*w*np.exp(-1j*t*w)*np.sin(a*w) - 23*t**22*np.exp(-1j*t*w)*np.sin(a*w)
		if k==24:
			return a**24*w*np.exp(-1j*t*w)*np.sin(a*w) + 24*1j*a**23*t*w*np.exp(-1j*t*w)*np.cos(a*w) - 24*a**23*np.exp(-1j*t*w)*np.cos(a*w) + 276*a**22*t**2*w*np.exp(-1j*t*w)*np.sin(a*w) + 552*1j*a**22*t*np.exp(-1j*t*w)*np.sin(a*w) + 2024*1j*a**21*t**3*w*np.exp(-1j*t*w)*np.cos(a*w) - 6072*a**21*t**2*np.exp(-1j*t*w)*np.cos(a*w) + 10626*a**20*t**4*w*np.exp(-1j*t*w)*np.sin(a*w) + 42504*1j*a**20*t**3*np.exp(-1j*t*w)*np.sin(a*w) + 42504*1j*a**19*t**5*w*np.exp(-1j*t*w)*np.cos(a*w) - 212520*a**19*t**4*np.exp(-1j*t*w)*np.cos(a*w) + 134596*a**18*t**6*w*np.exp(-1j*t*w)*np.sin(a*w) + 807576*1j*a**18*t**5*np.exp(-1j*t*w)*np.sin(a*w) + 346104*1j*a**17*t**7*w*np.exp(-1j*t*w)*np.cos(a*w) - 2422728*a**17*t**6*np.exp(-1j*t*w)*np.cos(a*w) + 735471*a**16*t**8*w*np.exp(-1j*t*w)*np.sin(a*w) + 5883768*1j*a**16*t**7*np.exp(-1j*t*w)*np.sin(a*w) + 1307504*1j*a**15*t**9*w*np.exp(-1j*t*w)*np.cos(a*w) - 11767536*a**15*t**8*np.exp(-1j*t*w)*np.cos(a*w) + 1961256*a**14*t**10*w*np.exp(-1j*t*w)*np.sin(a*w) + 19612560*1j*a**14*t**9*np.exp(-1j*t*w)*np.sin(a*w) + 2496144*1j*a**13*t**11*w*np.exp(-1j*t*w)*np.cos(a*w) - 27457584*a**13*t**10*np.exp(-1j*t*w)*np.cos(a*w) + 2704156*a**12*t**12*w*np.exp(-1j*t*w)*np.sin(a*w) + 32449872*1j*a**12*t**11*np.exp(-1j*t*w)*np.sin(a*w) + 2496144*1j*a**11*t**13*w*np.exp(-1j*t*w)*np.cos(a*w) - 32449872*a**11*t**12*np.exp(-1j*t*w)*np.cos(a*w) + 1961256*a**10*t**14*w*np.exp(-1j*t*w)*np.sin(a*w) + 27457584*1j*a**10*t**13*np.exp(-1j*t*w)*np.sin(a*w) + 1307504*1j*a**9*t**15*w*np.exp(-1j*t*w)*np.cos(a*w) - 19612560*a**9*t**14*np.exp(-1j*t*w)*np.cos(a*w) + 735471*a**8*t**16*w*np.exp(-1j*t*w)*np.sin(a*w) + 11767536*1j*a**8*t**15*np.exp(-1j*t*w)*np.sin(a*w) + 346104*1j*a**7*t**17*w*np.exp(-1j*t*w)*np.cos(a*w) - 5883768*a**7*t**16*np.exp(-1j*t*w)*np.cos(a*w) + 134596*a**6*t**18*w*np.exp(-1j*t*w)*np.sin(a*w) + 2422728*1j*a**6*t**17*np.exp(-1j*t*w)*np.sin(a*w) + 42504*1j*a**5*t**19*w*np.exp(-1j*t*w)*np.cos(a*w) - 807576*a**5*t**18*np.exp(-1j*t*w)*np.cos(a*w) + 10626*a**4*t**20*w*np.exp(-1j*t*w)*np.sin(a*w) + 212520*1j*a**4*t**19*np.exp(-1j*t*w)*np.sin(a*w) + 2024*1j*a**3*t**21*w*np.exp(-1j*t*w)*np.cos(a*w) - 42504*a**3*t**20*np.exp(-1j*t*w)*np.cos(a*w) + 276*a**2*t**22*w*np.exp(-1j*t*w)*np.sin(a*w) + 6072*1j*a**2*t**21*np.exp(-1j*t*w)*np.sin(a*w) + 24*1j*a*t**23*w*np.exp(-1j*t*w)*np.cos(a*w) - 552*a*t**22*np.exp(-1j*t*w)*np.cos(a*w) + t**24*w*np.exp(-1j*t*w)*np.sin(a*w) + 24*1j*t**23*np.exp(-1j*t*w)*np.sin(a*w)
		if k==25:
			return a**25*w*np.exp(-1j*t*w)*np.cos(a*w) - 25*1j*a**24*t*w*np.exp(-1j*t*w)*np.sin(a*w) + 25*a**24*np.exp(-1j*t*w)*np.sin(a*w) + 300*a**23*t**2*w*np.exp(-1j*t*w)*np.cos(a*w) + 600*1j*a**23*t*np.exp(-1j*t*w)*np.cos(a*w) - 2300*1j*a**22*t**3*w*np.exp(-1j*t*w)*np.sin(a*w) + 6900*a**22*t**2*np.exp(-1j*t*w)*np.sin(a*w) + 12650*a**21*t**4*w*np.exp(-1j*t*w)*np.cos(a*w) + 50600*1j*a**21*t**3*np.exp(-1j*t*w)*np.cos(a*w) - 53130*1j*a**20*t**5*w*np.exp(-1j*t*w)*np.sin(a*w) + 265650*a**20*t**4*np.exp(-1j*t*w)*np.sin(a*w) + 177100*a**19*t**6*w*np.exp(-1j*t*w)*np.cos(a*w) + 1062600*1j*a**19*t**5*np.exp(-1j*t*w)*np.cos(a*w) - 480700*1j*a**18*t**7*w*np.exp(-1j*t*w)*np.sin(a*w) + 3364900*a**18*t**6*np.exp(-1j*t*w)*np.sin(a*w) + 1081575*a**17*t**8*w*np.exp(-1j*t*w)*np.cos(a*w) + 8652600*1j*a**17*t**7*np.exp(-1j*t*w)*np.cos(a*w) - 2042975*1j*a**16*t**9*w*np.exp(-1j*t*w)*np.sin(a*w) + 18386775*a**16*t**8*np.exp(-1j*t*w)*np.sin(a*w) + 3268760*a**15*t**10*w*np.exp(-1j*t*w)*np.cos(a*w) + 32687600*1j*a**15*t**9*np.exp(-1j*t*w)*np.cos(a*w) - 4457400*1j*a**14*t**11*w*np.exp(-1j*t*w)*np.sin(a*w) + 49031400*a**14*t**10*np.exp(-1j*t*w)*np.sin(a*w) + 5200300*a**13*t**12*w*np.exp(-1j*t*w)*np.cos(a*w) + 62403600*1j*a**13*t**11*np.exp(-1j*t*w)*np.cos(a*w) - 5200300*1j*a**12*t**13*w*np.exp(-1j*t*w)*np.sin(a*w) + 67603900*a**12*t**12*np.exp(-1j*t*w)*np.sin(a*w) + 4457400*a**11*t**14*w*np.exp(-1j*t*w)*np.cos(a*w) + 62403600*1j*a**11*t**13*np.exp(-1j*t*w)*np.cos(a*w) - 3268760*1j*a**10*t**15*w*np.exp(-1j*t*w)*np.sin(a*w) + 49031400*a**10*t**14*np.exp(-1j*t*w)*np.sin(a*w) + 2042975*a**9*t**16*w*np.exp(-1j*t*w)*np.cos(a*w) + 32687600*1j*a**9*t**15*np.exp(-1j*t*w)*np.cos(a*w) - 1081575*1j*a**8*t**17*w*np.exp(-1j*t*w)*np.sin(a*w) + 18386775*a**8*t**16*np.exp(-1j*t*w)*np.sin(a*w) + 480700*a**7*t**18*w*np.exp(-1j*t*w)*np.cos(a*w) + 8652600*1j*a**7*t**17*np.exp(-1j*t*w)*np.cos(a*w) - 177100*1j*a**6*t**19*w*np.exp(-1j*t*w)*np.sin(a*w) + 3364900*a**6*t**18*np.exp(-1j*t*w)*np.sin(a*w) + 53130*a**5*t**20*w*np.exp(-1j*t*w)*np.cos(a*w) + 1062600*1j*a**5*t**19*np.exp(-1j*t*w)*np.cos(a*w) - 12650*1j*a**4*t**21*w*np.exp(-1j*t*w)*np.sin(a*w) + 265650*a**4*t**20*np.exp(-1j*t*w)*np.sin(a*w) + 2300*a**3*t**22*w*np.exp(-1j*t*w)*np.cos(a*w) + 50600*1j*a**3*t**21*np.exp(-1j*t*w)*np.cos(a*w) - 300*1j*a**2*t**23*w*np.exp(-1j*t*w)*np.sin(a*w) + 6900*a**2*t**22*np.exp(-1j*t*w)*np.sin(a*w) + 25*a*t**24*w*np.exp(-1j*t*w)*np.cos(a*w) + 600*1j*a*t**23*np.exp(-1j*t*w)*np.cos(a*w) - 1j*t**25*w*np.exp(-1j*t*w)*np.sin(a*w) + 25*t**24*np.exp(-1j*t*w)*np.sin(a*w)
	return func

def w_sin_wa(a):
	def func(w,k=0):
		if k==0:
			return w*np.sin(a*w)
		if k==1:
			return a*w*np.cos(a*w) + np.sin(a*w)
		if k==2:
			return -a**2*w*np.sin(a*w) + 2*a*np.cos(a*w)
		if k==3:
			return -a**3*w*np.cos(a*w) - 3*a**2*np.sin(a*w)
		if k==4:
			return a**4*w*np.sin(a*w) - 4*a**3*np.cos(a*w)
		if k==5:
			return a**5*w*np.cos(a*w) + 5*a**4*np.sin(a*w)
		if k==6:
			return -a**6*w*np.sin(a*w) + 6*a**5*np.cos(a*w)
		if k==7:
			return -a**7*w*np.cos(a*w) - 7*a**6*np.sin(a*w)
		if k==8:
			return a**8*w*np.sin(a*w) - 8*a**7*np.cos(a*w)
		if k==9:
			return a**9*w*np.cos(a*w) + 9*a**8*np.sin(a*w)
		if k==10:
			return -a**10*w*np.sin(a*w) + 10*a**9*np.cos(a*w)
		if k==11:
			return -a**11*w*np.cos(a*w) - 11*a**10*np.sin(a*w)
		if k==12:
			return a**12*w*np.sin(a*w) - 12*a**11*np.cos(a*w)
		if k==13:
			return a**13*w*np.cos(a*w) + 13*a**12*np.sin(a*w)
		if k==14:
			return -a**14*w*np.sin(a*w) + 14*a**13*np.cos(a*w)
		if k==15:
			return -a**15*w*np.cos(a*w) - 15*a**14*np.sin(a*w)
		if k==16:
			return a**16*w*np.sin(a*w) - 16*a**15*np.cos(a*w)
		if k==17:
			return a**17*w*np.cos(a*w) + 17*a**16*np.sin(a*w)
		if k==18:
			return -a**18*w*np.sin(a*w) + 18*a**17*np.cos(a*w)
		if k==19:
			return -a**19*w*np.cos(a*w) - 19*a**18*np.sin(a*w)
		if k==20:
			return a**20*w*np.sin(a*w) - 20*a**19*np.cos(a*w)
		if k==21:
			return a**21*w*np.cos(a*w) + 21*a**20*np.sin(a*w)
		if k==22:
			return -a**22*w*np.sin(a*w) + 22*a**21*np.cos(a*w)
		if k==23:
			return -a**23*w*np.cos(a*w) - 23*a**22*np.sin(a*w)
		if k==24:
			return a**24*w*np.sin(a*w) - 24*a**23*np.cos(a*w)
		if k==25:
			return a**25*w*np.cos(a*w) + 25*a**24*np.sin(a*w)
	return func

def w_cos_wa(a):
	def func(w,k=0):
		if k==0:
			return w*np.cos(a*w)
		if k==1:
			return -a*w*np.sin(a*w) + np.cos(a*w)
		if k==2:
			return -a**2*w*np.cos(a*w) - 2*a*np.sin(a*w)
		if k==3:
			return a**3*w*np.sin(a*w) - 3*a**2*np.cos(a*w)
		if k==4:
			return a**4*w*np.cos(a*w) + 4*a**3*np.sin(a*w)
		if k==5:
			return -a**5*w*np.sin(a*w) + 5*a**4*np.cos(a*w)
		if k==6:
			return -a**6*w*np.cos(a*w) - 6*a**5*np.sin(a*w)
		if k==7:
			return a**7*w*np.sin(a*w) - 7*a**6*np.cos(a*w)
		if k==8:
			return a**8*w*np.cos(a*w) + 8*a**7*np.sin(a*w)
		if k==9:
			return -a**9*w*np.sin(a*w) + 9*a**8*np.cos(a*w)
		if k==10:
			return -a**10*w*np.cos(a*w) - 10*a**9*np.sin(a*w)
		if k==11:
			return a**11*w*np.sin(a*w) - 11*a**10*np.cos(a*w)
		if k==12:
			return a**12*w*np.cos(a*w) + 12*a**11*np.sin(a*w)
		if k==13:
			return -a**13*w*np.sin(a*w) + 13*a**12*np.cos(a*w)
		if k==14:
			return -a**14*w*np.cos(a*w) - 14*a**13*np.sin(a*w)
		if k==15:
			return a**15*w*np.sin(a*w) - 15*a**14*np.cos(a*w)
		if k==16:
			return a**16*w*np.cos(a*w) + 16*a**15*np.sin(a*w)
		if k==17:
			return -a**17*w*np.sin(a*w) + 17*a**16*np.cos(a*w)
		if k==18:
			return -a**18*w*np.cos(a*w) - 18*a**17*np.sin(a*w)
		if k==19:
			return a**19*w*np.sin(a*w) - 19*a**18*np.cos(a*w)
		if k==20:
			return a**20*w*np.cos(a*w) + 20*a**19*np.sin(a*w)
		if k==21:
			return -a**21*w*np.sin(a*w) + 21*a**20*np.cos(a*w)
		if k==22:
			return -a**22*w*np.cos(a*w) - 22*a**21*np.sin(a*w)
		if k==23:
			return a**23*w*np.sin(a*w) - 23*a**22*np.cos(a*w)
		if k==24:
			return a**24*w*np.cos(a*w) + 24*a**23*np.sin(a*w)
		if k==25:
			return -a**25*w*np.sin(a*w) + 25*a**24*np.cos(a*w)
	return func

def sin_wa(a):
	def func(w,k=0):
		if k==0:
			return np.sin(a*w)
		if k==1:
			return a*np.cos(a*w)
		if k==2:
			return -a**2*np.sin(a*w)
		if k==3:
			return -a**3*np.cos(a*w)
		if k==4:
			return a**4*np.sin(a*w)
		if k==5:
			return a**5*np.cos(a*w)
		if k==6:
			return -a**6*np.sin(a*w)
		if k==7:
			return -a**7*np.cos(a*w)
		if k==8:
			return a**8*np.sin(a*w)
		if k==9:
			return a**9*np.cos(a*w)
		if k==10:
			return -a**10*np.sin(a*w)
		if k==11:
			return -a**11*np.cos(a*w)
		if k==12:
			return a**12*np.sin(a*w)
		if k==13:
			return a**13*np.cos(a*w)
		if k==14:
			return -a**14*np.sin(a*w)
		if k==15:
			return -a**15*np.cos(a*w)
		if k==16:
			return a**16*np.sin(a*w)
		if k==17:
			return a**17*np.cos(a*w)
		if k==18:
			return -a**18*np.sin(a*w)
		if k==19:
			return -a**19*np.cos(a*w)
		if k==20:
			return a**20*np.sin(a*w)
		if k==21:
			return a**21*np.cos(a*w)
		if k==22:
			return -a**22*np.sin(a*w)
		if k==23:
			return -a**23*np.cos(a*w)
		if k==24:
			return a**24*np.sin(a*w)
		if k==25:
			return a**25*np.cos(a*w)
	return func

def cos_wa(a):
	def func(w,k=0):
		if k==0:
			return np.cos(a*w)
		if k==1:
			return -a*np.sin(a*w)
		if k==2:
			return -a**2*np.cos(a*w)
		if k==3:
			return a**3*np.sin(a*w)
		if k==4:
			return a**4*np.cos(a*w)
		if k==5:
			return -a**5*np.sin(a*w)
		if k==6:
			return -a**6*np.cos(a*w)
		if k==7:
			return a**7*np.sin(a*w)
		if k==8:
			return a**8*np.cos(a*w)
		if k==9:
			return -a**9*np.sin(a*w)
		if k==10:
			return -a**10*np.cos(a*w)
		if k==11:
			return a**11*np.sin(a*w)
		if k==12:
			return a**12*np.cos(a*w)
		if k==13:
			return -a**13*np.sin(a*w)
		if k==14:
			return -a**14*np.cos(a*w)
		if k==15:
			return a**15*np.sin(a*w)
		if k==16:
			return a**16*np.cos(a*w)
		if k==17:
			return -a**17*np.sin(a*w)
		if k==18:
			return -a**18*np.cos(a*w)
		if k==19:
			return a**19*np.sin(a*w)
		if k==20:
			return a**20*np.cos(a*w)
		if k==21:
			return -a**21*np.sin(a*w)
		if k==22:
			return -a**22*np.cos(a*w)
		if k==23:
			return a**23*np.sin(a*w)
		if k==24:
			return a**24*np.cos(a*w)
		if k==25:
			return -a**25*np.sin(a*w)
	return func

