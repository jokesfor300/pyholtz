# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
@authors: Georg Mensah, Philip Buschmann, Jonas Moeck, Alessandro Orchini TU Berlin 2015

Finds the eigenvalues (omega) and eigenvectors v of an eigenvalue problem nonlinear in the eigenvalue
L(omega) v = 0
This is solved by using a newton-type algorithm on a linear eigenvalue problem of the form
L(omega) v = lambda v 
where omega is treated as a parameter. The solutions are found when lambda = 0.   
"""

import math as math
import numpy as np
import scipy.sparse.linalg as sspla


def iteration(L, omega_0, maxiter=10,
              tol=1e-16, tol_lamb=1e-8, tol_herm=1e-15,
              relax=1, n_eig_val=3, v0=None, output=True, purge=False):
    """ Utilizes Newton's method for the solution of
    non-linear eigenvalue problems.

    The eigenvalue problem considered reads as follows:
    ``L(k)*v =0``

    Parameters
    ----------

    L : function, callable object
        operator family

    omega : float or complex
        initial guess for the eigenvalue

    maxiter : int, optional
        maximum number of iterations (default is ``10``)

    tol : float
        tolerance. Convergence is assumed  when ``abs(k_n-k_{n-1})<tol``

    relax : float, optional
        relaxation parameter between ``0`` and ``1``. (This feature is
        experimental, the default is ``1``, which implies no relaxation )

    v0: array_like, optional
        initial guess for the eigenvector (if not specified the vector is
        initialized usings ones only)

    Returns
    -------

    k : matrix
        eigenvalue

    v : matrix
        eigenvector

    v_adj : matrix
        adjoint eigenvector

    Notes
    -----
        In case of mode degeneracy the vectors returned feature multiple columns.

    """
    if output:
        print("Launching Newton...")

    domega = float('inf')
    iteration = 0
    abs_lamb = np.zeros(n_eig_val + 1) + float('inf')

    if v0 is None:
        v0 = np.ones(L(omega_0).shape[0])
    v0_adj = v0.conj()
    v = None
    v_adj = None

    while (domega > tol or abs_lamb[n_eig_val - 1] > tol_lamb)\
            and iteration < maxiter:
        if output:
            print("Iteration {}: residue={}, |lambda|={}, eigval={}"
                  .format(iteration + 1, domega, abs_lamb, omega_0))
        # Update matrix
        A = L(omega_0)
        # Check hermiticity to the desired tolerance
        hermitian = True
        try:
            hermitian = np.abs((A.H - A).data).max() < tol_herm
        except ValueError:  # if data is empty i.e. matrix is exactly hermitian
            pass
        # Choose appropriate eigenvalue routine
        if hermitian:
            eigs_routine = sspla.eigsh
        else:
            eigs_routine = sspla.eigs
        # Calculate n+1 smallest eigenvalues and sort them by distance
        # Note: which='LM' mode must still be used here as we set sigma=0
        # refer to https://docs.scipy.org/doc/scipy/reference/tutorial/arpack.html
        lamb, v = eigs_routine(A=A, k=n_eig_val + 1, sigma=0, v0=v0, which='LM')
        index = np.argsort(abs(lamb))
        lamb = lamb[index]
        v = v[:, index]
        abs_lamb = abs(lamb)

        if not hermitian:
            # Calculate adjoint eigenvalues
            lamb_adj, v_adj = sspla.eigs(A=A.H, k=n_eig_val + 1, sigma=0, v0=v0_adj, which='LM')
            index_adj = np.argsort(abs(lamb_adj))
            lamb_adj = lamb_adj[index_adj]
            v_adj = v_adj[:, index_adj]
        else:
            lamb_adj, v_adj = lamb, v

        # Calculate adjoint-based corrections for all n+1 eigenvalues
        omegas_corr = np.zeros(n_eig_val + 1, dtype=np.complex_)
        for i in range(n_eig_val + 1):
            norm = v_adj[:, i].conj().dot(v[:, i])
            deriv = v_adj[:, i].conj().dot(L(omega_0, m=1).dot(v[:, i])) / norm
            omegas_corr[i] = relax * (omega_0 - lamb[i] / deriv) + (1 - relax) * omega_0

        # Choose the closest corrected eigenvalue to perform the next step
        iclosest = np.argsort(np.abs(omegas_corr - omega_0))
        domega = np.abs(omegas_corr - omega_0)[iclosest[0]]
        omega_0 = omegas_corr[iclosest[0]].item(0)
        v0 = v[:, iclosest[0]]
        v0_adj = v_adj[:, iclosest[0]]

        iteration += 1

    nconverged_lamb = sum(abs_lamb < tol_lamb)
    if n_eig_val > nconverged_lamb:
        print("WARNING: User requested {0} eigenvalues"
              " but only {1} converged within tolerance.".format(n_eig_val, nconverged_lamb))
        print("Solver returns only those that converged."
              " Check number of eigenvalues requested and/or tolerance.")
    elif n_eig_val < nconverged_lamb:
        print("WARNING: User requested {0} eigenvalues"
              " but {1} converged within tolerance.".format(n_eig_val, nconverged_lamb))
        print("Solver returns the number requested by the user."
              " Check number of eigenvalues requested and/or tolerance.")

    if iteration >= maxiter:
        print('WARNING:'
              ' Newton solver exited because the maximum number of iterations was reached.'
              ' Convergence is not guaranteed. Check step size and residual.\n')

    if output:
        print('################')
        print(' Newton results ')
        print('################')
        print('Number of steps:', iteration)
        print('Last step parameter variation:', domega)
        print('Auxiliary eigenvalue lambda residual (rhs):', abs_lamb)

    nconverged = min(n_eig_val, nconverged_lamb)
    if output:
        print('Eigenvalue: {} with (geometric) multiplicity {}'.format(omega_0, nconverged))

    v = v[:, iclosest[:nconverged]]
    v_adj = v_adj[:, iclosest[:nconverged]]

    return omega_0, v, v_adj, iteration
