# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Fri Dec  2 14:02:39 2016

@author: georg
"""

# -*- coding: utf-8 -*-
# Georg Mensah, Philip Buschmann, Jonas Moeck, TU Berlin 2015
"""
Created on Fri Dec  4 16:21:54 2015

@author: georg
"""
import math as math
import numpy as np
import scipy.sparse as ssp
import scipy.sparse.linalg as sspla


def iteration(L, omega_0, maxiter=10,
              tol=0, tol_herm=0,
              relax=1, n_eig_val=3, v0=None, output=True):
    """ Utilizes Halley's method for the solution of
    non-linear eigenvalue problems.

    The eigenvalue problem considered reads as follows:
    `L(k)*v =0`

    Parameters
    ----------

    L : function, callable object
        operator family

    omega : float or complex
        initial guess for the eigenvalue

    maxiter : int, optional
        maximum number of iterations (default is 10)

    tol : float
        tolerance. Convergnece is assumend  when `abs(omega_n-k_{n-1})<tol`

    relax : float, optional
        relaxation parameter between 0 and 1. (This feature is
        experimental, the default is 1, which implies no relaxation )

    v0 : array_like, optional
         initial guess for the eigenvector (if not specified the vector is
         initialized usings ones only)

    Returns
    -------

    k : matrix
        eigenvalue

    v : matrix
        eigenvector

    v_adj : matrix
        adjoint eigenvector

    Notes
    -----
        In case of mode degenracy the vectors returned feature multiple
        columns.

    """
    if output:
        print("Launching Halley...")

    domega = float('inf')
    iteration = 0
    abs_lamb = np.zeros(n_eig_val + 1) + float('inf')

    if v0 is None:
        v0 = np.ones(L(omega_0).shape[0])
    v0_adj = v0.conj()
    I = ssp.eye(v0.shape[0])

    while domega > tol and iteration < maxiter:
        if output:
            print("Iteration {}: residue={}, eigval={}"
                  .format(iteration + 1, domega, omega_0))
        # Update matrix
        A = L(omega_0)
        # Check hermiticity to the desired tolerance
        hermitian = True
        try:
            hermitian = np.abs((A.H - A).data).max() < tol_herm
        except ValueError:  # if data is empty i.e. matrix is exactly hermitian
            pass
        # Choose appropriate eigenvalue routine
        if hermitian:
            eigs_routine = sspla.eigsh
        else:
            eigs_routine = sspla.eigs
        # Calculate n+1 smallest eigenvalues and sort them by distance
        # Note: which='LM' mode must still be used here as we set sigma=0
        # refer to https://docs.scipy.org/doc/scipy/reference/tutorial/arpack.html
        lamb, v = eigs_routine(A=A, k=n_eig_val, sigma=0, v0=v0, which='LM')
        index = np.argsort(abs(lamb))
        lamb = lamb[index]
        v = v[:, index]
        abs_lamb = abs(lamb)

        if not hermitian:
            # Calculate adjoint eigenvalues
            lamb_adj, v_adj = sspla.eigs(A=A.H, k=n_eig_val, sigma=0, v0=v0_adj, which='LM')
            index_adj = np.argsort(abs(lamb_adj))
            v_adj = v_adj[:, index_adj]
        else:
            lamb_adj, v_adj = lamb, v

        omegas_corr = np.zeros(n_eig_val, dtype=np.complex_)
        for i in range(n_eig_val):
            # compute derivative
            # derivative is taken at omega_0!
            rhs = L(omega_0, m=1) * v[:, i]
            norm = v_adj[:, i].conj().dot(v[:, i])
            deriv = v_adj[:, i].conj().dot(rhs) / norm
            deriv = deriv.item(0)
            mode_deriv = sspla.spsolve(A - lamb[i] * I, -rhs + deriv * v[:, i])
            rhs = L(omega_0, m=1).dot(mode_deriv) + L(omega_0, m=2).dot(v[:, i]) - deriv * mode_deriv
            second_deriv = 2 * (v_adj[:, i].conj().dot(rhs)) / norm
            omegas_corr[i] = relax * (omega_0 - 2 * lamb[i] * deriv / (2 * deriv ** 2 - lamb[i] * second_deriv)) \
                             + (1 - relax) * omega_0
        # Choose the closest corrected eigenvalue to perfrom the next step
        iclosest = np.argmin(np.abs(omegas_corr - omega_0))
        domega = np.abs(omegas_corr - omega_0)[iclosest]
        omega_0 = omegas_corr[iclosest].item(0)
        v0 = v[:, iclosest]
        v0_adj = v_adj[:, iclosest]

        iteration += 1

    # print(delta_k,tol,delta_k[index[1]])
    # TODO degnerate
    # if delta_k[index[1]]<=(tol*10):
    #    #print('juhu',v0.shape)
    #    v0=v[:,index[0:2]]
    # print('juhu',v0.shape)

    print('################')
    print(' Halley results ')
    print('################')
    print('Number of steps:', iteration)
    print('Last step parameter variation:', domega)
    print('Auxiliary eigenvalue (lambda) residual (rhs):', abs_lamb)
    # nconverged = min(n_eig_val, nconverged_lamb)
    # print('Eigenvalue: {} with (geometric) multiplicity {}'.format(omega_0, nconverged))
    return omega_0, v0, v0_adj, iteration
