# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Last modified on May 02 14:47:53 2018
@author: orchini
This routine iteratively perfroms a degenerate perturbation expansion on a provided baseline (degenerate) solution.
We refer to Orchini, Mensah, Moeck, JCP 2018 (submitted) for a detailed discussion of the algorithm.
Inputs:
    OpFam: The operator family of which we want to find eigenvalues and eigenvectors
    Lambda0: the degenerate eigenvalue
    eps0: the value of the paramter for which the baseline solution was obtained
    v0: a base for the direct, degenerate eigenvector subspace
    v0_adj: a base for the adjoint, degenerate eigenvector subspace
    N: order to which the perturbation expansion will be performed
    issparse=True: flag to toggle if the OpFam operator is sparse
    rtol=100: tolerance value in descerning degenerate eigenvalues
    debug=False: debug Flag. If true, a file called "Debug_Output.txt" is created, which contains some info on the operations performed by the algorithm.
    normalisation=False: flag to toggle normalisation condition operations on the eigenvectors. The normalisation can always be performed as a post-processing step. 

"""

import scipy.linalg as linalg
import numpy as np
from scipy.sparse.linalg import spsolve
from scipy.linalg import cholesky
from PyHoltz.nlevp.evaluate_r import calculate_r, calculate_Mv, calculate_r_fast
import time, datetime


# Orthonormalize degenerate basis using Cholesky decomposition
def orthonormalize(a):
    norm = np.matrix(cholesky(a.H * a))
    a = a * norm.I
    return a


# Bi-orthonormalize direct and adjoint eigenvectors wrt identity matrix
def biorthonormalize(a, b):
    norm = b.H * a
    b = b * (norm.I).H
    return b


# Returns the order at which two branches split in a tree-structure
def split_order(branch, branch2):
    if branch == branch2:
        return np.nan
    else:
        order = 0
        b = True
        while b:
            if branch[order] != branch2[order]:
                b = False
            order += 1
        return order


# Identifies degenerate subspaces in auxilary eigenvalue problems and bi-orthonormalize them
def eigval_split(Lambda, z, z_adj, atol):
    # First: identify degenerate subspaces
    sort = [[(Lambda.pop(0), z.pop(0), z_adj.pop(0))]]
    while Lambda:
        for idx, lamb in enumerate(Lambda):
            if np.abs(sort[-1][0][0] - lamb) < atol:
                sort[-1].append((Lambda.pop(idx), z.pop(idx), z_adj.pop(idx)))
                break
        else:
            sort.append([(Lambda.pop(0), z.pop(0), z_adj.pop(0),)])

    # Second: reorganize the structure for output.
    # TODO: Best would be to create in right format at structure creation
    Spaces = []
    for spc in sort:
        Lambda = []
        z = []
        z_adj = []
        for tpl in spc:
            Lambda.append(tpl[0])
            z.append(tpl[1])
            z_adj.append(tpl[2])
        Spaces.append([np.matrix(np.diag(Lambda)), np.matrix(z).T, np.matrix(z_adj).T])

    # Third: bi-orthonormalize each subspace    
    for spc in Spaces:
        spc[1] = orthonormalize(spc[1])
        spc[2] = biorthonormalize(spc[1], spc[2])

    return Spaces


def evaluate_norm_fact(k, branch, v):
    norm = v[0][branch].H * v[0][branch] * 0
    for n in range(1, k):
        norm -= 0.5 * np.diag(np.diag(v[k - n][branch].H * v[n][branch]))
    return norm


# Evaluate inter-branches coefficients. Necessary to impose Fredholm conditions
def evaluate_cs(order, splits, branch, v, v_adj, Lambda, L, fh, debug=False):
    A = np.array([])
    B = np.array([])
    build_B = True

    # Loop through branches
    for alpha1, split_orders in splits[branch].items():
        Atemp = np.array([])
        Mv = calculate_Mv(L, split_orders, branch, Lambda, v, 0, alpha1)
        # Loop through elements of each branch
        for alpha, useless in splits[branch].items():
            # Build coefficients matrix by stacking scalar produts
            a_abc = v_adj[0][alpha].H * Mv
            Atemp = np.vstack((Atemp, a_abc)) if Atemp.size else a_abc
            # Build rhs (does not depend on alpha1, so can be evaluated only once.)
            if build_B:
                b_ab = v_adj[0][alpha].H * calculate_r_fast(L, order, v, branch, Lambda)
                B = np.vstack((B, b_ab)) if B.size else b_ab
        A = np.hstack((A, Atemp)) if A.size else Atemp
        build_B = False

    # Solve linear system Ac = B for c
    cs = np.matrix(np.linalg.solve(A, B))
    if debug:
        print_to_file(fh, 'Gammas branch', branch)
        print_to_file(fh, 'A', A)
        print_to_file(fh, 'B', B)
        print_to_file(fh, 'Cs', cs)
        fh.write('\n'.encode('ascii'))

    return cs


# Print data to file if debugging is on
def print_to_file(fh, name, matr):
    temp = np.array(matr)
    fh.write((name + ':\n').encode('ascii'))
    np.savetxt(fh, temp, fmt='%.4e')


def perturb_degenerate(OpFam, lmbd0, eps0, v0, v0_adj, N,
        issparse=True, rtol=1e-2, debug=False, normalization=False, verbose=1):

    v0 = np.matrix(v0)
    v0_adj = np.matrix(v0_adj)

    if v0.shape[1] > 2 and normalization:
        print(
            'Warning:'
            ' normalisation conditions for eigenvalues with multiplicity larger than 2 are not implemented yet.'
            ' Switching the normalisation flag to false.')
        normalization = False

    if debug:
        fh = open("Debug_Output.txt", "w")
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        text = ["Debug file", "generated on " + st + '\n\n']
        fh.writelines(text)
        fh.close()
        fh = open("Debug_Output.txt", "ab")
    else:
        fh = []

        # Re-define operator L so that only 2 inputs are required rather than 4.
    def L(m, n):
        return OpFam(lmbd0, eps0, m, n)

    # Normalize basis vectors
    v0 /= np.linalg.norm(v0, 2, axis=0)
    # Bi-orthonormalize adjoint vectors wrt to dL/d\lambda.
    norm = v0_adj.H * L(1, 0) * v0
    v0_adj = v0_adj * (norm.I).H

    # initilaize tree structure
    Lambda = {}
    Lambda[()] = lmbd0

    v = [{} for _ in range(N + 1)]
    v[0][()] = v0

    v_adj = [{}]
    v_adj[0][()] = v0_adj

    # Solve perturbation orders in ascending order
    for order in range(1, N + 1):
        if verbose > 0:
            print('Solving order {}'.format(order))
        if debug:
            fh.write(('\nSolving order' + str(order) + '\n').encode('ascii'))

        ############################# PART 1 #############################
        # Define set of branches at previous order
        old_branches = [i for i in Lambda.keys() if len(i) == order - 1]

        # Find c coefficients (inter-branches corrections)
        splits = {}
        cs = {}
        for branch in old_branches:
            if v0.shape[1] - v[order - 1][branch].shape[1] == 0:
                continue
            splits[branch] = {}
            for branch2 in old_branches:
                # Determine at which orders the splitting occur
                if branch2 != branch:  # The coefficients branch-branch are determined by normalization
                    splits[branch][branch2] = split_order(branch, branch2)
            cs[branch] = evaluate_cs(order, splits, branch, v, v_adj, Lambda, L, fh, debug=debug)

        # Correct branches with c coefficients        
        for branch, useless in splits.items():
            count = 0
            for branch2, split_orders in splits[branch].items():
                Nbranch2 = v[0][branch2].shape[1]
                v[order - split_orders][branch] += v[0][branch2] @ cs[branch][count:count + Nbranch2, :]
                if normalization:
                    cs_branch = -(v[0][branch].H * v[0][branch2]) * cs[branch][count:count + Nbranch2, :]
                    v[order - split_orders][branch] += v[0][branch] * cs_branch
                count += Nbranch2

        ############################# PART 2 #############################
        # Formulate auxiliary eigenvalue problem
        for branch in old_branches:

            # The threshold 8 is determined empirically.
            r = calculate_r_fast(L, order, v, branch, Lambda)

            # Construct auxiliary eigenvalue problem matrices. Y should be the identity.
            X = v_adj[0][branch].H * r
            Y = v_adj[0][branch].H * L(1, 0) * v[0][branch]

            if debug:
                fh.write('\n'.encode('ascii'))
                print_to_file(fh, 'Auxiliary Eigenproblem branch', branch)
                print_to_file(fh, 'X' + str(order), X)

            # Solve auxiliary eigenvalue problem
            Lambda_new, z_adj, z = linalg.eig(-X, Y, left=True)
            z = np.matrix(z)
            z_adj = np.matrix(z_adj)

            if debug:
                fh.write('\n'.encode('ascii'))
                print_to_file(fh, 'Auxiliary Eigenproblem Eigenvalues', Lambda_new)
                fh.write('\n'.encode('ascii'))

            # Determine in branch splitting has occurred.
            # TODO: There is an un-necessary manipulation matrix-lists-matrix. Can be cleaned up             
            atol = max(np.min(np.abs(Lambda_new)) * rtol, 1e-20)
            sorted_space = eigval_split(Lambda_new.tolist(), z.T.tolist(), z_adj.T.tolist(), atol)

            # Create new level branches in the tree and assign them new eigenvalues and adjoint-eigenvectors
            for i, space in enumerate(sorted_space):
                Lambda[branch + (i,)] = space[0].item(0)
                v_adj[0][branch + (i,)] = v_adj[0][branch] * space[2]

                # Correct lower order eigenvectors in the tree so that they span the correct subspace,
                # in case eigenvalue splitting occurred
                for n in range(order):
                    v[n][branch + (i,)] = v[n][branch] * space[1]

        # Clean up the tree stucture at lower orders.
        for branch in old_branches:
            v_adj[0].pop(branch, None)
            for i in range(order):
                v[i].pop(branch, None)

        # Define set of branches at this order
        branches = [i for i in Lambda.keys() if len(i) == order]

        ############################# PART 3 #############################
        # Calculate new eigenvectors          
        for branch in branches:
            # Calculate rhs
            r = calculate_r_fast(L, order, v, branch, Lambda)
            rhs = -r - L(1, 0) @ v[0][branch] * Lambda[branch]

            # Calcualte new eigenvectors of each branch and assign them to their node.
            # Choose solver depending on type of problem. Least square and sparse are implemented.
            if not issparse:
                sol = np.linalg.lstsq(L(0, 0), rhs)
                v[order][branch] = sol[0]
            else:
                v[order][branch] = np.matrix(spsolve(L(0, 0), rhs))
                if v[0][branch].shape[1] == 1:
                    v[order][branch] = v[order][branch].T

            v[order][branch] -= v0 * (v0.H * v[order][branch])
            #
            if debug:
                print_to_file(fh, 'v' + str(order) + 'solution Norm',
                              [np.linalg.norm(L(0, 0) * v[order][branch] - rhs)])
                fh.write('\n'.encode('ascii'))

        ############################# PART 4 #############################
        # Normalise eigenvectors. Only the 2 branch case is implemented.
        for branch in branches:
            if normalization:
                norm_fact = evaluate_norm_fact(order, branch, v)
                v[order][branch] += v[0][branch] * norm_fact

                if debug:
                    print_to_file(fh, 'branch', branch)
                    fh.write('\n'.encode('ascii'))
                    print_to_file(fh, 'Normalization factors', norm_fact)
                    fh.write('\n'.encode('ascii'))

    if debug:
        fh.close()

    return Lambda, v, v_adj
