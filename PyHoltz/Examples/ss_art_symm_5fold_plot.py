#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Plot some results from test_suite_3
"""
#import os
#os.chdir('../')

import pickle
import numpy as np
from matplotlib import pyplot as plt
from copy import deepcopy
from math import atan2
#%%
def poly(eps,eps0,order,branch,lamps):
    lam = 0.
    for k in range(order+1):
        lam += (eps - eps0)**k*lamps[branch][k]
    return lam

def polyV(eps,eps0,order,branch,vecps):
    p_pert = deepcopy(vecps[branch][0])*0.
    for k in range(order+1):
        p_pert += (eps - eps0)**k*vecps[branch][k]
    return p_pert

#%%
Data = pickle.load( open( "PyHoltz/Examples/ss_art_symm_5fold_data.pickle", "rb" ) )
branches = Data[0]
lamps = Data[1] # Perturpation LAMbddaS Coefficients
vecps = Data[2] # Perturbation VECtorS Coefficients
eps = Data[3]
lames = Data[4] # Exact LAMbdaS
veces = Data[5] # Exact VECtorS

#%%
order = len(lamps[branches[0]]) - 1
omega0 = deepcopy(lamps[branches[0]][0])    
eps0 = 0

#%%
br_cls = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
cm2in = 0.393701
plt.close('all')
plt.figure(num='1',figsize=(24*cm2in,10*cm2in))
plt.clf()
plt.subplot(1,2,1)
plt.grid(True)

hand = []

inds = [0,1,3,4,2]

plt.figure('1')
for i in range(5):
    plt.plot(eps,np.real(lames[branches[i]]),'k-',linewidth=2)

names = [r'$e^\epsilon -1$',r'$\log(\epsilon + 1)$',r'$\epsilon$',r'$\sin(\epsilon)$',r'$\arctan(\epsilon)$']
for nB, branch in enumerate(branches):
    branch = branches[inds[nB]]
    for n, ep in enumerate(eps):
        if (np.mod(n,3)==1):
            w_pert = poly(ep,eps0,order,branch,lamps)                
            p_pert = polyV(ep,eps0,order,branch,vecps)    
            proj = vecps[branch][0].H*veces[branch][n]
            veces[branch][n] *= np.exp(-1j*atan2(np.imag(proj),np.real(proj))) 
            
            
            if n == 1:
                hand += plt.plot(ep,np.real(w_pert),'o',color=br_cls[nB],markersize=3,label=names[nB])
            else:
                plt.plot(ep,np.real(w_pert),'o',color=br_cls[nB],markersize=3)
                
    
plt.xlabel(r'$\epsilon$',fontsize=14)
plt.ylabel(r'$\lambda_{\mathbf{\beta}}$',fontsize=14)
plt.tick_params(labelsize=10)
plt.legend(handles=hand)
plt.tight_layout()

#%%
def hex2rgb(hex_val):
    hex_val = hex_val.strip('#')
    rgb_val = tuple(int(hex_val[i:i+2], 16) for i in (0, 2 ,4))
    return rgb_val
  
plt.subplot(1,2,2)
nB = 0
branch = branches[inds[nB]]
mycol = hex2rgb(br_cls[nB])
hand = []
vmax = 1
vmin = 0.3
for n in [51,63,75,88,100]:
    fact = ((100-n)*(vmax-vmin)/(100-50)+vmin)/255.
    col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
    ep = eps[n]
    err = []
    name = r'$\epsilon = '+str("%.3f" % ep)+'$'
    for orde in range(0,order+1):
        w_pert = poly(ep,eps0,orde,branch,lamps)     
        err+=[np.abs(w_pert-lames[branch][n])]
    hand += plt.semilogy(np.linspace(0,order,order+1),err,'-o',color=col,label=name,markersize=5)

plt.grid(True)
plt.xlabel('Perturbation order',fontsize=12)
plt.ylabel(r'$|\lambda_{exact}-\lambda_{\mathbf{\beta}}|$',fontsize=14)
plt.tick_params(labelsize=10)
plt.legend(handles=hand)
plt.tight_layout()
plt.xlim((0,order))
plt.ylim((1e-15,10))
plt.xticks(np.linspace(0,order,order+1))
#plt.savefig('test_suite_3.eps', dpi=300)
