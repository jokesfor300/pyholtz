#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Plot some results from the test_suite_1
"""
#import os
#os.chdir('../')

import pickle
import numpy as np
from scipy.interpolate import spline
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick
from copy import deepcopy
from math import atan2

#%%
def poly(eps,eps0,order,branch,lamps):
    lam = 0.
    for k in range(order+1):
        lam += (eps - eps0)**k*lamps[branch][k]
    return lam

def polyV(eps,eps0,order,branch,vecps):
    p_pert = deepcopy(vecps[branch][0])*0.
    for k in range(order+1):
        p_pert += (eps - eps0)**k*vecps[branch][k]
    return p_pert

def hex2rgb(hex_val):
    hex_val = hex_val.strip('#')
    rgb_val = tuple(int(hex_val[i:i+2], 16) for i in (0, 2 ,4))
    return rgb_val

#%% Load data. Because we make some inset plots, we require 3 set of data.
Data = pickle.load( open( "PyHoltz/Examples/Degenerate_Membrane_ins10.pickle", "rb" ) )
branches = Data[0]
lamps = Data[1] # Perturpation LAMbddaS Coefficients
vecps = Data[2] # Perturbation VECtorS Coefficients
eps = Data[3]
lames = Data[4] # Exact LAMbdaS
veces = Data[5] # Exact VECtorS
#
DataI = pickle.load( open( "PyHoltz/Examples/Degenerate_Membrane_ins1.pickle", "rb" ) )
branchesI = DataI[0]
lampsI = DataI[1] # Perturpation LAMbddaS Coefficients
vecpsI = DataI[2] # Perturbation VECtorS Coefficients
epsI = DataI[3]
lamesI = DataI[4] # Exact LAMbdaS
vecesI = DataI[5] # Exact VECtorS
#
DataV = pickle.load( open("PyHoltz/Examples/Degenerate_Membrane_viring.pickle", "rb" ) )
w1 = DataV[0]
w2 = DataV[1]
epsV = DataV[2]

#%%
order = len(lamps[branches[0]]) - 1
omega0 = deepcopy(lamps[branches[0]][0])    
eps0 = 0
order=10

#%% Plot
br_cls = ['#ff7f0e', '#1f77b4', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
cm2in = 0.393701
vmax = 1
vmin = 0.5
plt.close('all')
fig = plt.figure(num='1',figsize=(20*cm2in,10*cm2in))
plt.clf()

hand = []

inds = [0,1,2]
ax1 = plt.axes([0.1,0.12,0.45,0.83])
plt.grid(True)

for i in range(3):
    plt.plot(eps,np.real(lames[branches[i]])-lamps[branches[0]][0],'k-',linewidth=2)

names = [r'$\lambda_{\mathbf{\beta}_a}$',r'$\lambda_{\mathbf{\beta}_{s_1}}$',r'$\lambda_{\mathbf{\beta}_{s_2}}$']
for nB, branch in enumerate(branches):
    branch = branches[inds[nB]]
    for n, ep in enumerate(eps):
        if (np.mod(n,3)==1):
            w_pert = poly(ep,eps0,order,branch,lamps)                
            p_pert = polyV(ep,eps0,order,branch,vecps)
            
            p_pert /= np.linalg.norm(p_pert)
            proj = vecps[branch][0].H*p_pert
            p_pert *= np.exp(-1j*atan2(np.imag(proj),np.real(proj))) 
    
            proj = vecps[branch][0].H*veces[branch][n]
            veces[branch][n] *= np.exp(-1j*atan2(np.imag(proj),np.real(proj))) 
            
            fact = (-np.abs(50-n)*(vmax-vmin)/(100-50)+vmax)/255.
            mycol = hex2rgb(br_cls[nB])
            col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
            
            if n == 52:
                hand += plt.plot(ep,np.real(w_pert-lamps[branches[0]][0]),'o',color=col,markersize=5,label=names[nB])
            else:
                plt.plot(ep,np.real(w_pert-lamps[branches[0]][0]),'o',color=col,markersize=5)

plt.xlim([eps[0], eps[-1]])
plt.xlabel(r'$\epsilon$',fontsize=14)
plt.ylabel(r'$\lambda_{\mathbf{\beta}} - \lambda_0 $',fontsize=14)
plt.tick_params(labelsize=10)
plt.legend(handles=hand,bbox_to_anchor=(0.8, 0.98),handletextpad=0,ncol=3)

col = br_cls[3]
plt.plot([-1,0.5],[-20,-20],'-',linewidth=0.75,color=col)
plt.plot([-1,0.5],[20,20],'-',linewidth=0.75,color=col)
plt.plot([-1,-1],[-20,20],'-',linewidth=0.75,color=col)
plt.plot([0.5,0.5],[-20,20],'-',linewidth=0.75,color=col)
#%%
ax2 = plt.axes([0.625,0.6,0.35,0.35])
ax2.cla()
plt.grid(True)

for i in range(3):
    plt.plot(epsI,np.real(lamesI[branches[i]])-lampsI[branches[0]][0],'k-',linewidth=2)

for nB, branch in enumerate(branchesI):
    branch = branchesI[inds[nB]]
    for n, ep in enumerate(epsI):
        if (np.mod(n,3)==1):
            w_pert = poly(ep,eps0,order,branch,lampsI)                
            p_pert = polyV(ep,eps0,order,branch,vecpsI)
            
            p_pert /= np.linalg.norm(p_pert)
            proj = vecps[branch][0].H*p_pert
            p_pert *= np.exp(-1j*atan2(np.imag(proj),np.real(proj))) 
    
            proj = vecps[branch][0].H*veces[branch][n]
            veces[branch][n] *= np.exp(-1j*atan2(np.imag(proj),np.real(proj))) 
            
            fact = (-np.abs(0-ep)*(vmax-vmin)/(max(eps)-0)+vmax)/255.
            mycol = hex2rgb(br_cls[nB])
            col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
    
            plt.plot(ep,np.real(w_pert-lamps[branches[0]][0]),'o',color=col,markersize=4)

col = br_cls[3]
plt.xlim([-1, 0.5])
plt.ylim([-4, 4])
plt.xticks([-1,-0.5,0,0.5],color=col)
plt.yticks([-4,-2,0,2,4],[-4,' ',0,' ',4],color=col)
#plt.xlabel(r'$\epsilon$',fontsize=14)
#plt.ylabel(r'$\lambda_{\mathbf{\beta}} - \lambda_0 $',fontsize=14)
#ax2.yaxis.set_label_coords(-0.075, 0.4)
plt.tick_params(labelsize=10)
plt.setp(tuple(ax2.spines.values()), color=col)
plt.setp([ax2.get_xticklines(), ax2.get_yticklines()], color=col)

col = br_cls[9]
col = np.array(hex2rgb(col))*0.7/255.
xmin = -0.83
xmax = -0.73
ymin = -0.6
ymax = 0.6
plt.plot([xmin, xmax],[ymin, ymin],'-',linewidth=0.75,color=col)
plt.plot([xmin, xmax],[ymax, ymax],'-',linewidth=0.75,color=col)
plt.plot([xmin, xmin],[ymin, ymax],'-',linewidth=0.75,color=col)
plt.plot([xmax, xmax],[ymin, ymax],'-',linewidth=0.75,color=col)


#%%
ax3 = plt.axes([0.65,0.1,0.3,0.35])
ax3.cla()
plt.grid(True)
epsNew = np.linspace(min(epsV),max(epsV),15)
w1_smooth = spline(epsV,w1,epsNew)
w2_smooth = spline(epsV,w2,epsNew)

plt.plot(epsNew,w1_smooth,'k-',linewidth=1)
plt.plot(epsNew,w2_smooth,'k-',linewidth=1)

branch = branches[0]
for n, ep in enumerate(epsNew):
    if (np.mod(n,2)==1):
        if ep > (epsNew[0]+epsNew[-1])/2.:
            nB1 = 2
            nB2 = 0
        else:
            nB1 = 0
            nB2 = 2 
        fact = (-np.abs(0-ep)*(vmax-vmin)/(max(eps)-0)+vmax)/255.
        mycol = hex2rgb(br_cls[nB1])
        col1 = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
        mycol = hex2rgb(br_cls[nB2])
        col2 = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
        plt.plot(ep,w1_smooth[n],'o',color=col1,markersize=4)
        plt.plot(ep,w2_smooth[n],'o',color=col2,markersize=4)

plt.xlim([epsNew[0],epsNew[-1]])
plt.ylim([2.321e-8,2.326e-8])
plt.gca().xaxis.set_major_formatter(mtick.FormatStrFormatter('%.6f')) 
plt.xticks([epsNew[0],(epsNew[0]+epsNew[-1])/2.,epsNew[-1]],[epsNew[0],' ',epsNew[-1]],color=col)
plt.yticks([2.321e-8,(2.321e-8+2.326e-8)/2,2.326e-8],color=col)


plt.setp(tuple(ax3.spines.values()), color=col)
plt.setp([ax3.get_xticklines(), ax3.get_yticklines()], color=col)
ax3.yaxis.get_offset_text().set_color(col)
#plt.savefig('test_suite_1_EigVals.eps', dpi=300)

#%%
plt.figure(num='2',figsize=(22*cm2in,15*cm2in))
plt.clf()

indB = [1,2]

for nV, nB in enumerate(indB):
    branch = branches[inds[nB]]
    
    plt.subplot(2,2,nV+1)
    hand = []
    
    for n in [51,55,65,75,85]:
        ep = eps[n]
        err = []
        errP = []
        name = r'$\epsilon = '+str("%.3f" % ep)+'$'
        
        fact = (-np.abs(0-ep)*(vmax-vmin)/(max(eps)-0)+vmax)/255.
        mycol = hex2rgb(br_cls[nB])
        col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
            
        for orde in range(0,order+1):
            w_pert = poly(ep,eps0,orde,branch,lamps)     
            err+=[np.abs((w_pert-lames[branch][n])/lames[branch][n])]
        hand += plt.semilogy(np.linspace(0,order,order+1),err,'-o',color=col,label=name,markersize=5)
    
    plt.grid(True)
    if nV == 0:
        plt.ylabel(r'$\left|\frac{\lambda_{exact}-\lambda_{\mathbf{\beta}}}{\lambda_{exact}}\right|$',fontsize=14)
    plt.tick_params(labelsize=10)
    plt.legend(handles=hand,fontsize=9)
    plt.tight_layout()
    plt.xlim((0,order))
    plt.ylim((1e-12,1e-3))
    plt.xticks(np.linspace(0,order,order+1))
    
for nV, nB in enumerate(indB):

    branch = branches[inds[nB]]   
    plt.subplot(2,2,nV+3)
    plt.cla()
    hand = []
    if nB == 1:
        nS = [51,60,65,70,75]
    else:
        nS = [51,52,53,55,60]
    for n in nS:
        ep = eps[n]
        err = []
        errP = []
        name = r'$\epsilon = '+str("%.3f" % ep)+'$'
    
        for orde in range(0,order+1):           
            L.active = ['omega','eps']
            #minimum residual
            v_pert = polyV(ep,eps0,orde,branch,vecps)   
            v_pert/=np.linalg.norm(v_pert)
            vec_a=L(poly(ep,eps0,order,branch,lamps),ep)*v_pert
            vec_b=L(poly(ep,eps0,order,branch,lamps),ep)*vecps[branch][0]
            c=(vec_a.H*vec_b).item(0)/(vec_b.H*vec_b).item(0)
            v_pert=v_pert-np.conj(c)*vecps[branch][0]
            v_pert/=np.linalg.norm(v_pert)
            Vx=veces[branch][n]
            Vx/=np.linalg.norm(Vx)
            errP+=[np.linalg.norm(Vx-v_pert*(v_pert.H*Vx))]
            
            
        fact = (-np.abs(0-ep)*(vmax-vmin)/(eps[nS[-1]]-0)+vmax)/255.
        mycol = hex2rgb(br_cls[nB])
        col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
        hand += plt.semilogy(np.linspace(0,order,order+1),errP,'-o',color=col,label=name,markersize=5)
    
    plt.grid(True)
    plt.xlabel('Perturbation order',fontsize=12)
    if nV == 0:
        plt.ylabel(r'$|\!|\mathbf{v}_{exact}-\mathbf{v}_{\mathbf{\beta}}|\!|$',fontsize=12)
        plt.ylim((1e-8,1))
        plt.legend(handles=hand,fontsize=9)

    else:
        plt.ylim((1e-9,1e-1))
        plt.legend(handles=hand,ncol=2,fontsize=9)
  
    plt.xlim((0,order))
    plt.tick_params(labelsize=10)
  
    plt.xticks(np.linspace(0,order,order+1))
    plt.tight_layout()
#plt.savefig('test_suite_1_Converg.eps', dpi=300)

#%%
nL = 300
plt.figure(num='3',figsize=(24*cm2in,11.6*cm2in))
plt.clf()

plt.subplot(2,4,1)
F = np.cos(2*np.pi*xxg)+np.cos(2*np.pi*yyg)
F /= np.max(np.abs(F))
cnt = plt.contourf(xxg,yyg,F,nL,vmin=-1.,vmax=1.)
for c in cnt.collections:
    c.set_edgecolor("face")
plt.clim([-1,1])

plt.subplot(2,4,5)
F = np.multiply(np.cos(2*np.pi*xxg),np.cos(1*np.pi*yyg))
F /= np.max(np.abs(F))
cnt = plt.contourf(xxg,yyg,F,nL,vmin=-1.,vmax=1.)
for c in cnt.collections:
    c.set_edgecolor("face")
plt.clim([-1,1])
    
for nB, branch in enumerate(branches):
    plt.subplot(2,4,nB+2)
    F = vecps[branch][0].reshape((25,25))
    F /= np.max(np.abs(F))    
    cnt = plt.contourf(xxg,yyg,F,nL,vmin=-1.,vmax=1.)
    for c in cnt.collections:
        c.set_edgecolor("face")
    plt.clim([-1,1])


x2 = np.linspace(0,2*np.pi,1001)    
y2 = np.linspace(0,2*np.pi,1001)    
[x2g,y2g] = np.meshgrid(x2,y2)    
f2 = np.sin(x2g)    
hand1 = plt.contourf(x2-100,y2-100,f2,100)
    
nB = 1    
ep = 4
branch = branches[nB]

plt.subplot(2,4,6)
#minimum residual
v_pert = polyV(ep,eps0,order,branch,vecps)   
v_pert/=np.linalg.norm(v_pert)
vec_a=L(poly(ep,eps0,order,branch,lamps),ep)*v_pert
vec_b=L(poly(ep,eps0,order,branch,lamps),ep)*vecps[branch][0]
c=(vec_a.H*vec_b).item(0)/(vec_b.H*vec_b).item(0)
v_pert=v_pert-np.conj(c)*vecps[branch][0]
v_pert/=np.linalg.norm(v_pert)

F = deepcopy(v_pert.reshape((25,25)))
F /= np.max(np.abs(F))
cnt = plt.contourf(xxg,yyg,F,nL,vmin=-1.,vmax=1.)
for c in cnt.collections:
    c.set_edgecolor("face")
plt.clim([-1,1])

plt.subplot(2,4,7)
#minimum residual
indsC = np.argsort(np.abs(eps-ep))
Vx=veces[branch][indsC[0]]
Vx/=np.linalg.norm(Vx)

F = deepcopy(Vx.reshape((25,25)))
F /= np.max(np.abs(F))
cnt = plt.contourf(xxg,yyg,F,nL,vmin=-1.,vmax=1.)
for c in cnt.collections:
    c.set_edgecolor("face")
plt.clim([-1,1])

plt.subplot(2,4,8)
#F = (Vx-v_pert*(v_pert.H*Vx)).reshape((25,25))
F = (Vx-v_pert).reshape((25,25))
hand2 = plt.contourf(xxg,yyg,np.log10(np.abs(F)),nL,cmap='Greys')
for c in hand2.collections:
    c.set_edgecolor("face")
    
for nP in range(1,9):
    plt.subplot(2,4,nP)
    plt.xticks([-0.5,-0.25,0,0.25,0.5],[-0.5, '', 0, '', 0.5])
    plt.yticks([-0.5,-0.25,0,0.25,0.5],[-0.5, '', 0, '', 0.5])
    if nP != 1 and nP!= 5:
        plt.yticks([])
    else:
        plt.ylabel(r'$y$')
    if nP <5:
        plt.xticks([])
    else:
        plt.xlabel(r'$x$')
    plt.axis('equal')
    plt.xlim([-0.5,0.5])
    plt.ylim([-0.5,0.5])
    
    plt.tight_layout

plt.subplot(2,4,1)
plt.title(r'$\mathrm{\mathbf{P}}_1$')
plt.subplot(2,4,2)
plt.title(r'$\mathbf{v}_{\mathbf{\beta}_{a,0}}$',color = br_cls[0])
plt.subplot(2,4,3)
plt.title(r'$\mathbf{v}_{\mathbf{\beta}_{s_{1},0}}$',color = br_cls[1])
plt.subplot(2,4,4)
plt.title(r'$\mathbf{v}_{\mathbf{\beta}_{s_{2},0}}$',color = br_cls[2])
plt.subplot(2,4,5)
plt.title(r'$\mathrm{\mathbf{P}}_2$')
plt.subplot(2,4,6)
plt.title(r'$\mathbf{v}_{\mathbf{\beta}_{s_{1}}}(\varepsilon = 4)$',color = br_cls[1])
plt.subplot(2,4,7)
plt.title(r'$\mathbf{v}_{exact_{s_{1}}}(\varepsilon = 4)$',color = br_cls[1])
plt.subplot(2,4,8)
plt.title(r'$\log_{10} | \mathbf{v}_{\mathbf{\beta}_{s_{1}}} - \mathbf{v}_{exact_{s_{1}}} |$',color = br_cls[1])


plt.subplot(2,4,4)
ax = plt.axes([0,0,1,1])
plt.xlim([0,1])
plt.ylim([0,1])
ax.tick_params(size=0)
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_facecolor("none")
for pos in ["right", "top", "bottom"]:
    ax.spines[pos].set_visible(False) 
cb1 = plt.colorbar(hand1, shrink = 0.35,anchor=(0.55,0.815),aspect = 10, ticks=[-1.0,-0.5,0,0.5,1.0])

plt.subplot(2,4,8)
ax2 = plt.axes([0,0,1,1])
plt.xlim([0,1])
plt.ylim([0,1])
ax2.tick_params(size=0)
ax2.set_xticklabels([])
ax2.set_yticklabels([])
ax2.set_facecolor("none")
for pos in ["right", "top", "bottom"]:
    ax2.spines[pos].set_visible(False)
cb2 = plt.colorbar(hand2, shrink = 0.35, anchor=(0.55,0.17),aspect = 10, ticks=[-10,-9,-8])
#plt.savefig('test_suite_1_Vectors.eps', dpi=500)
