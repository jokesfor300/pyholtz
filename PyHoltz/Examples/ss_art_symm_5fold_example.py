#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Thu Mar 15 14:57:53 2018
@author: orchini
5-fold degenerate case. This is adapted from the "ss_art_symm" case of Numer Matj (2015). 129: 353--381
It has a 5-fold degenerate baseline.
At first order the degeneracy is retained;
at 2nd order 2 eigenvalues split and the other 3 still forms a 3-fold degenrate subspace;
at 3rd order the degeneracy is fully resolved.
"""
#import os
#os.chdir('../')

import numpy as np
from scipy.linalg import eig
from PyHoltz.nlevp.all_algebra import pow1, pow2, sin_wa, exp_wa, log_waplusb, arctan_w
from PyHoltz.nlevp.parameter import ParaModel
from copy import deepcopy
from math import atan2


#%% Fix seed for reproducibility
np.random.seed(1)

N = 256

GA = np.random.rand(N ,N) + 0j
GB = np.linalg.inv(GA)

c0 = np.random.rand(N-5) + 0j+1
c1 = np.random.rand(N-5) + 0j
c2 = np.random.rand(N-5) + 0j

M1=np.zeros((N,N),dtype=complex)
M1[0,0]=1.
  
M2=np.zeros((N,N),dtype=complex)
M2[1,1]=1.
  
M3=np.zeros((N,N),dtype=complex)
M3[2,2]=1.
  
M4=np.zeros((N,N),dtype=complex)
M4[3,3]=1.
  
M5=np.zeros((N,N),dtype=complex)
M5[4,4]=1.
  
C0 = np.diag(np.insert(c0,0,[-1,0,0,0j,0]))
C1 = np.diag(np.insert(c1,0,[0,0,0,0j,0]))
C2 = np.diag(np.insert(c2,0,[0,0,0,0j,0]))
I = np.eye(N)

#%% Define parametric operator (nonlinear fashion)
L=ParaModel()

L.add_param('omega',1.)
L.add_param('eps',0)

L.add_term({'matrix':GA@M1@GB,'functions':[exp_wa(1.)],        'parameters':[['eps']],'operator':'M1'})
L.add_term({'matrix':GA@M2@GB,'functions':[sin_wa(1.)],        'parameters':[['eps']],'operator':'M2'})
L.add_term({'matrix':GA@M3@GB,'functions':[log_waplusb(1.,1.)],'parameters':[['eps']],'operator':'M3'})
L.add_term({'matrix':GA@M4@GB,'functions':[pow1],              'parameters':[['eps']],'operator':'M4'})
L.add_term({'matrix':GA@M5@GB,'functions':[arctan_w()],        'parameters':[['eps']],'operator':'M5'})
L.add_term({'matrix':GA@C0@GB,'functions':[],                  'parameters':[],       'operator':'C0'})
L.add_term({'matrix':GA@C1@GB,'functions':[pow1],              'parameters':[['eps']],'operator':'C1'})
L.add_term({'matrix':GA@C2@GB,'functions':[pow2],              'parameters':[['eps']],'operator':'C2'})
L.add_term({'matrix':-I,      'functions':[pow1],              'parameters':[['omega']],  'operator':'-I'})

#%% Define parametric operator (linear fashion)
M=ParaModel()
M.add_param('eps',0)

M.add_term({'matrix':GA@M1@GB,'functions':[exp_wa(1.)],        'parameters':[['eps']],'operator':'M1'})
M.add_term({'matrix':GA@M2@GB,'functions':[sin_wa(1.)],        'parameters':[['eps']],'operator':'M2'})
M.add_term({'matrix':GA@M3@GB,'functions':[log_waplusb(1.,1.)],'parameters':[['eps']],'operator':'M3'})
M.add_term({'matrix':GA@M4@GB,'functions':[pow1],              'parameters':[['eps']],'operator':'M4'})
M.add_term({'matrix':GA@M5@GB,'functions':[arctan_w()],        'parameters':[['eps']],'operator':'M5'})
M.add_term({'matrix':GA@C0@GB,'functions':[],                  'parameters':[],       'operator':'C0'})
M.add_term({'matrix':GA@C1@GB,'functions':[pow1],              'parameters':[['eps']],'operator':'C1'})
M.add_term({'matrix':GA@C2@GB,'functions':[pow2],              'parameters':[['eps']],'operator':'C2'})

#%% Calculate exact solution in a linear fashion
M.active=['eps']
w, p_adj, p = eig(M(0), left=True)
p = np.matrix(p)
p_adj = np.matrix(p_adj)
wInd = np.argsort(w)
p=p[:,wInd[0:5]]

#%% Calculate exact solution in a non-linear fashion
L.set_value('eps',0.0)
L.newton(0.0001, tol=0.0001, n_eig_val=5)

L.omega = np.real(L.omega)
L.p = np.real(L.p) + 0j
L.p_adj = np.real(L.p_adj) +0j

#%% Run perturbation theory
order = 10
L.perturb_degenerate('eps', N=order, issparse=False, debug=True, normalisation=True)

#%% Function to build power series approximations of the eigenvalue
def power_series_lambda(lams,eps,order,branch,L):
    lam = 0
    for n in range(0,order+1):
        lam += eps**n*L.Result[0][branch[0:n]]
    return lam
    
#%% Calculate exact solutions
branches = [ i for i in L.Result[0].keys() if len(i)==order]
eps = np.linspace(-0.8,0.8,101)
w_Exact = []
p_Exact = []
inds = [0,1,3,4,2]

for n, ep in enumerate(eps):
    w, p = eig(M(ep))
    w = np.real(w)
    p = np.matrix(np.real(p))
    
    w_Temp = []
    f = []
    f += [np.exp(ep)-1]
    f += [np.log(ep+1)]
    f += [ep]
    f += [np.sin(ep)]
    f += [np.arctan(ep)]
    
    for i in range(5):
        wIndTemp = np.argsort(abs(w-f[i]))
        w_Temp += [deepcopy(w[wIndTemp[0]])]
        if i == 0:
            p_Temp = deepcopy(p[:,wIndTemp[0]])
        else:
            p_Temp = np.hstack((p_Temp,deepcopy(p[:,wIndTemp[0]])))
        w = np.delete(w,wIndTemp[0])
        p = np.delete(p,wIndTemp[0],axis=1)
        
    w_Exact += [w_Temp]
    p_Exact += [p_Temp]
    
    if ep == 0:
        for nB in range(5):
            branch = branches[inds[nB]]
            p_Exact[n][:,nB] = L.Result[1][0][branch]        

#%% Create data_structure for plots
pert_eigVals = {}       
pert_eigVecs = {}
eps = eps
exact_eigVals = {}
exact_eigVecs = {}

for nB, branch in enumerate(branches):
    lamp = []
    lame = []
    vp = []
    ve = []
    branch = branches[inds[nB]]
    for k in range(order+1):
         lamp += [L.Result[0][branch[0:k]]]
         vp += [L.Result[1][k][branch]]   
    for n, ep in enumerate(eps):        
        lame += [w_Exact[n][nB]]
        ve += [p_Exact[n][:,nB]]
    pert_eigVals[branch] = np.array(lamp)
    pert_eigVecs[branch] = vp
    exact_eigVals[branch] = lame
    exact_eigVecs[branch] = ve

#%%
import pickle
pickle.dump([branches,pert_eigVals,pert_eigVecs,eps,exact_eigVals,exact_eigVecs], open("PyHoltz/Examples/ss_art_symm_5fold_data.pickle", "wb" ) )

#%% Run some tests on the eigenvectors (Note that the problem is defined as an eigendecomposition of a matrix...)
# Eigenvectors test
ind = 3
inds = [0,2,4,3,1]
branch = branches[ind]
v1 = L.Result[1][0][branch]
GA = np.matrix(GA)
v2 = GA[:,inds[ind]]
v2 = v2/np.linalg.norm(v2)
proj = v1.H*v2
v2 *= np.exp(-1j*atan2(np.imag(proj),np.real(proj))) 
print(np.linalg.norm(v2-v1))

 