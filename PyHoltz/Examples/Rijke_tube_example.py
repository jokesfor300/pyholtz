#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
from PyHoltz.FEM.mesh_class import mesh 
from PyHoltz.FEM.discretization import discretization

import numpy as np
from PyHoltz.FEM.vtk_write import vtk_write, vtk_write2
from PyHoltz.nlevp.all_algebra import pow0,pow1,pow2,ntau2
from time import time
from matplotlib import pyplot as plt

#reload(discretization)
#%%
msh=mesh('./PyHoltz/mesh/rijke3D_FlameAtHalf.msh')

#msh=mesh()
#msh.collect_lines() #2nd_order elements
#%% define model
T_u = 300  # cold gas temperature in K
T_b = 4*T_u  # hot gas temperature in K
A = np.pi*0.025**2  # burner cross section in m^2
#M=28.8
#R=8314

gamma=1.4
P_0 =101325.0# 101325  # quiescent pressure Pa
rho= P_0/288.68/T_u


Q02u0 = P_0*(T_b/T_u-1)*A*gamma/(gamma-1)

x_ref=np.array([0.00, 0, -0.005])
ref_vec=np.array([0,0,1])

n=1
tau= 0.001
FTF=ntau2(n,tau)                      

def FTFwrapper(omega,n,tau,d_omega,d_n,d_tau):
    return FTF([n,tau],[d_n,d_tau],omega,d_omega)

#from statespace import ftf as FTF

def temp (x,y,z):
    if (z<0):
        return T_u
    else:
        return T_b
cfunc = lambda x, y, z: np.sqrt(gamma*288.68*temp(x,y,z))
c_field=np.empty((len(msh.Points),1))
for i, pnt in enumerate(msh.Points):
    c_field[i]=cfunc(pnt[0],pnt[1],pnt[2]) #TODO: vectorize  

#model description
model={ 'Interior':['interior',(),[]],
        'Outlet':['impedance',1E-15,['Y']],
        'Flame':['flame',(rho,gamma,Q02u0,x_ref,ref_vec,FTFwrapper,n,tau),['n','tau']] #TODO: rewrite ntau such that it is seperated as follows f(n)*g(omega,tau)
        #'Flame':['measured flame',(rho,gamma,Q02u0,x_ref,ref_vec,FTF,n),['n']]
        }
        
msh.keep(list(model.keys()))
print(msh)
#%%

discr=discretization(msh,model,c_field)
L=discr.discretize_operators()

#for i in range(len(L.terms)):
#    L.terms[i]['matrix']*=1E-10
       

#%%
#L.newt((820)*np.pi*2,relax=.5,maxiter=30)
L.add_param('λ',0)
L.add_term({'matrix':-L.terms[1]['matrix'],'functions':[pow1],'parameters':[['λ']],'operator':'-I'})
L.set_value('tau',tau)
#%%
L.householder((240)*np.pi*2,relax=1,maxiter=30,tol=1E-10,order=1,output=True)

#L.householder(1.6,relax=1,maxiter=30,tol=1E-10,order=1)

#%%
#zeit_par=time()
L.perturb_parallel('tau',30,normalize=True)
#L.perturb_parallel('n',30,)
#zeit_par-=time()
#%%
#zeit_fast=time()
#L.new_perturb_fast('tau',30)
#zeit_fast-=time()

sol=L.solution()
sol.normalize_pert()

    
#%%
sol.estimate_pol()
lam=L.omega_pert.copy()
expand=1.1
eps=np.linspace(sol.eps0-sol.conv_radius[-1]*expand,sol.eps0+sol.conv_radius[-1]*expand,101)
eps=np.round(eps,6)
Eps=np.linspace(sol.eps0-sol.conv_radius[-1]*expand,sol.eps0+sol.conv_radius[-1]*expand,1001)

#%%
#lam_exact=[]
#v_exact=[]
#for idx,e in enumerate(eps):
#    print(idx)
#    L.set_value(sol.epsilon,e)
#    L.householder(sol.poly(e,15),tol=1E-10,maxiter=15,output=False,order=1)
#    lam_exact+=[L.omega.item(0)]
#    v_exact+=[L.p]
#lam_exact=np.array(lam_exact)


lam_exact=[]
v_exact=[]
e=eps[50]
tempsol=sol
for idx,e in enumerate(eps[50:]):
    print(idx)
    L.set_value(sol.epsilon,e)
    L.householder(tempsol.poly(e,3),tol=1E-10,maxiter=30,output=False,order=1)
    lam_exact+=[L.omega.item(0)]
    v_exact+=[L.p]
    L.perturb_parallel(sol.epsilon,3,normalize=True)
    tempsol=L.solution()

tempsol=sol
for idx,e in enumerate(reversed(eps[0:50])):
    print(idx)
    L.set_value(sol.epsilon,e)
    L.householder(tempsol.poly(e,3),tol=1E-10,maxiter=30,output=False,order=1)
    lam_exact=[L.omega.item(0)]+lam_exact
    v_exact=[L.p]+v_exact
    L.perturb_parallel(sol.epsilon,3,normalize=True)
    tempsol=L.solution()    
    
    
lam_exact=np.array(lam_exact)


#%% normalize exact v
v0=sol.p_pert[0]
for i in range(len(v_exact)):
    print(np.abs(np.exp(np.angle(v0.H*v_exact[i])*1j)))
    v_exact[i]/=np.exp(np.angle(v0.H*v_exact[i])*1j).item(0)
    v_exact[i]/=np.linalg.norm(v_exact[i])
#    v_exact[i] /= v0.H*v_exact[i]

#%% 
plt.clf()
results=[]
for i,e in enumerate(eps):
    results+=[np.linalg.norm(v_exact[i]-sol.polyP(e,30)) ]
plt.figure('3')
plt.plot(eps,results)

#%%prepare colors
cmap=plt.get_cmap('viridis_r')
colors=cmap(np.linspace(0.,1.,6))
cmap2=plt.get_cmap('magma_r')
#%%
plt.close('all')
plt.figure('eigval',figsize=(5.0/1.3,4.8/1.3))
levels=[1,2,5,10,15,30]
for N, clr in zip(levels,cmap(np.linspace(0.,1.,len(levels)))):#,25]:#range(0,25):
    lams=sol.poly(Eps,N)
    plt.plot(np.real(lams),np.imag(lams),color=clr,label='N='+str(N))
plt.plot(np.real(sol.omega),np.imag(sol.omega),'ko')

plt.plot(np.real(lam_exact),np.imag(lam_exact),'k',label='exact')
plt.arrow(np.real(lam_exact[-2]),np.imag(lam_exact[-2]),np.real(lam_exact[-1]-lam_exact[-2]),np.imag(lam_exact[-1]-lam_exact[-2]),fc="k",ec="k",head_width=15,zorder=20)

plt.legend()
plt.grid('on')
plt.xlabel('real ($\omega$)')
plt.ylabel('imag ($\omega$)')
plt.axis('equal')
plt.title('a) eigenvalue evolution')
plt.xticks(np.linspace(700,1300,7))
plt.tight_layout()
    
#plt.figure('error',figsize=(9,4.8))
plt.figure('error',figsize=(12.8/1.3,4.8/1.3))

L.active=['omega',sol.epsilon]
#['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
levels=[51,62,75,90,100]
#levels=[49,38,25,10,0]
for O, color in zip(levels,cmap2(np.linspace(0.15,1.,len(levels)))):
    err=[]
    errV=[]
    errP=[]
    norm=[]
    for N in range(0,31):
        print(N)
        err+=[lam_exact[O]-sol.poly(eps[O],N)]
        
        Vp=np.matrix(sol.polyP(eps[O],N))
        V1=Vp.copy()
        #minimum residual
        Vp/=np.linalg.norm(Vp)
        vec_a=L(sol.poly(eps[O],N),eps[O])*Vp
        vec_b=L(sol.poly(eps[O],N),eps[O])*sol.p_pert[0]
        c=(vec_a.H*vec_b).item(0)/(vec_b.H*vec_b).item(0)
        Vp=Vp-np.conj(c)*sol.p_pert[0]
        Vp/=np.linalg.norm(Vp)
        Vx=v_exact[O].copy()
        Vx/=np.linalg.norm(Vx)
        #res0=Vx-(Vp.H*Vx).item(0)*Vp
        res0=Vp-(Vx.H*Vp).item(0)*Vx   
        
        #iterative scheme
        Vp=np.matrix(sol.polyP(eps[O],N))
        print('This should be zero:',np.sum(np.abs(V1-Vp)))
        Vx=v_exact[O].copy()
        res=Vx-(Vp.H*Vx).item(0)*Vp
    
        errP+=[np.sqrt(res0.H*res0).item(0)]
        errV+=[np.sqrt(res.H*res).item(0)]
        
    err=np.array(err)
    errV=np.array(errV)
    plt.figure('error')
    plt.subplot(121)    
    plt.semilogy(np.abs(err),color=color,label=r'$\tau$='+str(round(eps[O]*1000,3))+' ms')
    plt.subplot(122)
    plt.semilogy(np.abs(errP),color=color,label=r'$\tau$='+str(round(eps[O]*1000,3))+' ms')
    
    #plt.gca().set_prop_cycle(None)
    plt.hold('all')
    plt.semilogy(np.abs(errV),'--',color=color,label='__nolabel__')
    plt.hold('on')
    plt.semilogy(norm)
    
    
    
#from matplotlib.font_manager import FontProperties
#
#fontP = FontProperties()
#fontP.set_size('small')

    
plt.figure('error')
plt.subplot(121)
#plt.legend()
plt.grid('on')
plt.xticks(np.linspace(0,30,7))
plt.xlabel('perturbation order $N$')
plt.ylabel('$|\lambda_\mathrm{exct}-\lambda_\mathrm{pert}|$')
plt.title('c) eigenvalue error')
plt.legend(bbox_to_anchor=(0,-.35,2.24, 0.102), loc=3, mode = "expand", borderaxespad=0,ncol=5)

plt.subplot(122)
#plt.legend(bbox_to_anchor=(1.04,1.), borderaxespad=0)
plt.grid('on')
plt.xlabel('perturbation order $N$')
plt.xticks(np.linspace(0,30,7))
plt.ylabel('$||\mathbf{v}_\mathrm{exct}-\mathbf{v}_\mathrm{pert}||$')        
plt.title('d) eigenvector error')
#plt.suptitle('Errors')     

L.active=['omega']
#plt.tight_layout(rect=[0,0,0.86,1])
plt.tight_layout(rect=[0,0.075,1,1])
#%%
exmpl=95
vtk_write(msh.Name[0:-4],msh,{'1':np.abs(sol.polyP(eps[exmpl],1)), 
          '2':np.abs(sol.polyP(eps[exmpl],2)),'5':np.abs(sol.polyP(eps[exmpl],5)),
          '10':np.abs(sol.polyP(eps[exmpl],10)),'15':np.abs(sol.polyP(eps[exmpl],15)),
          '30':np.abs(sol.polyP(eps[exmpl],30)),'unperturbed':np.abs(sol.polyP(eps[exmpl],0)),
          'exact':np.abs(v_exact[exmpl]),'1phase':np.angle(sol.polyP(eps[exmpl],1)), 
          '2phase':np.angle(sol.polyP(eps[exmpl],2)),'5phase':np.angle(sol.polyP(eps[exmpl],5)),
          '10phase':np.angle(sol.polyP(eps[exmpl],10)),'15phase':np.angle(sol.polyP(eps[exmpl],15)),
          '30phase':np.angle(sol.polyP(eps[exmpl],30)),'unperturbedphase':np.angle(sol.polyP(eps[exmpl],0)),
          'exactphase':np.angle(v_exact[exmpl]),'cfield':np.real(c_field)})