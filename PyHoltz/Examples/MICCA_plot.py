#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Plots some results for the MICCA_example routine.
"""
import os

from PyHoltz.FEM.vtk_write import vtk_write
import pickle
import numpy as np
from matplotlib import pyplot as plt
from copy import deepcopy
from math import atan2
from scipy.interpolate import interp1d
#%%
def poly(eps,eps0,order,branch,lamps):
    lam = 0.
    for k in range(order+1):
        lam += (eps - eps0)**k*lamps[branch][k]
    return lam

def polyV(eps,eps0,order,branch,vecps):
    p_pert = deepcopy(vecps[branch][0])*0.
    for k in range(order+1):
        p_pert += (eps - eps0)**k*vecps[branch][k]
    return p_pert


def hex2rgb(hex_val):
    hex_val = hex_val.strip('#')
    rgb_val = tuple(int(hex_val[i:i+2], 16) for i in (0, 2 ,4))
    return rgb_val


#%%
Data = pickle.load( open( "PyHoltz/Examples/Micca_dat.pickle", "rb" ) )
branches = Data[0]
lamps = Data[1] # Perturpation LAMbddaS Coefficients
vecps = Data[2] # Perturbation VECtorS Coefficients
taus = Data[3]
lames = Data[4] # Exact LAMbdaS
veces = Data[5] # Exact VECtorS
msh = Data[6]
taus2 = Data[7]
lames2 = Data[8] # Exact LAMbdaS
veces2 = Data[9] # Exact VECtorS

order = len(lamps[branches[0]]) - 1
omega0 = deepcopy(lamps[branches[0]][0])    
tau0 = 0.003
taus2 = np.linspace(tau0,taus[-1],51)

#%%
br_cls = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
cm2in = 0.393701
vmax = 1
vmin = 0.5
plt.close('all')
plt.figure(num='1',figsize=(24*cm2in,8*cm2in))
plt.clf()

lbSize = 11
fnSize = 9
lgSize = 9
lg2Size = 6.5

hand = []

inds = [0,1]

plt.figure('1')
for i in range(2):
    plt.subplot(1,3,1)
    plt.grid(True)
    plt.plot((taus-tau0)/tau0,np.real(lames[branches[i]])/2/np.pi,'k-',linewidth=2)
    plt.subplot(1,3,2)
    plt.grid(True)
    plt.plot((taus-tau0)/tau0,np.imag(lames[branches[i]]),'k-',linewidth=2)

names = [r'$\mathbf{\beta}_1$',r'$\mathbf{\beta}_2$']
for nB, branch in enumerate(branches):
    branch = branches[inds[nB]]
    for n, tau in enumerate(taus):
        w_pert = poly(tau,tau0,order,branch,lamps)                

        proj = vecps[branch][0].H*veces[branch][n]
        veces[branch][n] *= np.exp(-1j*atan2(np.imag(proj),np.real(proj))) 
        
        fact = (-np.abs(tau0-tau)*(vmax-vmin)/(max(taus-tau0)-0)+vmax)/255.
        mycol = hex2rgb(br_cls[nB])
        col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
            
        if n == 1:
            plt.subplot(1,3,1)
            hand += plt.plot((tau-tau0)/tau0,np.real(w_pert)/2/np.pi,'o',color=col,markersize=4,label=names[nB])
            plt.subplot(1,3,2)
            plt.plot((tau-tau0)/tau0,np.imag(w_pert),'o',color=col,markersize=4,label=names[nB])
        else:
            plt.subplot(1,3,1)
            plt.plot((tau-tau0)/tau0,np.real(w_pert)/2/np.pi,'o',color=col,markersize=4)
            plt.subplot(1,3,2)
            plt.plot((tau-tau0)/tau0,np.imag(w_pert),'o',color=col,markersize=4)
                
plt.subplot(1,3,1)
plt.xlabel(r'$\Delta\tau/\tau_0$',fontsize=lbSize)
plt.ylabel(r'$\mathrm{Re}\left[\lambda_{\mathbf{\beta}}\right]/(2\pi) \quad (\mathrm{Hz})$',fontsize=lbSize)
plt.tick_params(labelsize=fnSize)
plt.legend(handles=hand,fontsize=lgSize,ncol=2)
plt.xticks([-0.2,-0.1,0,0.1,0.2])
plt.tight_layout()

plt.subplot(1,3,2)
plt.xlabel(r'$\Delta\tau/\tau_0$',fontsize=lbSize)
plt.ylabel(r'$\mathrm{Im}\left[\lambda_{\mathbf{\beta}}\right] \quad (\mathrm{s}^{-1})$',fontsize=lbSize)
plt.tick_params(labelsize=fnSize)
plt.legend(handles=hand,fontsize=lgSize,ncol=2)
plt.xticks([-0.2,-0.1,0,0.1,0.2])
plt.tight_layout()

ax = plt.subplot(1,3,3)
nB = 0
branch = branches[nB]
hand = []
plt.grid(True)
errs = []

for ind_ord, orders in enumerate(range(0,order+1)):

    err = []

    for n, tau in enumerate(taus2):
        w_pert = poly(tau,tau0,orders,branch,lamps)                
        w_ex = lames2[branch][n]
        err += [(np.abs(w_pert-w_ex)+np.finfo(float).eps)/np.abs(w_ex)]
    
    if ind_ord%2 == 0:    
        hand+=plt.semilogy((taus2-tau0)/tau0,err,'-',color=np.array([1, 1, 1])*(orders)/(order+2),
                        label='$N = '+str(orders)+'$',lw=2)
    errs += [err]

ind = len(taus2)-1
diffs = np.array(errs[-1])-np.array(errs[-2])
inds = len(diffs)
bVal = True
while bVal:
    inds = inds-1
    bVal = diffs[inds]*diffs[inds-1] > 0
myConv = (diffs[inds]/(diffs[inds]-diffs[inds-1])*(taus2[inds-1]-taus2[inds]) + taus2[inds]-tau0)/tau0
myErr = errs[-1][inds]+(errs[-1][inds-1]-errs[-1][inds])*(myConv-(taus2[inds]-tau0)/tau0)/((taus2[inds-1]-taus2[inds])/tau0)
plt.plot([myConv,myConv],[1e-16,myErr],'--',color=br_cls[3])
plt.plot([myConv],[myErr],'o',color=br_cls[3])

plt.xlim([(taus2[0]-tau0)/tau0,(taus2[-1]-tau0)/tau0*1.2])
plt.ylim((1e-16,1e-1))
plt.xlabel(r'$\Delta\tau/\tau_0$',fontsize=lbSize)
plt.ylabel(r'$\left|\lambda^{(exact)}-\lambda_{\mathbf{\beta_1}}^{(N)}\right|/\lambda^{(exact)}$',fontsize=lbSize)
plt.tick_params(labelsize=fnSize)
plt.legend(handles=hand,fontsize=lg2Size,bbox_to_anchor=(.74, .76),fancybox=True,framealpha=1)
plt.tight_layout()
#plt.savefig('Micca_EigVals.eps', dpi=300)

#%%
plt.figure(num='2',figsize=(20*cm2in,10*cm2in))
plt.clf()

nB = 1
branch = branches[nB]
plt.subplot(1,2,1)
hand = []

for n in [11,13,15,17,20]:
    tau = taus[n]
    err = []
    errP = []
    name = r'$\Delta\tau / \tau_0 = '+str("%.3f" % ((tau-tau0)/tau0))+'$'
    
    fact = (-np.abs(tau0-tau)*(vmax-vmin)/(max(taus-tau0)-0)+vmax)/255.
    mycol = hex2rgb(br_cls[nB])
    col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
        
    for orde in range(0,order+1):
        w_pert = poly(tau,tau0,orde,branch,lamps)     
        err+=[np.abs((w_pert-lames[branch][n])/lames[branch][n])]
    hand += plt.semilogy(np.linspace(0,order,order+1),err,'-o',color=col,label=name,markersize=5)

plt.grid(True)
plt.xlabel('Perturbation order $N$',fontsize=14)
plt.ylabel(r'$\left|\frac{\lambda^{(exact)}-\lambda_{\mathbf{\beta_2}}^{(N)}}{\lambda^{(exact)}}\right|$',fontsize=14)
plt.tick_params(labelsize=10)
plt.legend(handles=hand,fontsize=9)
plt.xlim((0,order))
plt.ylim((1e-16,1e-1))
ticksx = np.linspace(0,order,order+1)
plt.xticks(ticksx[0::2])
plt.tight_layout()

plt.subplot(1,2,2)
plt.cla()
hand = []
nS =[11,13,15,17,20]
for n in nS:
    tau = taus[n]
    err = []
    errP = []
    name = r'$\Delta\tau / \tau_0 = '+str("%.3f" % ((tau-tau0)/tau0))+'$'

    for orde in range(0,order+1):
        v_pert = polyV(tau,tau0,orde,branch,vecps)   
        Vx=veces[branch][n]
        errP+=[np.linalg.norm(Vx-v_pert)]
       
    fact = (-np.abs(tau0-tau)*(vmax-vmin)/(max(taus-tau0)-0)+vmax)/255.
    mycol = hex2rgb(br_cls[nB])
    col = (mycol[0]*fact, mycol[1]*fact, mycol[2]*fact)
    hand += plt.semilogy(np.linspace(0,order,order+1),errP,'-o',color=col,label=name,markersize=5)

plt.grid(True)
plt.xlim((0,order))
plt.ylim((1e-15,1))
plt.xlabel('Perturbation order $N$',fontsize=14)
plt.ylabel(r'$|\!|\mathbf{v}^{(exact)}-\mathbf{v}_{\mathbf{\beta_2}}^{(N)}|\!|$',fontsize=14)
plt.legend(handles=hand,fontsize=9)
plt.tick_params(labelsize=10)
plt.xticks(ticksx[0::2])
plt.tight_layout()
#plt.savefig('Micca_Conv.eps', dpi=300)

#%%
plt.close('3')
fig = plt.figure(num='3',figsize=(9*cm2in,8*cm2in))
plt.clf()
ax = plt.subplot(111)
nB = 0
branch = branches[nB]
hand = []
plt.grid(True)
errs = []

for ind_ord, orders in enumerate(range(0,order+1)):
    err = []

    for n, tau in enumerate(taus2):
        w_pert = poly(tau,tau0,orders,branch,lamps)                
        w_ex = lames2[branch][n]
        err += [(np.abs(w_pert-w_ex)+np.finfo(float).eps)/np.abs(w_ex)]
    
    if ind_ord%2 == 0:    
        hand+=plt.semilogy((taus2-tau0)/tau0,err,'-',color=np.array([1, 1, 1])*(orders)/(order+2),
                        label='$N = '+str(orders)+'$',lw=2)
    errs += [err]

ind = len(taus2)-1
diffs = np.array(errs[-1])-np.array(errs[-2])
inds = len(diffs)
bVal = True
while bVal:
    inds = inds-1
    bVal = diffs[inds]*diffs[inds-1] > 0
myConv = (diffs[inds]/(diffs[inds]-diffs[inds-1])*(taus2[inds-1]-taus2[inds]) + taus2[inds]-tau0)/tau0

plt.plot([myConv,myConv],[1e-16,2.98e-4],'--',color=br_cls[3])
plt.plot([myConv],[2.98e-4],'o',color=br_cls[3])

plt.xlim([(taus2[0]-tau0)/tau0,(taus2[-1]-tau0)/tau0*1.2])
plt.ylim((1e-16,1e-1))
plt.xlabel(r'$\Delta\tau/\tau_0$',fontsize=9)
plt.ylabel(r'$\left|\lambda^{(exact)}-\lambda_{\mathbf{\beta_1}}^{(N)}\right|/\lambda^{(exact)}$',fontsize=10)
plt.tick_params(labelsize=10)
plt.legend(handles=hand,fontsize=7)
plt.tight_layout()
#plt.savefig('Micca_Radius.eps', dpi=300)






