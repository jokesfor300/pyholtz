#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Fri Feb 23 14:21:43 2018
@author: orchini
Example from https://physics.stackexchange.com/questions/7679/perturbation-theory-with-degeneracy-even-after-1st-order
It is a 2-fold degenerate eigenvalue problem arising in Quantum Mechanics. It is linear and normal, used as first benchmark
"""
#import os
#os.chdir('../')

import numpy as np
from scipy.linalg import eig
from PyHoltz.nlevp.all_algebra import pow1
from PyHoltz.nlevp.parameter import ParaModel
from PyHoltz.Examples.Stark_Effect_Hydrogen_plot import plot_degenerate_perturb

#%% Define matrices
N=4

I=np.matrix(np.eye(N)) + 0j

A= np.matrix(np.zeros((N,N))) + 0j 
A[0,0], A[1,1] = 1, 1;

B=np.matrix(np.zeros((N,N))) + 0j
B[0,2], B[0,3], B[1,2], B[1,3], B[2,0], B[3,0], B[2,1], B[3,1] = 1, 1, 1, 1, 1, 1, 1, 1 

w,vl,vr=eig(A,left=True)
#%% Set up parametric model (in a nonlinear fashion)
L=ParaModel()

L.add_param('omega',1.)
L.add_param('eps',0)

L.add_term({'matrix':A,'functions':[],'parameters':[],'operator':'A'})
L.add_term({'matrix':-I,'functions':[pow1],'parameters':[['omega']],'operator':'-I'})
L.add_term({'matrix':B,'functions':[pow1],'parameters':[['eps']],'operator':'B'})

#%% Set up parametric model (in a linear fashion, for the exact solutions)
M=ParaModel()
M.add_param('eps',0)

M.add_term({'matrix':A,'functions':[],'parameters':[],'operator':'A'})
M.add_term({'matrix':B,'functions':[pow1],'parameters':[['eps']],'operator':'B'})

M.active=['eps']
w, p_adj, p = eig(M(0), left=True)
p = np.matrix(p)
p_adj = np.matrix(p_adj)
#%% Perform perturbatuib theory

# The Newton solver has issues with very small matrices. Use an explicit method instead based on the linear eigenproblem
# L.newt(0.1,tol=1E-8,n_eig_val=2)

L.omega = w[2]
L.p = np.matrix(p[:,2:4],dtype=complex)
L.p_adj = np.matrix(p_adj[:,2:4],dtype=complex)
order = 16
L.perturb_degenerate('eps', N=order, issparse=False, debug=True, normalisation=True)

#%% Exact results
eps = np.linspace(0,0.4,401)

exact = {'omega': [], 'p': [], 'p_adj': [], 'eps': []}

for n, ep in enumerate(eps):

    w, p_adj, p = eig(M(ep), left=True)    
    
    exact['omega'].append(w)
    exact['p'].append(p)    
    exact['p_adj'].append(p_adj)    
    exact['eps'].append(ep)    
    
#%% Post processing
branch_num = 0;
fig_name = 'TestCase2_Branch'+str(branch_num)+'.eps'
plot_degenerate_perturb(L.Result,branch_num,exact,order,fig_name)
