# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Sat Feb 17 11:39:39 2018

@author: georg
"""

import multiprocessing as mp
import os


def parallizer(Menge, function, mode='sum', pipe=False, Master=True):
    if Master:
        print('Menge an Aufgaben: ', len(Menge))
        Number_CPU = mp.cpu_count()
        # partition workload
        parts = len(Menge) // Number_CPU  # Note that // is integer devision
        extra = len(Menge) % Number_CPU  # there might be some partitions with one extra job
        active_processes = []
        # The master process invokes the slaves
        # currently there is only one hierachy level, i.e. the slaves cannot be masters of new process
        for CPU in range(0, Number_CPU - 1):  # tehre are Number_CPU -1 slaves
            receiver, transmitter = mp.Pipe(False)  # initialize a connection object for cummunication
            if CPU <= extra - 1:
                n = parts + 1
            else:
                n = parts
            # initialize slave process and store process and corresponding receiver in list
            active_processes.append(
                (mp.Process(target=parallizer, args=(Menge[:n], function, mode, transmitter, False)), receiver))
            active_processes[CPU][0].start()  # start the slave process
            Menge[:n] = []  # remove the asigned tasks from job que

    if mode == 'sum':
        y = function(Menge.pop())

        def add(calc):
            return function(calc)
    elif mode == 'append':
        y = []

        def add(calc):
            return [function(calc)]
    elif mode == 'proto':
        y = []

        def add(calc):
            return [[calc, function(calc)]]

    # Do the work        
    for calc in Menge:
        y += add(calc)  # it seems rediculous but the actual work is done is in this line ^^
    # print('Tschüss',Master,flush=True)
    if not Master:
        pipe.send(y)  # E.T. home phone
        pipe.close()  # hang up

    # collect results from slaves
    if Master:
        sentinel = False
        while active_processes:  # this loop runs until all slaves have finished

            for (idx, (proc, rcv)) in enumerate(active_processes):
                if rcv.poll():  # is there result in the pipe?
                    y += rcv.recv()  # get the result
                    proc.terminate()  # terminate the slave process
                    proc.join()  # wait until termination has really finished
                    sentinel = True
                    break

            if sentinel:
                active_processes[idx:idx + 1] = []  # remove slave from list
                sentinel = False

    return y  # return result


# %% example
# def function(calc): # die Funktion die alle Einträge der Menge verarbeitet
#    # calc = f(calc) mit calc eine weitere funktion ausführen oder sonst was machen, dann
#    return calc
#
#
# Menge =[1]*30000000
# a = parallizer(Menge,function)
# print(a)

# %% Ein neuer Parallelisierer, der Aufgaben unterschiedlicher Länge bewältigt

def func(x):
    return x


import multiprocessing as mp


def parallizer2(menge, function, mode='sum', pipe=False, master=True, lock=None, CPU=1000, queue=True, ID=0,
                partitioned=False):  # TODO: maximum CPU Number is 1000
    if queue:
        if master:
            # print('Menge an Aufgaben: ',len(Menge))
            ncpu = mp.cpu_count()
            ncpu = min(ncpu, CPU)

            tasks = mp.SimpleQueue()  # queues are thread-safe
            for idx, i in enumerate(menge):
                tasks.put(i)

            menge = tasks
            lock = mp.Lock()

        if mode == 'sum':
            lock.acquire()
            if not menge.empty():
                y = function(menge.get())
            else:
                y = None
            lock.release()

            def add(calc):
                return function(calc)
        elif mode == 'append':
            y = []

            def add(calc):
                return [function(calc)]
        elif mode == 'proto':
            y = []

            def add(calc):
                return [[calc, function(calc)]]
        elif mode == 'csv':  # at the moment a very spacial task
            y = 0

            def add(calc):
                function(calc, ID)  # function gets called, no return -> saves data in csv
                return 1  # return is just a number without meaning

        if master:
            active_processes = []
            # The master process invokes the slaves
            # currently there is only one hierachy level, i.e. the slaves cannot be masters of new process
            for CPU in range(0, ncpu - 1):  # tehre are ncpu -1 slaves
                receiver, transmitter = mp.Pipe(False)  # initialize a connection object for cummunication
                # initialize slave process and store process and corresponding receiver in list
                active_processes.append((mp.Process(target=parallizer2, args=(
                    menge, function, mode, transmitter, False, lock, 1, True, ID)), receiver))
                active_processes[CPU][0].start()  # start the slave process
                ID += 1  # Just interesting for csv files

        # Do the work+
        while not menge.empty():
            lock.acquire()
            if not menge.empty():
                calc = menge.get()
                lock.release()
                y += add(calc)  # it seems rediculous but the actual work is done is in this line ^^
            else:
                lock.release()

    if not queue:
        if master:
            # print('Menge an Aufgaben: ',len(Menge),flush=True)
            ncpu = mp.cpu_count()
            ncpu = min(ncpu, CPU)
            # partition workload
            parts = len(menge) // ncpu  # Note that // is integer devision
            extra = len(menge) % ncpu  # there might be some partitions with one extra job
            active_processes = []
            # The master process invokes the slaves
            # currently there is only one hierachy level, i.e. the slaves cannot be masters of new process
            for CPU in range(0, ncpu - 1):  # tehre are ncpu -1 slaves
                receiver, transmitter = mp.Pipe(False)  # initialize a connection object for cummunication
                if CPU <= extra - 1:
                    n = parts + 1
                else:
                    n = parts
                # initialize slave process and store process and corresponding receiver in list
                if partitioned:
                    active_processes.append((mp.Process(target=parallizer2, args=(
                        menge[:n][0], function, mode, transmitter, False, None, 1, False, ID, True)), receiver))
                else:
                    active_processes.append((mp.Process(target=parallizer2, args=(
                        menge[:n], function, mode, transmitter, False, None, 1, False, ID)), receiver))
                active_processes[CPU][0].start()  # start the slave process
                menge[:n] = []  # remove the asigned tasks from job que
                ID += 1  # Just interesting for csv files

        if mode == 'sum':
            if len(menge) != 0:
                y = function(menge.pop())
            else:
                y = None

            def add(calc):
                return function(calc)
        elif mode == 'append':
            y = []

            def add(calc):
                return [function(calc)]
        elif mode == 'proto':
            y = []

            def add(calc):
                return [[calc, function(calc)]]
        elif mode == 'csv':  # at the moment a very spacial task
            y = 0

            def add(calc):
                function(calc, ID)  # function gets called, no return -> saves data in csv
                return 1

                # Do the work
        for calc in menge:
            y += add(calc)

    # From here on the code for Queue = True and Queue = False is identical
    if not master:
        pipe.send(y)  # E.T. home phone
        pipe.close()  # hang up

    # collect results from slaves
    if master:
        sentinel = False
        while active_processes:  # this loop runs until all slaves have finished

            for (idx, (proc, rcv)) in enumerate(active_processes):

                if rcv.poll():  # is there result in the pipe?
                    x = rcv.recv()
                    if type(y) == type(None):
                        y = x
                    elif type(x) != type(None):
                        y += x  # get the result

                    proc.terminate()  # terminate the slave process
                    proc.join()  # wait until termination has really finished
                    sentinel = True
                    break

            if sentinel:
                active_processes[idx:idx + 1] = []  # remove slave from list
                sentinel = False
            # else:
            # print('Waiting...',flush=True)
        # print('finished')
        return y  # return result
    return

# %%
# def func(x):
#        return x
# Menge = [1]*10000
#
# a = parallizer2(Menge,func,pipe=True,CPU=4)
#
# a = parallizer2(Menge,func,Queue=False)
# a = parallizer2(Menge,func)
# a = parallizer2(Menge,func,CPU=8,Queue=True)
