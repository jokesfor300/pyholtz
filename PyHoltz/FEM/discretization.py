#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Fri Mar 17 15:14:48 2017

@author: georg
"""

#import operators_manual as FEM 
#import operators_auto_lin as FEM 
from PyHoltz.nlevp.all_algebra import pow0,pow1,pow2,tau_delay,ntau2, exp_a
import numpy as np
from scipy.sparse import csr_matrix
from PyHoltz.nlevp.parameter import ParaModel
from copy import deepcopy

def outer(aa,ii,bb,jj):
    #TODO: this would be much more efficient using numpy functionality and list comprehensions
    mm=[]
    II=[]
    JJ=[]    
    for a, i in zip(aa,ii):
        for b, j in zip(bb,jj):
            mm+=[a*b]
            II+=[i]
            JJ+=[j]
    mm=np.array(mm)        
    II=np.array(II)
    JJ=np.array(JJ)
    # for example (Jakob)
    # mm = np.outer(aa,bb).ravel()
    # II = (ii*np.ones((len(ii),len(ii)))).T.ravel()
    # JJ = (jj*np.ones((len(jj),len(jj)))).ravel()
    return mm, II, JJ

def extent(mm, ii, jj, npoints, peripoints, N):
    #TODO: virtualize this functionality
    n_peri=len(peripoints)
    n_cell_points=npoints - n_peri
    idx=np.r_[0:npoints]
    idx[0:n_peri]=idx[n_peri-1::-1]

    
    for k in range(N):
        II=ii+k*n_cell_points
        JJ=jj+k*n_cell_points        
        if k==N-1:
            II=np.mod(II,N*n_cell_points)
            JJ=np.mod(II,N*n_cell_points)
        II=II[idx]
        JJ=JJ[idx]                                        
        yield mm , II, JJ 
        
        

            
        
    
        
        
#def new_extent(mm, ii, jj, npoints, imgpoints, N):
#    n_img=len(imgpoints)
#    n_cell_points=npoints-n_img
#    print(n_cell_points,' ',np.max(ii) )
#    pmap = np.arange(npoints)
#    pmap[min(imgpoints)::] = np.flipud(pmap[min(imgpoints)::])
#    ii=pmap[ii]
#    jj=pmap[jj]        
#    for k in range(N):
#        II=ii+k*n_cell_points
#        JJ=jj+k*n_cell_points        
#        if k==N-1:
#            II=np.mod(II,N*n_cell_points)
#            JJ=np.mod(JJ,N*n_cell_points)                                    
#        yield mm , II, JJ, n_cell_points, k 
    
    
def new_extent(mm,ii,jj,msh):
    N=msh.DoS
    npoints=len(msh.Points)
    msh.build_domains('Bloch_img','Lines')
    n_cell_points=npoints-len(msh.Domains['Bloch_img']['nodes'])
    nDof=N*(n_cell_points+len(msh.Lines)-len(msh.Domains['Bloch_img']['Lines']))
    
    def get_new(dof,k):
        if dof <npoints:
            return (msh.pmap[dof]+k*n_cell_points)%(N*n_cell_points)
        else:
            dof-=npoints
            #print(dof,flush=True)
            return msh.line_extension_mapping[k][dof]+N*n_cell_points
            
    
    for k in range(N):
        II=np.zeros(mm.shape[0],dtype=int)
        JJ=np.zeros(mm.shape[0],dtype=int)

        for idx,(old_dofi,old_dofj) in enumerate(zip(ii,jj)):
            II[idx]=get_new(old_dofi,k)
            JJ[idx]=get_new(old_dofj,k)
        yield mm, II, JJ, nDof, k        

def blochify(mm,ii,jj,npoints, blochpoints, N, b = 0):
    mm = np.complex128(mm)
    imagepoints = npoints - blochpoints - 1     
    pmap = np.arange(npoints)
    pmap[imagepoints] = blochpoints    
    imagerows = np.in1d(ii,imagepoints)
    imagecols = np.in1d(jj,imagepoints)    
    mm[imagerows] = np.exp(1j*b*(2*np.pi/N))*mm[imagerows]
    mm[imagecols] = np.exp(-1j*b*(2*np.pi/N))*mm[imagecols]
    ii = (pmap[ii])  
    jj = (pmap[jj]) 
    npoints = npoints - max(blochpoints) - 1
    return mm, ii, jj, npoints

    
def old_blochify(mm,ii,jj,npoints,blochpoints, N, b=0):
    mm = np.complex128(mm)
    imagepoints = npoints - blochpoints - 1     
    pmap = np.arange(npoints)
    pmap[imagepoints] = blochpoints
    
    imagerows = np.in1d(ii,imagepoints)
    imagecols = np.in1d(jj,imagepoints)    
    
    mask=np.ones(mm.shape,dtype=bool)
    maskPlus=np.logical_and(imagerows, np.logical_not(imagecols) )
    maskMinus=np.logical_and(imagecols, np.logical_not(imagerows) )
    mask[imagerows]=False
    mask[imagecols]=False
    mask[np.logical_and(imagerows,imagecols)]=True
    mm0=mm[mask]
    ii0=pmap[ii[mask]]
    jj0=pmap[jj[mask]]
    npoints = npoints - max(blochpoints) - 1
                           
    yield mm0, ii0, jj0, npoints, [] 
    #TODO: Is this really necessary
    #ii0=pmap[ii0]
    #jj0=pmap[jj0]
    
    
    mmPlus=mm[maskPlus]
    iiPlus=pmap[ii[maskPlus]]
    jjPlus=pmap[jj[maskPlus]]
    yield mmPlus, iiPlus, jjPlus, npoints, [pow1]
    
    mmMinus=mm[maskMinus]
    iiMinus=pmap[ii[maskMinus]]
    jjMinus=pmap[jj[maskMinus]]
    pow1Minus= lambda z,o : pow1(np.conj(z),o)
    yield mmMinus, iiMinus, jjMinus, npoints, [pow1Minus]
        
def new_blochify(mm,ii,jj,npoints,dofmap, N):
    mm=np.complex128(mm)
    ndata=len(ii)
    mmPlus=np.zeros(ndata,dtype=complex)   
    mmMinus=np.zeros(ndata,dtype=complex)
    #print(ii,jj,type(ii),type(jj))
    for idx,(i,j) in enumerate(zip(ii,jj)):
        if np.in1d(i,dofmap[:,1]) and not np.in1d(j,dofmap[:,1]):
            mmPlus[idx]=mm[idx]
            mm[idx]=0
        elif not np.in1d(i,dofmap[:,1]) and np.in1d(j,dofmap[:,1]):
            mmMinus[idx]=mm[idx]
            mm[idx]=0
              
    for (new, old) in dofmap:
        ii[ii==old]=new
        jj[jj==old]=new

    idx=0    
    for i in range(npoints):
        ii[ii==i]=idx
        jj[jj==i]=idx
        if not i in  dofmap[:,1]: idx+=1
        
    npoints=npoints-len(dofmap)
    yield mm, ii, jj, npoints, []            
    #yield mmPlus, ii, jj, npoints, [pow1]
    yield mmPlus, ii, jj, npoints, [exp_a(+1j*2*np.pi/N)]
    #pow1Minus= lambda z,o : pow1(np.conj(z),o)
    yield mmMinus, ii, jj, npoints, [exp_a(-1j*2*np.pi/N)]#[pow1Minus]
    #yield mmMinus, ii, jj, npoints, [pow1Minus]
    
        
        
    


class discretization:
    '''
        Main class to organize the discretization, i.e., assembley of
        local matrices and creation of a parametrized linear operator object.
        Parameters
        ----------
        
        mesh : PyHoltz mesh object
                 computational grid to be used 
        
        
        physics : dict
               dictionary containing the specification of the operators 
               to be discretized, i.e, operator type, applied region, 
               and operator-specific additional data.
       
        c_field : np.array of length len(mesh.Tetrahedra)
               local speed of sound for each element.
               
        Bloch: bool
               If True Bloch-periodicity is invoked for boundaries labeled 
               "Bloch" and "Bloch_img", respectively.

    '''
    def __init__(self,mesh,physics,c_field,Bloch=False):
        self.mesh=mesh
        self.physics=physics
        self.operator_definition={}
        self.Bloch=Bloch
        self.c_field=c_field
   
    def discretize_operators(self):
        '''
           Compute the regional matrices and generate the parametrized 
           linear operator object.  
        '''
        
        if len(self.mesh.Lines)==0: 
            import PyHoltz.FEM.operators_auto_lin as FEM
        else:
            import PyHoltz.FEM.operators_auto_Quad as FEM
        
        LinOp=ParaModel()
        LinOp.add_param('omega',1) #eigenvalue is always present
 
        #c_field=self.c_field    
        #npoint=len(self.mesh.Points)
        nDoF=len(self.mesh.Points)+len(self.mesh.Lines)
        for domain in self.physics:
            dom_type=self.physics[domain][0] #TODO: implement that one domain can be tight to multiple operators            
            if dom_type in ['interior','flame','mass', 'stiffness','cold']: #TODO: move domain_build to last mesh step
                self.mesh.build_domains(domain,'Tetrahedra')
            if dom_type in ['impedance','boundary mass','half boundary']:
                self.mesh.build_domains(domain,'Triangles')
            else:
                continue
            
        #do actual discretization                
        for domain in self.physics:
            dom_type=self.physics[domain][0]
            params=self.physics[domain][2] #get parameter names
            print(domain)
            
            if dom_type=='interior':
                operators=['K','M']
                c_field=self.c_field
                
            elif dom_type=='cold':
                operators=['K','M']
                c_field=self.c_field.copy()
                c_field[:]=self.physics[domain][1]
                
            elif dom_type=='flame':
                operators=['Q']
                rho,gamma,Q02u0,x_ref,ref_vec,FTF,n,tau=self.physics[domain][1]
                self.mesh.compute_domain_volume(domain)
                V=self.mesh.Domains[domain]['volume']
                nlocal=Q02u0/V
                LinOp.add_param(params[0],n)
                LinOp.add_param(params[1],tau)
                flame_functions=[pow1,tau_delay] 
                flame_params=[[params[0]],['omega',params[1]]]
                
            elif dom_type=='measured flame':
                operators=['Q']
                rho,gamma,Q02u0,x_ref,ref_vec,FTF,n=self.physics[domain][1]
                self.mesh.compute_domain_volume(domain)
                V=self.mesh.Domains[domain]['volume']
                nlocal=Q02u0/V
                LinOp.add_param(params[0],n)
                flame_functions=[pow1,FTF]
                flame_params=[[params[0]],['omega']]
                

            elif dom_type=='impedance':
                operators=['C']
                Z=self.physics[domain][1]
                if Z==float('inf'):
                    #operators=[]
                    LinOp.add_param(params[0],0)
                elif Z==0:
                    #Z=np.finfo(float).eps
                    LinOp.add_param(params[0],float('inf'))
                else:
                    LinOp.add_param(params[0],1/Z)
                boundary_params=[params]
                boundary_functions=[pow1]
                c_field=self.c_field
                
            elif dom_type=='mass':
                operators=['M']

            elif dom_type=='stiffness':
                operators=['K']
                c_field=self.c_field
                
            elif dom_type=='boundary mass':
                operators=['C']
                boundary_params=[]
                boundary_functions=[]
                c_field=self.c_field                
            
            elif dom_type=='half boundary':
                operators=['H']
                half_params=[['b']]
                half_functions=[pow1]
                c_field=np.ones((len(self.c_field),1))
            
            else:
                #TODO: print('Error: domain type "',dom_type,'" not supported!')
                continue
            
            
            for operator in operators:
                if operator=='K':
                    mm,ii,jj=FEM.assemble_stiffness_operator(self.mesh.Points,c_field, self.mesh.get_e2p('Tetrahedra',domain),self.mesh.Lines)
                    functions=[]#[pow0]
                    params=[]#[['omega']]                   

                if operator=='M':
                    mm,ii,jj=FEM.assemble_mass_operator(self.mesh.Points, self.mesh.get_e2p('Tetrahedra',domain),self.mesh.Lines)
                    functions=[pow2]
                    params=[['omega']]

                if operator=='Q':
                    qq,qi,qj=FEM.assemble_volume_source(self.mesh.Points,self.mesh.get_e2p('Tetrahedra',domain),self.mesh.Lines)
                    gg,gi,gj=FEM.assemble_gradient_source(self.mesh.Points,self.mesh.Tetrahedra,x_ref,ref_vec,self.mesh.Lines)                    
                    gg=-(gamma-1)/rho*nlocal*gg #TODO: check sign
                    mm,ii,jj=outer(qq,qi,gg,gi)
                    functions=flame_functions#[pow1,tau_delay]                    
                    params=flame_params#[[params[0]],['omega',params[1]]]

                if operator=='C':
                    mm,ii,jj=FEM.assemble_boundary_mass_operator(self.mesh.Points, c_field, self.mesh.get_e2p('Triangles',domain),self.mesh.Lines)
                    mm=-mm*1j
                    functions=[pow1]+boundary_functions
                    params=[['omega']]+boundary_params
                
                if operator=='H':
                    mm,ii,jj=FEM.assemble_boundary_mass_operator(self.mesh.Points, c_field, self.mesh.get_e2p('Triangles',domain),self.mesh.Lines)
                    functions=[]+half_functions
                    params=[]+half_params
                    LinOp.add_param('b',0)       
                


                #Blochify #TODO: store unit cell discretization
                if self.Bloch=='Bloch':                
                    #mm,ii,jj,DoF=blochify(mm,ii,jj,npoint,blochpoints,self.mesh.DoS,self.Bloch)
                    
                    #blochpoints = self.mesh.Domains['Bloch']['nodes'].flatten()
                    #bloch_generator = old_blochify(mm,ii,jj,nDoF,blochpoints,self.mesh.DoS,1)
                    
                    self.mesh.get_dofmap()
                    bloch_generator = new_blochify(mm,ii,jj,nDoF,self.mesh.dofmap,self.mesh.DoS)
                    
                    LinOp.add_param('b',1)
                    for mm, ii, jj, DoF, new_func in bloch_generator:
                        # create sparse matrix
                        if not new_func == []: 
                            new_params = params + [['b']]
                        else: 
                            new_params = params
                        new_func=functions+new_func    
                        matrix = csr_matrix((mm, (ii, jj)), shape = (DoF,DoF),dtype=complex)#.toarray() #TODO: consider real matrices for storage...
                        matrix.eliminate_zeros()
                        LinOp.add_term({'matrix':matrix,'functions':new_func,'parameters':new_params,'operator':operator,'domain':domain}) 
                
                elif self.Bloch=='extent':
                    #img_points= self.mesh.Domains['Bloch_img']['nodes'].flatten()
                    #sector_generator = new_extent(mm, ii, jj, nDof, img_points, self.mesh.DoS)
                    
                    self.mesh.get_linextension()
                    sector_generator = new_extent(mm,ii,jj,self.mesh)
                    for mm, ii, jj, DoF, k in sector_generator:
                        #DoF=DoF*self.mesh.DoS
                        new_params=[[par+'#'+str(k) if par!= 'omega' else 'omega' for par in elm ] for elm in params]
                        matrix = csr_matrix((mm, (ii, jj)), shape = (DoF,DoF),dtype=complex)#.toarray() #TODO: consider real matrices for storage...
                        matrix.eliminate_zeros()
                        LinOp.add_term({'matrix':matrix,'functions':functions,'parameters':new_params,'operator':operator,'domain':domain+'#'+str(k)}) 

                else:
                    #DoF=npoint+len(self.mesh.Lines)
                    matrix = csr_matrix((mm, (ii, jj)), shape = (nDoF,nDoF),dtype=complex)#.toarray() #TODO: consider real matrices for storage...    
                    matrix.eliminate_zeros()
                    LinOp.add_term({'matrix':matrix,'functions':functions,'parameters':params,'operator':operator,'domain':domain}) 
                
        
        
        if self.Bloch=='extent':
            params=[]
            values=[]
            derivs=[] #TODO:check whether the deriv feature is still needed
            for (par,val,drv) in zip(LinOp.params,LinOp.values,LinOp.derivs):
                if par!='omega':
                    values+=[val]*self.mesh.DoS
                    params+=[par+'#'+str(i) for i in range(self.mesh.DoS)]
                    derivs+=[drv]*self.mesh.DoS
                else:
                    values+=[val]
                    params+=[par]
                    derivs+=[drv]
            LinOp.params=params
            LinOp.values=values 
            LinOp.derivs=derivs 
                
        return LinOp
                
    def initialize_regional_sources(self):
        '''
            compute load vector for forced response calculations . (experimental)
        '''
        c_field=self.c_field
        npoint=len(self.mesh.Points)
        for domain in self.physics:
            dom_type=self.physics[domain][0] #TODO: implement that one domain can be tight to multiple operators            
            if dom_type in ['volume_source']: #TODO: move domain_build to last mesh step, implement monopole
                self.mesh.build_domains(domain,'Tetrahedra')
            if dom_type in ['loudspeaker']:
                self.mesh.build_domains(domain,'Triangles')
            else:
                continue
            
        for domain in self.physics:
            dom_type=self.physics[domain][0]
            if dom_type=='loudspeaker':
                source,ii,jj=FEM.loudspeaker(self.mesh.Points,self.mesh.get_e2p('Triangles',domain),c_field[self.mesh.get_tri2tet(domain)])
                #source==-source*1j #TODO: check sign
            elif dom_type=='volume_source':
                source,ii,jj=FEM.assemble_volume_source(self.mesh.Points,self.mesh.get_e2p('Tetrahedra',domain),self.mesh.Lines)

                
        DoF=npoint+len(self.mesh.Lines)        
        #print('DoF: ',DoF,source,ii,jj,flush=True)
        matrix = csr_matrix((source, (ii, jj)), shape = (DoF,1),dtype=complex)
        self.source=matrix
                
                #TODO: implement Blochification
                #TODO: implement source paramodel
