#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
"""
Created on Mon Feb 27 11:00:13 2017

@author: georg
"""

import sys
import numpy as np

def cart2pol(x, y):
    '''
        Converts cartesian coordinates to polar coordinates.
        
        Parameters 
        ----------
        
        x,y : array
        
        Returns
        -------
        
        theta, rho : array
        
    '''
    theta = np.arctan2(y, x)
    rho = np.sqrt(x**2 + y**2)
    return (theta, rho)
    
def pol2cart(theta, rho):
    x = rho*np.cos(theta)
    y = rho*np.sin(theta)
    return x,y 

def tet_volume(tet):
        x = tet[:,0]
        y = tet[:,1]
        z = tet[:,2]
        
        # Jacobian Matrix
        J = np.matrix([\
        [x[1]-x[0], x[2]-x[0], x[3]-x[0]], \
        [y[1]-y[0], y[2]-y[0], y[3]-y[0]], \
        [z[1]-z[0], z[2]-z[0], z[3]-z[0]]])
        # Compute Determinant of Jacobian
        edet = np.linalg.det(J)
        edet=abs(edet)
        V=edet/6
        return V

def tet_info(tet):
        x = tet[:,0]
        y = tet[:,1]
        z = tet[:,2]
        
        # Jacobian Matrix
        J = np.matrix([\
        [x[1]-x[0], x[2]-x[0], x[3]-x[0]], \
        [y[1]-y[0], y[2]-y[0], y[3]-y[0]], \
        [z[1]-z[0], z[2]-z[0], z[3]-z[0]]])
        # Compute Determinant of Jacobian
        edet = np.linalg.det(J)
        cond=np.linalg.cond(J)
        edet=abs(edet)
        V=edet/6
        return V,cond
    
def gen_index(log_table):
    return np.r_[0:len(log_table)][log_table]
#%%
def find_tetrahedron_containing_x(nodes, tetrahedra, x_point):
    x = nodes[:,0]
    y = nodes[:,1]
    z = nodes[:,2]
    
    for idx,tet in enumerate(tetrahedra):
        #Jacobian of local coordinate transformation        
        J = np.matrix([
            [x[tet[1]]-x[tet[0]], x[tet[2]]-x[tet[0]], x[tet[3]]-x[tet[0]]], 
            [y[tet[1]]-y[tet[0]], y[tet[2]]-y[tet[0]], y[tet[3]]-y[tet[0]]],
            [z[tet[1]]-z[tet[0]], z[tet[2]]-z[tet[0]], z[tet[3]]-z[tet[0]]]])
        
        Xi=J.I*np.matrix([[x_point[0]-x[tet[0]]],[x_point[1]-y[tet[0]]],[x_point[2]-z[tet[0]]]])
        
        #barycentric coordinates 
        Xi=np.vstack((1-sum(Xi),Xi))
        inside=True
        for xi in Xi:
            inside &= (0 <= xi.item(0)<= 1)
        
        if inside: return idx
        
    return 'no tetrahedron contains x= '+ str(x_point)
        
    
#%%
def smplx_compare(A,B):
    '''
        Simple routine to compare two simplices A and B.
    '''
    if len(B)!=len(A): sys.exit('Error: lists do not share same length!') 
    cmpr=0
    for a,b in zip(A[::-1],B[::-1]):
        if a!=b: 
            if a<b:
                cmpr=1
                break
            else:
                cmpr=-1
                break
    return cmpr        

#%%
def smplx_sorter(new_smplx,list_of_smplxs):
    '''
        Sorts simplices.
    '''
    #utilize divide an counquer strategy to find a simplex in ordered list of simpleces
    smplx=np.sort(new_smplx)
    smplx_idx=None
    list_length=list_of_smplxs.shape[0]
    if list_length>=2:
        low=0
        high=list_length-1
        
        #check whether smplx is in list range
        # lower bound
        cmpr=smplx_compare(list_of_smplxs[low],smplx)
        if cmpr<0:
            list_of_smplxs=np.insert(list_of_smplxs,low,smplx,axis=0)
            smplx_idx=low
        elif cmpr==0:
            smplx_idx=low

        #upper bound
            
        cmpr=smplx_compare(smplx,list_of_smplxs[high])
        if cmpr<0:
            list_of_smplxs=np.insert(list_of_smplxs,high+1,smplx,axis=0)
            smplx_idx=high+1
        elif cmpr==0:
            smplx_idx=high    
            
        if smplx_idx==None:
            #divide et impera
            while high-low>1:
                mid=round((high+low)/2)
                
                cmpr=smplx_compare(list_of_smplxs[mid],smplx)
                if cmpr>0:
                    low=mid
                elif cmpr<0:
                    high=mid
                else:
                    smplx_idx=mid
                    break

                
        if smplx_idx==None:
            #find location from last two possibilities
            cmpr_low=smplx_compare(list_of_smplxs[low],smplx)
            cmpr_high=smplx_compare(smplx,list_of_smplxs[high])
            if cmpr_low==0:
                smplx_idx=low
            elif cmpr_high==0:
                smplx_idx=high
            else:
                smplx_idx=high
                list_of_smplxs=np.insert(list_of_smplxs,smplx_idx,smplx,axis=0)
                    
                   
                                
    elif list_length==1:
        
        cmpr=smplx_compare(list_of_smplxs[0],smplx)
        if cmpr<0:
            smplx_idx=0
            list_of_smplxs=np.insert(list_of_smplxs,smplx_idx,smplx,axis=0)
        elif cmpr>0:
            smplx_idx=1
            list_of_smplxs=np.insert(list_of_smplxs,smplx_idx,smplx,axis=0)
        else:
            smplx_idx=0
            
    
    elif list_length==0:
        list_of_smplxs=np.insert(list_of_smplxs,0,smplx,axis=0)
    
    #return smplx_idx, list_of_smplxs
    return list_of_smplxs

#%%
def smplx_finder(new_smplx,list_of_smplxs): #TODO: merge with smplx_sorter
    #utilize divide an counquer strategy to find a simplex in ordered list of simpleces
    smplx=np.sort(new_smplx)
    smplx_idx=None
    list_length=list_of_smplxs.shape[0]
    if list_length>=2:
        low=0
        high=list_length-1
        
        #check whether smplx is in list range
        # lower bound
        cmpr=smplx_compare(list_of_smplxs[low],smplx)
        if cmpr<0:
            #list_of_smplxs=np.insert(list_of_smplxs,low,smplx,axis=0)
            #smplx_idx=low
            pass
        elif cmpr==0:
            smplx_idx=low

        #upper bound
            
        cmpr=smplx_compare(smplx,list_of_smplxs[high])
        if cmpr<0:
            #list_of_smplxs=np.insert(list_of_smplxs,high+1,smplx,axis=0)
            #smplx_idx=high+1
            pass
        elif cmpr==0:
            smplx_idx=high    
            
        if smplx_idx==None:
            #divide et impera
            while high-low>1:
                mid=round((high+low)/2)
                
                cmpr=smplx_compare(list_of_smplxs[mid],smplx)
                if cmpr>0:
                    low=mid
                elif cmpr<0:
                    high=mid
                else:
                    smplx_idx=mid
                    break

                
        if smplx_idx==None:
            #find location from last two possibilities
            cmpr_low=smplx_compare(list_of_smplxs[low],smplx)
            cmpr_high=smplx_compare(smplx,list_of_smplxs[high])
            if cmpr_low==0:
                smplx_idx=low
            elif cmpr_high==0:
                smplx_idx=high
            else:
                pass  
                #smplx_idx=high
                #list_of_smplxs=np.insert(list_of_smplxs,smplx_idx,smplx,axis=0)
               
                   
                                
    elif list_length==1:
        
        cmpr=smplx_compare(list_of_smplxs[0],smplx)
        if cmpr<0:
            #smplx_idx=0
            #list_of_smplxs=np.insert(list_of_smplxs,smplx_idx,smplx,axis=0)
            pass
        elif cmpr>0:
            pass
            #smplx_idx=1
            #list_of_smplxs=np.insert(list_of_smplxs,smplx_idx,smplx,axis=0)
        else:
            smplx_idx=0
            
    
    elif list_length==0:
        #list_of_smplxs=np.insert(list_of_smplxs,0,smplx,axis=0)
        pass
        #return smplx_idx, list_of_smplxs
    return smplx_idx



#%% unit tests
#%%
#list_of_lines=np.empty((0,2),dtype=int)
##print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([1,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([.5,1],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,1],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,2],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,1.5],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,-1.5],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,15],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([2,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([1,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)    
##%%
##%%
#list_of_lines=np.empty((0,3),dtype=int)
##print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([1,0,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([.5,1,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,1,1],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,2,9],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,1.5,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,-1.5,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([0,15,8],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([2,0,25],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)
#line_idx,list_of_lines=smplx_sorter(np.array([1,0,0],dtype=int),list_of_lines)    
#print(list_of_lines,line_idx)    
