#_____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
# Last edited by: 
#
# Date:
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
# ____________________________________________________________________

"""
Created on Fri Feb 24 10:59:46 2017

@author: georg
"""


import sys
import numpy as np
import time 
import math

from os.path import exists
from PyHoltz.FEM.mesh_utils import smplx_sorter, smplx_finder, cart2pol, pol2cart, gen_index 
from copy import deepcopy



class mesh:
    '''
        Main class for handeling meshes.
        
        Examples
        -------
        M = PyHoltz.FEM.mesh_class.mesh(myMesh.msh)
            Reads points, tetrahedras and domains from myMesh.msh file.
        M.collect_lines()
          Calls :func:`collect_lines` to collect the lines. If lines are added
          FEM second order is used.
        
        
        Parameter
        --------
        filename : string,optional
                   Function calls :func:`read_cfx5` or :func:`read_cfx5` 
                   depending on the ending of filename.
        
        
        Attributes
        ----------
        Name : string
               filename
               
        Points : int
                 Number of points in mesh.
        
        Lines : int
                Number of lines in mesh.
                
        Terahedra : int
                Number of tetrahedras in mesh.
        
        Domains : int
                  Number of physical domain in mesh.
        
       
    '''
    def __init__(self,file_name=None):
        '''
            Chooses the import routine msh(GMSH/cfx5) depending on
            the ending of the filename.
        '''
        self.Name=file_name
        if self.Name!=None:
            if file_name[-3:]=='msh':
                self.read_msh(file_name)
            elif file_name[-4:]=='cfx5':
                self.read_cfx5(file_name)
                
        
    def __str__(self):
        
        out='PyHoltz mesh object'
        out+='\n---------------------------------------------'
        out+='\nName:\t\t\t'+self.Name
        out+='\nPoints:\t\t\t'+str(len(self.Points))
        out+='\nLines:\t\t\t'+str(len(self.Lines))
        out+='\nTetrahedra:\t\t'+str(len(self.Tetrahedra))
        out+='\nDomains:\t\t\t'+str(len(self.Domains))
        out+='\n'
        for domain in self.Domains.keys():
            out+=domain+', '
        return out
        

    def __repr__(self):
        return self.__str__()
        
        
    def build_domains(self,domain,geom):    
        '''
            Assembles the connectivity (e2p) for a domain (geometric topology). Either Triangles or
            Tetrahedra. The connectivity is added as an entry to the dictionary-entry of the domain.
            
            Parameters
            ----------
            
            domain : str
                     Tag of the domain to be assembled. 
            
            
            geom : str
                   The geometric type to be assembled. Either 'Triangles' or 'Tetrahedra'.
        '''           
        elements = []
        for idx,tet in enumerate(self.__dict__[geom]):
            if np.all(np.in1d(tet,self.Domains[domain]['nodes'])):
                elements.append(idx)
        self.Domains[domain].update({geom:np.array(elements)})
        
        
    def compute_domain_volume(self,domain):
        '''
            Computes the volume of a domain and add it as an entry to the dictionary-entry of the domain.
            
            Parameters
            ----------
            
            domain : str
                     Tag of the domain.
        
        '''
        self.build_domains(domain,'Tetrahedra')
        e2p=self.Tetrahedra[self.Domains[domain]['Tetrahedra']]
         # 0. Get Properties
        x = self.Points[:,0]
        y = self.Points[:,1]
        z = self.Points[:,2]
        nelement = len(e2p[:,1])
        
        #1. initialize volume
        V=0
    
        #2. compute flame volume
        for k in range(0,nelement):
            # Jacobian Matrix
            J = np.matrix([\
            [x[e2p[k,1]]-x[e2p[k,0]], x[e2p[k,2]]-x[e2p[k,0]], x[e2p[k,3]]-x[e2p[k,0]]], \
            [y[e2p[k,1]]-y[e2p[k,0]], y[e2p[k,2]]-y[e2p[k,0]], y[e2p[k,3]]-y[e2p[k,0]]], \
            [z[e2p[k,1]]-z[e2p[k,0]], z[e2p[k,2]]-z[e2p[k,0]], z[e2p[k,3]]-z[e2p[k,0]]]])
           
            # Compute Determinant of Jacobian
            edet = np.linalg.det(J)
            edet=abs(edet)
            V+=edet/6                                
        print("... Volume complete!",V)
        self.Domains[domain].update({'volume':V})        
        
        
    def get_e2p(self, geom, domain):
        '''
            Returns the e2p of a specific domain.
            
            Parameters
            ----------
            
            domain : str
                     Tag of the domain to be assembled. 
            
            
            geom : str
                   The geometric type to be assembled. Either 'Triangles' or 'Tetrahedra'.
                    
        '''
        return getattr(self,geom)[self.Domains[domain][geom]]
        
        
    def get_tri2tet(self,domain):        
        self.build_domains(domain,'Triangles') #TODO: cache domain builts
        boundary=self.get_e2p('Triangles',domain)#self.Triangles[self.Domains[domain]['Triangles']] 
        #bnodes=np.unique(boundary) #all nodes associated to the boundary
        bnodes=self.Domains[domain]['nodes']
        #print(bnodes)
        tet_idx=np.in1d(self.Tetrahedra, bnodes).reshape(self.Tetrahedra.shape)
        tet_idx=tet_idx.sum(1)==3 
        tet_idx=np.r_[0:len(tet_idx)][tet_idx]#translate logical to integer indexing        
        tri2tet=np.empty(len(tet_idx),dtype=int)
        #print(tet_idx)
        for i,triangle in enumerate(boundary):
            j=0
            while len(tet_idx)!=0:
        
                if np.in1d(triangle,self.Tetrahedra[tet_idx[j]]).sum()==3:
                    tri2tet[i]=tet_idx[j]
                    mask = np.ones(len(tet_idx), dtype=bool)
                    mask[j]=False
                    tet_idx=tet_idx[mask] #something is wrong with np.delete therfore this verbose boolean deletion
                    j=0
                    break
                else:
                    j+=1 
                #possibly some 'else' stuff...
        return tri2tet #nest it in object instead of return?

        ##unit testing...
        #completed=True
        #for i in range(len(tri2tet)):
        #    local_test = np.in1d(boundary[i],S3[tri2tet[i]]).sum()==3
        #    local_test = completed and local_test
        #    print(i,local_test)
        #print('completed :',completed)
    
    def get_tri(self,domain):
        bnodes=self.Domains[domain]['nodes']
        tet_idx=np.in1d(self.Tetrahedra,bnodes).reshape(self.Tetrahedra.shape)
        tet_idx[np.invert(tet_idx.sum(1)==3)]=False
        triangles=self.Tetrahedra[tet_idx]
        triangles=triangles.reshape(len(triangles)/3,3)
        return triangles
        
        
    def find_tetrahedra_containing_point(self,point,info=False):
        '''
            Returns the tetrahedra containing a specified point.
            
            Parameters
            ----------
            
            point : array
                    
            info : boolean, optional
                    
        '''

        # 
        # TBD: has to become more responsive to failures
        # implement to check for points on surfaces (define a tolerance value?)

        print('Hmmmm',flush=True)
        for (idx,tet) in enumerate(self.Tetrahedra):
            J=np.matrix(self.Points[tet[1:4],:]-self.Points[tet[0],:]).T
            
            bary=J.I*(point-self.Points[tet[0],:]).T
            bary=np.vstack(([1-np.sum(bary)],bary))
            if np.all(np.logical_and(bary>=0,  bary<=1)):
                if info:
                    print('Tetrahedron: ',idx)
                    print('Barycentric coordinates: ',bary)
                    print('Coordinates: ',self.Points[tet])
                return idx

        return 'fail'#this is a sentinel value, if not overwritten it should result in an error becaouse string cannot used as an index
        
        
    def mirror(self):
        ''' 
            Function to mirror the coordinates. Receives the coordinates and sorts them s.t.
            the Bloch-Points are the first, followed by the interior points and lastly the symmetry points.
            What is physically 'Bloch_ref' is instead just called 'Bloch'.   
        '''
        npoint = len(self.Points)
        points = np.arange(npoint)
        
        blochpoints = np.unique(self.Domains['Bloch']['nodes'])
        coordbloch  = self.Points[blochpoints,:]
        
        sympoints   = np.unique(self.Domains['Symmetry']['nodes'])
        coordsym    = self.Points[sympoints,:]
            
        Nodes = np.delete(self.Points, np.hstack((blochpoints,sympoints)), axis = 0)
        mirroredHalf = np.vstack((np.flipud(Nodes),np.flipud(coordbloch)))   
        mirroredHalf[:,1] = -mirroredHalf[:,1]
        Nodes = np.vstack((coordbloch,Nodes,coordsym,mirroredHalf))
        self.Points=Nodes
        points = np.delete(points, np.hstack((blochpoints,sympoints)), axis = 0)
        points = np.hstack((blochpoints,points,sympoints))

        #mapping to relabel all points
        pmap = np.zeros(npoint,dtype=int) 
        for i in range(npoint):
            pmap[points[i]] = i
        #relabel all points in tetrahedra and triangle definitions
        self.Tetrahedra=pmap[self.Tetrahedra]
        self.Triangles=pmap[self.Triangles]        
        
        #relabel in Domain definitions
        for key in self.Domains:
            self.Domains[key]['nodes'] = pmap[self.Domains[key]['nodes']] #TODO: check dimension consitency
            
        sympoints = pmap[sympoints]
        blochpoints = pmap[blochpoints]
        
        pmap = np.zeros(npoint,dtype=int)
        for i in range(npoint):
            if i in sympoints:
                pmap[i] = i
            else:
                pmap[i] = 2*npoint - len(sympoints) - i - 1
                
        #self.Tetrahedra=np.vstack((self.Tetrahedra,pmap[self.Tetrahedra])) fast but unsorted
        for smplx in pmap[self.Tetrahedra]:
            self.Tetrahedra=smplx_sorter(smplx,self.Tetrahedra)
            
        for smplx in pmap[self.Triangles]:
            self.Triangles=smplx_sorter(smplx,self.Triangles)
            
        
        
        for dom in list(self.Domains):
            print(dom)
            self.Domains[dom+'_img']={'dimension':self.Domains[dom]['dimension'],'nodes':pmap[self.Domains[dom]['nodes']]}
        print('... mirroring to full unit cell complete!')
        
        #determine degree of symmetry N        
        left=self.Points[self.Domains['Bloch']['nodes'][0]]
        right=self.Points[self.Domains['Bloch_img']['nodes'][0]]
        phi_l,r_l=cart2pol(left[0][0],left[0][1])
        phi_r,r_r=cart2pol(right[0][0],right[0][1])                
        phi=abs(phi_l-phi_r)
        print(phi)        
        if phi>np.pi:
            phi=2*np.pi-phi
        if phi !=0:    
            self.DoS=int(round(2*np.pi/phi))
        else:
            self.DoS=float('infinity')
        print('Degree of Symmetry is ',self.DoS,'.')
            # We need to ensure mathematical positive orientae2pMirror = {}tion of the elements
#            if (np.size(e2p[key],1)==3):
#                e2pMirror[key + '_img'] = np.empty_like(e2p[key])
#                e2pMirror[key + '_img'][:,0] = pmap[(e2p[key][:,2]).astype(int)]
#                e2pMirror[key + '_img'][:,1] = pmap[(e2p[key][:,1]).astype(int)]
#                e2pMirror[key + '_img'][:,2] = pmap[(e2p[key][:,0]).astype(int)]
#            elif (np.size(e2p[key],1)==4):
#                e2pMirror[key + '_img'] =  np.roll(pmap[(e2p[key]).astype(int)],1,axis=1) # Circular shift for tetrahedrons
#            else:
#                sys.exit('ERROR: Corrupted Data in e2p-dictionary!')        
#        return coord, e2p, e2pMirror
        
    ###############################################################################
    def rotate(self):
        ''' 
            Function to rotate a unit cell mesh to obtain a full annular cobustion chamber    
        '''
        
#        #determine degree of symmetry N        
#        left=self.Points[self.Domains['Bloch']['nodes'][0]]
#        right=self.Points[self.Domains['Bloch_img']['nodes'][0]]
#        phi_l,r_l=cart2pol(left[0][0],left[0][1])
#        phi_r,r_r=cart2pol(right[0][0],right[0][1])                
#        phi=abs(phi_l-phi_r)        
#        if phi>np.pi:
#            phi=2*np.pi-phi                        
#        N=int(round(2*np.pi/phi))
        N=self.DoS
        
        npoints = len(self.Points)
        blochpoints = np.unique(self.Domains['Bloch_img']['nodes'])
        self.Points = self.Points[0:min(blochpoints),:]
    
        pmap = np.arange(npoints)
        pmap[min(blochpoints)::] = np.flipud(pmap[min(blochpoints)::])
        
        self.Tetrahedra=pmap[self.Tetrahedra]
        self.Triangles=pmap[self.Triangles]
                        
        #for key in e2p:
        #    e2p[key] = pmap[e2p[key]]
        
        theta, rho = cart2pol(self.Points[:,0], self.Points[:,1])
        dtheta = (2*np.pi)/N
        z = self.Points[:,2]
        
        originalDomains = list(self.Domains.keys())
        originalTet=self.Tetrahedra
        originalTri=self.Triangles
        npoints = len(self.Points)
        for i in range(1,N):
            x,y=pol2cart(theta+i*dtheta,rho)
            nextSector = np.transpose(np.vstack((x,y,z)))
            self.Points = np.vstack((self.Points,nextSector))
            for smplx in np.mod(originalTri+i*npoints,npoints*N):
                self.Triangles=smplx_sorter(smplx,self.Triangles)
            for smplx in np.mod(originalTet+i*npoints,npoints*N):
                self.Tetrahedra=smplx_sorter(smplx,self.Tetrahedra)
            for dom in originalDomains:
                self.Domains[dom + str(i)] ={'dimension':self.Domains[dom]['dimension'],'nodes':self.Domains[dom]['nodes'] + i*npoints}
        #fix last boundary to first:
        for dom in originalDomains:
            self.Domains[dom + str(N-1)]['nodes'] = np.mod(self.Domains[dom + str(N-1)]['nodes'],npoints*N)
        
        print('... rotation to full annular chamber complete!')
        
    





    def union(self,name,doms):
        self.Domains[name]={'dimension':self.Domains[doms[0]]['dimension'],
                'nodes':np.expand_dims(np.unique(np.vstack(tuple(self.Domains[i]['nodes']  for i in doms))),axis=1)
                }
        
    def merge(self,domA,domB):
        ''' 
            Merge domain A with domain B. Domain A will contain all points from domain B. Domain B will be deleted.       
            
            Parameters
            ----------
            
            domA : str
            domaB : str
        '''            
        if self.Domains[domA]['dimension']!=self.Domains[domB]['dimension']:
            sys.error('Domains have diffrent dimension and cannot be fused.' )
        self.Domains[domA]['nodes']=np.expand_dims(np.unique(np.vstack((self.Domains[domA]['nodes'],self.Domains[domB]['nodes']))), axis=1)
        del self.Domains[domB]
        
        
    def merge_with_img(self,list_of_doms):
        '''
            Following a mirroring procedure for all domains ``dom`` a corresponding mirrored counterpart
            ``dom_img`` exists. In this routine these two are merged into one domain ``dom``
            
            Parameters
            ----------
            
            list_of_doms : list
                           The domains which are merged with there corresponding ``_img`` counterparts.
        '''
        for dom in list_of_doms:
            self.merge(dom,dom+'_img')
            
            
    def keep(self,list_of_doms):
        '''
            For the un-cluttering of the data, deletes all not-specified domains. In the mesh
            construction there are usually more domains specified, than later on used in the 
            simulation.
            
            Parameters
            ----------
            
            list_of_domains : list
                              The domains that are kept, all other are deleted.
        '''
        for dom in list(self.Domains):
            if not dom in list_of_doms:
                del self.Domains[dom]
    
    def read_cfx5(self,file_name):
        '''        
        Imports a mesh-file, assuming ANSYS' .cfx5 format.
            
            Parameters
            ----------
            
            file_name : str
                        The filename (including path) of the mesh for import.
        '''
        print('start mesh import...')
        lines=np.empty((0,2),dtype=int) #TODO: populate this field
        triangles=np.empty((0,3),dtype=int)
        tetrahedra=np.empty((0,4),dtype=int)
        domains={}
        delmap=[3,2,0,1]
        if exists(file_name):
            fid = open(file_name,"r")
            end_point=0
            end_tet=0
            coord=[]
            tets=[]
            fields={}
            for num, line in enumerate(fid,1):
               #print(str(num)+': '+line)
                #print(num,end_point)
                #time.sleep(0.25)
                if num==3:
                    #print(line)
                    split=line.split(' ')
                    npoint=int(split[0])
                    ntet=int(split[1])
                    nvol=int(split[5])
                    nbound=int(split[6]) 
                    end_point=3+npoint
                    end_tet=3+npoint+ntet
                    continue
                
                if num<=end_point:
                    split=line.rstrip().split(' ')
                    coord.append([float(split[0]),float(split[1]),float(split[2])])
                    #print(split)
                if num==end_point:
                    pass
                    #self.olafs_coord=np.array(coord)
                if num>end_point and num<=end_tet:
                    split=line.strip().split(' ')
                    #Todo: use smplx_sorter directly
                    tet=[int(split[0])-1,int(split[1])-1,int(split[2])-1,int(split[3])-1]
                    tets.append(tet)
                    #tetrahedra=smplx_sorter(tet,tetrahedra)
                if num==end_tet:
                    pass
                    #self.olafs_tets=np.array(tets)
                    #self.olafs_tets=np.array(tets)
                if num>=end_tet+1 and num>3:
                    split=line.strip().split(' ')
                    #print(split[1],flush=True)
                    if split[1][0].isalpha(): #check whether line is field header
                        field=split[1]
                        fields.update({field:[int(split[0]),[]]})
                        if nvol!=-1: 
                            nvol-=1
                            dim=3
                        else:
                            dim=2
                        
                        domains[field]={'nodes':np.empty((0,1),dtype=int),'dimension':dim } #TODO: check domain format
                            
                            
                    else:
                        if nvol!=-1:
                            for entry in split:
                                new_nodes=tets[int(entry)-1]
                                for node in new_nodes:
                                    domains[field]['nodes']=smplx_sorter([node],domains[field]['nodes'])
                        else:
                            tetras=split[0::2]
                            faces=split[1::2]
                            for tet, face in zip(tetras,faces):
                                new_nodes=tets[int(tet)-1].copy()
                                #print('domain: ',field,'len: ',len(new_nodes), 'face', int(face)-1,flush=True)
                                del new_nodes[delmap[int(face)-1]]
                                triangles=smplx_sorter(new_nodes,triangles)
                                for node in new_nodes:
                                    domains[field]['nodes']=smplx_sorter([node],domains[field]['nodes'])
                                
                                
                            
#                        for entry in split:
#                            #print('entry : ',entry)
#                            fields[field][1].append(int(entry)-1)
                            
#            self.olafs_fields=fields
#            
#            for dom, data in fields.items():
#                if len(data[1])==data[0]:
#                    nodes=np.empty((0,1),dtype=int) #TODO: check domain format
#                    while data[1]!=[]:
#                        new_nodes=tets[data[1].pop()]
#                        #print('Hier: ', dom, data,new_nodes)
#                        for node in new_nodes:
#                            nodes=smplx_sorter([node],nodes)
#                    domains[dom]={'nodes':nodes,'dimension':3}
#                elif len(data[1])==2*data[0]:
#                    nodes=np.empty((0,1),dtype=int)
#                    while data[1]!=[]:
#                        face=data[1].pop()
#                        new_nodes=tets[data[1].pop()].copy()
#                        #print('domain: ',dom,'face: ',face,'len: ', len(new_nodes))
#                        del new_nodes[face]
#                        
#                        for node in new_nodes:
#                            nodes=smplx_sorter([node],nodes)
#                    domains[dom]={'nodes':nodes,'dimension':3}
                    
            for tet in tets:
                tetrahedra=smplx_sorter(tet,tetrahedra)
                        
            self.Points=np.array(coord)
            self.Lines=lines
            self.Triangles=triangles
            self.Tetrahedra=tetrahedra
            self.Domains=domains         
            print('Mesh import completed!')
                        
                    
                    
                    
                    

    
    def read_msh(self,file_name):
        '''
            Imports a mesh-file, assuming GMSH's native .msh format.
            
            Parameters
            ----------
            
            file_name : str
                        The filename (including path) of the mesh for import.
        '''
        if exists(file_name):
            # 1. Open file and Build data array (necessary for multiple iterations)
            fid = open(file_name,"r")
            keywords = ["$PhysicalNames", "$Nodes", "$Elements"]       
            data = []
            for num, line in enumerate(fid,1):
                data.append(line.strip('"').split())
                
            # 2. Import Physical Names
            key = keywords[0]
            #print(key,data)
        #        print('length',len(data))
        #        print(data[0:10])        
            for i in range(0,len(data)):
               if data[i][0] == key:
                   hit = i
            nphysical = int(data[hit+1][0])
            physical = []
            for i in range(2+hit,hit+nphysical+2):
                physical.append(data[i])
                physical[-1][0]=int(physical[-1][0])# convert dimension to integer
                physical[-1][1]=int(physical[-1][1]) #convert index to integer
                physical[-1][2]=physical[-1][2].strip('"') #remove double quotes from tag
        
            
            # 3. Import the coordinates
            key = keywords[1]
            for i in range(0,len(data)):
               if data[i][0] == key:
                   hit = i
            npoint = int(data[hit+1][0])
            coord = []
            for i in range(2+hit,hit+npoint+2):
                coord.append(list(map(float,data[i][1:])))             
            coord = np.array(coord) # as numpy array
            
            # 4. Import Elements
            key = keywords[2]
            for i in range(0,len(data)):
               if data[i][0] == key:
                   hit = i
            nelement = int(data[hit+1][0])
            elements = []
            for i in range(2+hit,hit+nelement+2):
                elements.append(list(map(int,data[i][1:])))
            elements = np.array(elements)
            
            # 5. Close file
            fid.close()
            
            # 6. Print Summary
            print("Physical Entities (Surfaces/Volumes): %i. Points (DOF): %i. Elements: %i." %(nphysical,npoint,nelement))
                   
                           
        else:
            sys.exit("ERROR getMesh: file not found!")
           
           
        #parse msh2-elements
           
        lines=np.empty((0,2),dtype=int)
        triangles=np.empty((0,3),dtype=int)
        tetrahedra=np.empty((0,4),dtype=int)
        domains={}
        tag_map={}
        for domain in physical:
            domains[domain[2]]={'dimension':domain[0],'nodes':np.empty((0,1),dtype=int)}
            tag_map[domain[1]]=domain[2]
            
        
        for element in elements:
            element_type=element[0]
            number_of_tags=element[1]
            tags=element[2:2+number_of_tags]
            node_number_list=np.array(element[2+number_of_tags:])-1 #shift indexing by one because python starts with 0
            
            if element_type==1: #2 node line
                if len(node_number_list)!=2: sys.exit('ERROR: The element has', len(node_number_list),'nodes which is supposed to be 2 in case of a line!' )
                lines=smplx_sorter(node_number_list,lines)             
            elif element_type==2: #3 node triangle
                if len(node_number_list)!=3: sys.exit('ERROR: The element has', len(node_number_list),'nodes which is supposed to be 3 in case of a triangle!' )
                triangles=smplx_sorter(node_number_list,triangles)
                       
        
                
            elif element_type==4: #four node tetrahedron
                tetrahedra=smplx_sorter(node_number_list,tetrahedra)
                
                
        
            elif element_type==15: #one node point
                pass
            else:
                sys.exit('ERROR : Mesh_read does not support element type', element_type,'!' )
            
            for node in node_number_list:
                domains[tag_map[tags[0]]]['nodes']=smplx_sorter(np.array([node]),domains[tag_map[tags[0]]]['nodes'])
        
        self.Points=coord
        self.Lines=lines
        self.Triangles=triangles
        self.Tetrahedra=tetrahedra
        self.Domains=domains        
    
        print("... mesh import completed!")
        
    
    def register(self,nodes,tets,domains,name):
        '''
            
        '''
        self.Points=nodes
        self.Name=name
        #self.Tetrahedra=tets
        #self.Triangles=tri
        self.Domains=domains
        
        #lines=np.empty((0,2),dtype=int)
        self.Triangles=np.empty((0,3),dtype=int)
        self.Tetrahedra=np.empty((0,4),dtype=int)        
        for tet in tets:
            self.Tetrahedra=smplx_sorter(tet,self.Tetrahedra)
            
        for dom in domains:
            if domains[dom]['dimension']==2:
                #triangles=np.in1d(self.Tetrahedra,domains[dom]['nodes']).reshape(len(self.Tetrahedra),4)#.sum(1)
                #idx=triangles.sum(1)==3
                #idx=gen_index(idx)
                #for tet,msk in zip(tets[idx,:],triangles[idx]):
                #    self.Triangles=smplx_sorter(tet[msk],self.Triangles)
                triangles=self.get_tri(dom)
                for tri in triangles:
                    self.Triangles=smplx_sorter(tri,self.Triangles)
                    
    def collect_lines(self):
        ''' Function collects lines between all points. If lines are added
            second order FEM is used. Only call this function if second order 
            FEM is required.
        '''
        lines=np.empty((0,2),dtype=int)
        for tet in self.Tetrahedra:
            lines=smplx_sorter(tet[[0,1]],lines)
            lines=smplx_sorter(tet[[0,2]],lines)
            lines=smplx_sorter(tet[[0,3]],lines)
            lines=smplx_sorter(tet[[1,2]],lines)
            lines=smplx_sorter(tet[[1,3]],lines)
            lines=smplx_sorter(tet[[2,3]],lines)            
        self.Lines=lines
        
    def get_dofmap(self):
        bloch_nodes=np.squeeze(self.Domains['Bloch']['nodes'])
        image_nodes=np.squeeze(self.Domains['Bloch_img']['nodes'])
        Npoints=len(self.Points)
        dofmap=[]
        for idx,img_idx in zip(bloch_nodes,image_nodes):
            dofmap+=[[idx,img_idx]]
        if len(self.Lines):
            for idx,line in enumerate(self.Lines):
                if np.all(np.in1d(line,bloch_nodes)):
                    img_line=Npoints-1-line
                    img_idx=smplx_finder(img_line,self.Lines)
                    dofmap+=[[idx+Npoints,img_idx+Npoints]]
        self.dofmap=np.array(dofmap)



    def get_pmap(self):      
        npoints = len(self.Points)
        blochpoints = np.unique(self.Domains['Bloch_img']['nodes'])
        #self.Points = self.Points[0:min(blochpoints),:]
    
        pmap = np.arange(npoints)
        pmap[min(blochpoints)::] = np.flipud(pmap[min(blochpoints)::])
        self.pmap=pmap


    def get_linextension(self):
        self.get_pmap()
        if self.Lines==[]:
            return
        
        N=self.DoS
        npoints=len(self.Points)-len(self.Domains['Bloch_img']['nodes'])
        #orgLines=self.pmap[deepcopy(self.Lines)]
        Lines=np.empty((0,2),dtype=int)
        for k in range(N):
            for line in self.Lines:
                newline=(self.pmap[line]+npoints*k)%(npoints*N)
                Lines=smplx_sorter(newline,Lines)
        
        line_extension_mapping=[]
        self.georgs=Lines
        for k in range(N):
            mapping=[]
            for idx,line in enumerate(self.Lines):
                newline=(self.pmap[line]+npoints*k)%(npoints*N)        
                nidx=smplx_finder(newline,Lines)
                mapping+=[nidx]
            line_extension_mapping+=[mapping]
        self.line_extension_mapping=line_extension_mapping           
        
      
    def bloch_extent(self):
         msh1=deepcopy(self)
         self.rotate()
         self.collect_lines()
         
         n_point=len(msh1.Points)
         n_cell=n_point- len(msh1.Domains['Bloch']['nodes'])
         pmap=np.arange(n_point)
         start=min(msh1.Domains['Bloch_img']['nodes'])
         pmap[start::]=np.flipud(pmap[start::])
         linemap=[]
         for k in range(msh1.DoS):
             sector=[]
             for idx,line in enumerate(msh1.Lines):
                 line=pmap[line]
                 line=(line+k*n_cell)%(msh1.DoS*n_cell)
                 new=smplx_finder(line,self.Lines)
                 sector+=[new]
             linemap+=[sector]

         dim=n_point+len(msh1.Lines)
         mapping=[]

         for i in range(dim):
             if not np.in1d(i,msh1.dofmap[:,1]):
                 mapping+=[i]
         dim=len(msh1.Points)+len(msh1.Lines)-len(msh1.dofmap)
         print('######Hier',dim)
            
         fullmap=[]
         for k in range(msh1.DoS):
            sector=[]
            for dof in range(dim):
                new=mapping[dof]
                if new // n_cell==0:
                    new=dof+k*n_cell
                else:
                    new-=n_point
                    new=linemap[k][new]
                    new+=msh1.DoS*n_cell
                sector+=[new]
            fullmap+=[np.array(sector)]
         self.blochxmap=fullmap  

        

#%%

    
    
