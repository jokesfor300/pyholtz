# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________

from setuptools import setup, find_packages

from setuptools.command.install import install
from pathlib import Path


class PostInstallCommand(install):
    """Post-installation code.
       This code is supposed to run only once.
       It is called during installation.
       Installs data for pertubation theory.
    """

    def run(self):
        install.run(self)

        import os
        # os.chdir('/')
        os.rename('PyHoltz', 'PyHoltz_instalationfile')
        # rename to make sure the installed PyHoltz
        # is imported in line 39
        from PyHoltz.nlevp.perturb_utils import prepare_perturbation
        # must rename directory, otherwise PyHoltz is imported from this
        # directory -> data will get installed in this directory
        # when directory is renamed, PyHoltz is imported from the correct path
        import PyHoltz
        print('Installation of perturbation theory data started....\n', flush=True)
        print('.... the installation can take up to 30 minutes, please wait.\n', flush=True)
        path = os.path.dirname(PyHoltz.__file__)
        print(path, flush=True)
        # find directory where data will be installed

        N = 30
        for x in range(0, N + 1):
            # create a directory
            if not os.path.isdir(Path(path + "/nlevp/perturbation_data")):
                os.mkdir(Path(path + "/nlevp/perturbation_data"))
            file_path = Path(path + "/nlevp/perturbation_data/L_order_" + str(x) + '/files.txt')
            directory = os.path.dirname(Path(file_path))
            os.mkdir(directory)

            L = prepare_perturbation(x)
            keys = L.keys()
            keys = [i for i in keys]

            values = L.values()
            values = [[list(i2) for i2 in i] for i in values]

            line = 1
            for k, v in zip(keys, values):
                with open(Path(path + "/nlevp/perturbation_data/L_order_" + str(x) + "/" + str(line) + ".txt"),
                          "w") as fp:
                    fp.write(str(k) + '\n' + str(v) + '\n')
                line += 1
        ##%%

        os.rename('PyHoltz_instalationfile', 'PyHoltz')

        print('.... installation finished.\n', flush=True)
        print('PyHolt package now ready for usage.\n', flush=True)


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='PyHoltz',
    version='1.0',
    author='Georg Mensah',
    author_email='georg.a.mensah@tu-berlin.de',
    discription='Package for solving the Hemlholtz Equation and general non-linear eigenvalue problems.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://bitbucket.org/pyholtzdevelopers/public/src/master/',
    packages=find_packages(),
    include_package_data=True,
    #    classifiers = ['Programming Language :: Python :: 3', 'Operatin System :: OS Independent'],
    cmdclass={'install': PostInstallCommand}
)
