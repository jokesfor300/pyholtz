# _____________________________________________________________________
# Copyright (c) 2018 Georg Mensah et al.
# All rights reserved.
#
# Last edited by: 
#
# Date: 
#
# This file is part of PyHoltz.
# PyHoltz is free software: you can redistribute it and/or modify 
# it under the terms of the GNU Lesser General Public License as published by 
# the Free Software Foundation, version 3 of the License. 
#
# PyHoltz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License 
# (COPYING.LESSER) along with PyHoltz.
# If not, see <https://www.gnu.org/licenses/>.
# ____________________________________________________________________
import os
from PyHoltz.nlevp.perturb_utils import prepare_perturbation
from pathlib import Path

path = os.path.dirname(os.path.realpath(__file__))

N = 30  # Defines the perturbation order that will be installed
print('Installation of perturbation theory data up to order', N, 'started.\n', flush=True)
print('If a higher perturbation order is needed. modify the value of $N$ in setup.py.\n', flush=True)
print('The installation can take some time, please wait.\n', flush=True)

path = path + "/PyHoltz/nlevp/perturbation_data"

if not Path.is_dir(Path(path)):
    Path.mkdir(Path(path))

for x in range(0, N + 1):
    print('Installing order', x, flush=True)

    directory = path + "/L_order_" + str(x) + "/"
    if not Path.is_dir(Path(directory)):
        Path.mkdir(Path(directory))

    L = prepare_perturbation(x)
    keys = L.keys()
    keys = [i for i in keys]

    values = L.values()
    values = [[list(i2) for i2 in i] for i in values]

    corr_path = Path(directory)

    line = 1
    for k, v in zip(keys, values):
        fileName = str(line) + ".txt"
        file_to_open = corr_path / fileName
        with open(file_to_open, "w") as fp:
            fp.write(str(k) + '\n' + str(v) + '\n')
        line += 1
